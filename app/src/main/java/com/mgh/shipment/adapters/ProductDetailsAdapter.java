package com.mgh.shipment.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mgh.shipment.R;
import com.mgh.shipment.Utility.GeneralUtils;
import com.mgh.shipment.beans.ArrayListManagerModel;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Akshay Thapliyal on 26-05-2016.
 */
public class ProductDetailsAdapter extends RecyclerView.Adapter<ProductDetailsAdapter.MyViewHolder> {

    private String type [] ={"Shipment Date: ", "Delivery Date: ", "HBL: ", "POD: ", "POL: " ,"Container No.: ", "Document No.: ", "Shipper Name: ", "Buyer Name: ", "Carrier Name: ", "Carrier No.:",
    "Total Volume: ", "Charge Weight: ", "Gross Weight: "};
    private String data ;
    private  Context context;
    private ArrayListManagerModel arrayListManagerModel;
    public ProductDetailsAdapter(Context context, ArrayListManagerModel arrayListManagerModel){
        this.context = context;
        this.arrayListManagerModel = arrayListManagerModel;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_details_layout, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.tvType.setText(type[position]);

        switch (position){
            case 0 : data = GeneralUtils.getDate(arrayListManagerModel.getTrackingDataArrayList().get(0).getShipmentDate());
                break;
            case 1 : //data = GeneralUtils.getDate(arrayListManagerModel.getTrackingDataArrayList().get(0).getDeliveryDate());
                data = "NA";
                break;
            case 2 : data = arrayListManagerModel.getTrackingDataArrayList().get(0).getHblNo();
                break;
            case 3 : data = arrayListManagerModel.getTrackingDataArrayList().get(0).getPod();
                break;
            case 4 : data = arrayListManagerModel.getTrackingDataArrayList().get(0).getPol();
                break;
            case 5 : data = "NA";
                break;
            case 6 : data = arrayListManagerModel.getTrackingDataArrayList().get(0).getDocumentNo();
                break;
            case 7 : data = arrayListManagerModel.getTrackingDataArrayList().get(0).getShipperName();
                break;
            case 8 : data = arrayListManagerModel.getTrackingDataArrayList().get(0).getBuyerName();
                break;
            case 9 :// data = arrayListManagerModel.getTrackingDataArrayList().get(0).getCarrierName();
                data="NA";
                break;
            case 10 :// data = arrayListManagerModel.getTrackingDataArrayList().get(0).getCarrierNo();
                data="NA";
                break;
            case 11 : data = arrayListManagerModel.getTrackingDataArrayList().get(0).getTotalVol();
                break;
            case 12 : data = arrayListManagerModel.getTrackingDataArrayList().get(0).getChargeWt();
                break;
            case 13 : data = arrayListManagerModel.getTrackingDataArrayList().get(0).getGrossWt();
                break;
        }
        holder.tvValue.setText(data);
    }

    private String getDate(String date){
        String dateString = date;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return DateFormat.getDateInstance().format(convertedDate);
    }

    @Override
    public int getItemCount() {
        return type.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvType, tvValue;

        public MyViewHolder(View view) {
            super(view);
            tvType = (TextView) view.findViewById(R.id.tvType);
            tvValue = (TextView)view.findViewById(R.id.tvValue);
        }
    }
}
