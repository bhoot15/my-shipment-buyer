package com.mgh.shipment.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mgh.shipment.R;
import com.mgh.shipment.Utility.GeneralUtils;
import com.mgh.shipment.beans.ArrayListManagerModel;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Akshay Thapliyal on 08-09-2016.
 */
public class ScheduleDetailAdapter extends RecyclerView.Adapter<ScheduleDetailAdapter.MyViewHolder> {

    private String type [] ={"POD: ", "POL: " ,"ETD: ", "ETA: ", "Transhipment PORT: "};
    private String data ;
    private Context context;
    private ArrayListManagerModel arrayListManagerModel;
    public ScheduleDetailAdapter(Context context, ArrayListManagerModel arrayListManagerModel){
        this.context = context;
        this.arrayListManagerModel = arrayListManagerModel;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_details_layout, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.tvType.setText(type[position]);

        switch (position){
            case 0 : data =  arrayListManagerModel.getPackagingDataArrayList().get(0).getPod();
                break;
            case 1 : data =   arrayListManagerModel.getPackagingDataArrayList().get(0).getPol();
                break;
            case 2 : data = GeneralUtils.getDate(arrayListManagerModel.getPackagingDataArrayList().get(0).getEtd());
                break;
            case 3 : data =  GeneralUtils.getDate(arrayListManagerModel.getPackagingDataArrayList().get(0).getEta());
                break;
            case 4 : data =  arrayListManagerModel.getPackagingDataArrayList().get(0).getTransShipmentNo();
                break;
        }
        holder.tvValue.setText(data);
    }

    private String getDate(String date){
        String dateString = date;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return DateFormat.getDateInstance().format(convertedDate);
    }

    @Override
    public int getItemCount() {
        return type.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvType, tvValue;

        public MyViewHolder(View view) {
            super(view);
            tvType = (TextView) view.findViewById(R.id.tvType);
            tvValue = (TextView)view.findViewById(R.id.tvValue);
        }
    }
}
