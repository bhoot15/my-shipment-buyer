package com.mgh.shipment.adapters;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.mgh.shipment.R;
import com.mgh.shipment.Utility.ApiUrls;
import com.mgh.shipment.activities.MilestoneTrackingActivity;
import com.mgh.shipment.beans.ArrayListManagerModel;
import com.mgh.shipment.beans.TrackingData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Akshay Thapliyal on 10-08-2016.
 */
public class MISAdapter  extends RecyclerView.Adapter<MISAdapter.MyViewHolder> {
    public LinearLayout containerLayout;
    private List<TextView> textList;
    Context mContext;
    ArrayListManagerModel mArrayListManagerModel;

    public MISAdapter(Context context, ArrayListManagerModel arrayListManagerModel) {

        this.mContext = context;
        this.mArrayListManagerModel = arrayListManagerModel;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.trackings_inflate_layout, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        ArrayListManagerModel arrayListManagerModel = ArrayListManagerModel.getInstance();
        TextView hblTextView = holder.hblTextView;
        TextView containerTextView = holder.containerTextView;

        hblTextView.setText(mArrayListManagerModel.getGetNShipmentDataArrayList().get(position).getHblNo());
        containerTextView.setText(mArrayListManagerModel.getGetNShipmentDataArrayList().get(position).getContainerNo());
        holder.polTextView.setText(mArrayListManagerModel.getGetNShipmentDataArrayList().get(position).getPortOfLoading());
        holder.podTextView.setText(mArrayListManagerModel.getGetNShipmentDataArrayList().get(position).getPortOfDestination());
        addMoreTextView(holder);
        setMoreTextViewListeners();
        holder.cardLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //callHblDetailsApi();
            }
        });
     /*   holder.tvMoreContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (holder.tvMoreContainer.getText().toString().contains(mContext.getResources().getString(R.string.more_text))) {
                    holder.moreLinearLayout.setVisibility(View.VISIBLE);
                    holder.tvMoreContainer.setText(mContext.getResources().getString(R.string.less_text));
                } else {
                    holder.moreLinearLayout.setVisibility(View.GONE);
                    holder.tvMoreContainer.setText(mContext.getResources().getString(R.string.more_text));
                }
            }
        });*/
    }

    private void showHblDialog(){
        final Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.hbl_info_dialog);
        //dialog.setTitle(mContext.getResources().getString(R.string.forgot_paswrd_text));
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        RecyclerView productDetailsRecycler = (RecyclerView)dialog.findViewById(R.id.productDetailsRecycler);
        productDetailsRecycler.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext);
        layoutManager = new LinearLayoutManager(mContext);
        productDetailsRecycler.setLayoutManager(layoutManager);
        productDetailsRecycler.setItemAnimator(new DefaultItemAnimator());
        HBLDetailAdapter hblDetailAdapter = new HBLDetailAdapter(mContext, ArrayListManagerModel.getInstance());
        productDetailsRecycler.setAdapter(hblDetailAdapter);

        Button okBtn = (Button)dialog.findViewById(R.id.okBtn);
        dialog.show();

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void addMoreTextView(MyViewHolder holder) {

        if (textList != null) {
            textList.clear();
        }
        textList = new ArrayList<TextView>(3);
        for (int i = 0; i < 3; i++) {
            TextView textView = new TextView(mContext);
            textView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));
            textView.setText("TextView" + i);
            textView.setTypeface(null, Typeface.BOLD);
            textView.setTextSize(mContext.getResources().getDimension(R.dimen.text_size_medium));
            textView.setTextColor(mContext.getResources().getColor(R.color.color_dark_blue));
            holder.moreLinearLayout.addView(textView);
            textList.add(textView);
        }
    }

    private void setMoreTextViewListeners() {
        int length = textList.size();
        String containerText = "";
        for (int i = 0; i < length; i++) {
            containerText = textList.get(i).getText().toString();
            final int j = i;
            textList.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Toast.makeText(mContext, textList.get(j).getText().toString(), Toast.LENGTH_SHORT).show();
                   /* Intent intent = new Intent(mContext, MilestoneTrackingActivity.class);
                    intent.putExtra("containerText", textList.get(i).getText().toString());
                    mContext.startActivity(intent);*/

                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mArrayListManagerModel.getGetNShipmentDataArrayList().size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView hblTextView, containerTextView, polTextView, podTextView, tvMoreContainer;
        public LinearLayout moreLinearLayout;
        private CardView cardLayout;


        public MyViewHolder(View view) {
            super(view);
            cardLayout = (CardView)view.findViewById(R.id.cardLayout);
            hblTextView = (TextView) view.findViewById(R.id.hblTextView);
            containerTextView = (TextView) view.findViewById(R.id.containerTextView);
            podTextView = (TextView) view.findViewById(R.id.podTextView);
            moreLinearLayout = (LinearLayout) view.findViewById(R.id.moreLinearLayout);
            polTextView = (TextView) view.findViewById(R.id.polTextView);
            tvMoreContainer = (TextView) view.findViewById(R.id.tvMoreContainer);
            containerLayout = (LinearLayout) view.findViewById(R.id.containerLayout);

        }
    }


    private void callHblDetailsApi(){
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("blNo", "BOM000483");
            jsonObject.put("salesOrg", "BOM2");
            jsonObject.put("distributionChannel", "EX");
            jsonObject.put("division", "SE");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final ProgressDialog dialog = new ProgressDialog(mContext);
        dialog.setMessage(mContext.getResources().getString(R.string.please_wait_text));
        dialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                ApiUrls.TRACKING_URL, jsonObject, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

                try {
                    // Parsing json object response
                    System.out.println("response: " + response);
                    JSONArray jsonArray = response.getJSONArray("documentHeaderLst");
                    int length = jsonArray.length();
                    ArrayListManagerModel arrayListManagerModel = ArrayListManagerModel.getInstance();
                    ArrayList<TrackingData> trackingDataArrayList = new ArrayList<>();
                    if (length>0) {
                        for (int i =0; i<length; i++){
                            TrackingData trackingData = new TrackingData();
                            trackingData.setHblNo(jsonArray.getJSONObject(i).getString("bl_no"));
                            trackingData.setContainerNo(jsonArray.getJSONObject(i).getString("bl_no"));
                            trackingData.setShipmentDate(jsonArray.getJSONObject(i).getString("shipment_date"));
                            trackingData.setDeliveryDate(jsonArray.getJSONObject(i).getString("shipment_date"));
                            trackingData.setPod(jsonArray.getJSONObject(i).getString("pod_name"));
                            trackingData.setPol(jsonArray.getJSONObject(i).getString("pol_name"));
                            trackingDataArrayList.add(trackingData);
                        }
                        arrayListManagerModel.setTrackingDataArrayList(trackingDataArrayList);
                        if(trackingDataArrayList.size()>0){
                            //showHblDialog();
                            Intent intent = new Intent(mContext, MilestoneTrackingActivity.class);
                            mContext.startActivity(intent);
                        }else{
                            Toast.makeText(mContext,"No data available",Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(mContext,
                                mContext.getResources().getString(R.string.error_fetching),
                                Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(mContext,
                            mContext.getResources().getString(R.string.error_fetching),
                            Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mContext,
                        mContext.getResources().getString(R.string.error_fetching), Toast.LENGTH_SHORT).show();
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

        // Adding request to request queue
        requestQueue.add(jsonObjReq);
    }
    private void showHblDetails(){
        final Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.hbl_detail_dialog);
        dialog.setTitle(mContext.getResources().getString(R.string.forgot_paswrd_text));
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        RecyclerView trackRecyclerView = (RecyclerView)dialog.findViewById(R.id.trackRecyclerView);
        trackRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext);
        trackRecyclerView.setLayoutManager(layoutManager);
        trackRecyclerView.setItemAnimator(new DefaultItemAnimator());
        ProductDetailsAdapter productDetailsAdapter = new ProductDetailsAdapter(mContext, ArrayListManagerModel.getInstance());
        trackRecyclerView.setAdapter(productDetailsAdapter);

        Button resetBtn = (Button)dialog.findViewById(R.id.resetBtn);
        EditText userEditText = (EditText)dialog.findViewById(R.id.userEditText);
        dialog.show();

        resetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
}
