package com.mgh.shipment.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mgh.shipment.R;
import com.mgh.shipment.activities.MilestoneTrackingActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Akshay Thapliyal on 16-05-2016.
 */
public class TrackingIdAdapter extends RecyclerView.Adapter<TrackingIdAdapter.MyViewHolder> {

    public LinearLayout containerLayout;
    private List<TextView> textList;
    ArrayList<String> data, data2, pod, pol;
    Context mContext;

    public TrackingIdAdapter(Context context, ArrayList<String> data, ArrayList<String> data2, ArrayList<String> pod, ArrayList<String> pol) {
        this.data = data;
        this.data2 = data2;
        this.pod = pod;
        this.pol = pol;
        this.mContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.trackings_inflate_layout, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        TextView hblTextView = holder.hblTextView;
        TextView containerTextView = holder.containerTextView;

        hblTextView.setText(data.get(position));
        containerTextView.setText(data2.get(position));
        holder.polTextView.setText(pol.get(position));
        holder.podTextView.setText(pod.get(position));
        addMoreTextView(holder);
        setMoreTextViewListeners();
        holder.tvMoreContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (holder.tvMoreContainer.getText().toString().contains(mContext.getResources().getString(R.string.more_text))) {
                    holder.moreLinearLayout.setVisibility(View.VISIBLE);
                    holder.tvMoreContainer.setText(mContext.getResources().getString(R.string.less_text));
                } else {
                    holder.moreLinearLayout.setVisibility(View.GONE);
                    holder.tvMoreContainer.setText(mContext.getResources().getString(R.string.more_text));
                }
            }
        });
    }

    private void addMoreTextView(MyViewHolder holder) {

        if (textList != null) {
            textList.clear();
        }
        textList = new ArrayList<TextView>(3);
        for (int i = 0; i < 3; i++) {
            TextView textView = new TextView(mContext);
            textView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));
            textView.setText("TextView" + i);
            textView.setTypeface(null, Typeface.BOLD);
            textView.setTextSize(mContext.getResources().getDimension(R.dimen.text_size_medium));
            textView.setTextColor(mContext.getResources().getColor(R.color.color_dark_blue));
            holder.moreLinearLayout.addView(textView);
            textList.add(textView);
        }
    }

    private void setMoreTextViewListeners() {
        int length = textList.size();
        String containerText = "";
        for (int i = 0; i < length; i++) {
            containerText = textList.get(i).getText().toString();
            final int j = i;
            textList.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Toast.makeText(mContext, textList.get(j).getText().toString(), Toast.LENGTH_SHORT).show();
                   /* Intent intent = new Intent(mContext, MilestoneTrackingActivity.class);
                    intent.putExtra("containerText", textList.get(i).getText().toString());
                    mContext.startActivity(intent);*/

                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView hblTextView, containerTextView, polTextView, podTextView, tvMoreContainer;
        public LinearLayout moreLinearLayout;


        public MyViewHolder(View view) {
            super(view);
            hblTextView = (TextView) view.findViewById(R.id.hblTextView);
            containerTextView = (TextView) view.findViewById(R.id.containerTextView);
            podTextView = (TextView) view.findViewById(R.id.podTextView);
            moreLinearLayout = (LinearLayout) view.findViewById(R.id.moreLinearLayout);
            polTextView = (TextView) view.findViewById(R.id.polTextView);
            tvMoreContainer = (TextView) view.findViewById(R.id.tvMoreContainer);
            containerLayout = (LinearLayout) view.findViewById(R.id.containerLayout);
        }
    }
}
