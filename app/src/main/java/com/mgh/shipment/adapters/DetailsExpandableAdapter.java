package com.mgh.shipment.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentObject;
import com.mgh.shipment.R;
import com.mgh.shipment.Utility.AppConstants;
import com.mgh.shipment.beans.ArrayListManagerModel;
import com.mgh.shipment.beans.ChildDetailsData;
import com.mgh.shipment.beans.DetailsChildViewHolder;
import com.mgh.shipment.beans.DetailsParentViewHolder;
import com.mgh.shipment.beans.ShipmentDetailsData;

import java.util.List;
import java.util.zip.Inflater;

/**
 * Created by Akshay Thapliyal on 01-09-2016.
 */
public class DetailsExpandableAdapter extends ExpandableRecyclerAdapter<DetailsParentViewHolder, DetailsChildViewHolder> {

    private LayoutInflater mInflater;
    private ArrayListManagerModel mArrayListManagerModel;

    public DetailsExpandableAdapter(Context context, List<ParentObject> parentItemList, LayoutInflater inflater, ArrayListManagerModel arrayListManagerModel) {
        super(context, parentItemList);
        this.mInflater = inflater;
        this.mArrayListManagerModel = arrayListManagerModel;
    }

    @Override
    public DetailsParentViewHolder onCreateParentViewHolder(ViewGroup viewGroup) {
        View view = mInflater.inflate(R.layout.expandable_parent_list, viewGroup, false);
        return new DetailsParentViewHolder(view);
    }

    @Override
    public DetailsChildViewHolder onCreateChildViewHolder(ViewGroup viewGroup) {
        View view = mInflater.inflate(R.layout.expandable_child_list, viewGroup, false);
        return new DetailsChildViewHolder(view);
    }

    @Override
    public void onBindParentViewHolder(DetailsParentViewHolder detailsParentViewHolder, int i, Object o) {
        System.out.println("Parent");
        detailsParentViewHolder.tvBlNo.setText(mArrayListManagerModel.getShipmentDetailsDataArrayList().get(i).getBlNo());
    }

    @Override
    public void onBindChildViewHolder(DetailsChildViewHolder detailsChildViewHolder, int i, Object o) {
        ChildDetailsData childDetailsData = (ChildDetailsData)o;
        System.out.println("Child" + childDetailsData.getBldate());
        detailsChildViewHolder.tvBLDate.setText(mArrayListManagerModel.getShipmentDetailsDataArrayList().get(i).getBldate());
    }
}