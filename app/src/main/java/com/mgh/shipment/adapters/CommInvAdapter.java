package com.mgh.shipment.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mgh.shipment.R;
import com.mgh.shipment.Utility.GeneralUtils;
import com.mgh.shipment.Utility.ScrollCallBack;
import com.mgh.shipment.beans.ArrayListManagerModel;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Akshay Thapliyal on 03-01-2017.
 */
public class CommInvAdapter extends RecyclerView.Adapter<CommInvAdapter.ShipmentViewHolder> {

    Context mContext;
    private int currentOpenPosition = -1;
    private ArrayListManagerModel mArrayListManagerModel;
    private ScrollCallBack scrollCallBack;

    public CommInvAdapter(Context mContext, ArrayListManagerModel arrayListManagerModel) {
        this.mContext = mContext;
        this.mArrayListManagerModel = arrayListManagerModel;
        this.scrollCallBack = (ScrollCallBack) mContext;
    }

    @Override
    public ShipmentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.comm_inv_inflate_list, null);
        return new ShipmentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ShipmentViewHolder holder, final int position) {

        try {
            holder.tvBlNo.setText(mArrayListManagerModel.getCommInvDataArrayList().get(position).getHblNo());
            holder.tvBookingDate.setText(GeneralUtils.getDate(mArrayListManagerModel.getCommInvDataArrayList().get(position).getBookingDate()));
            holder.tvGrossWt.setText(mArrayListManagerModel.getCommInvDataArrayList().get(position).getGrossWt());
            holder.tvPOD.setText(mArrayListManagerModel.getCommInvDataArrayList().get(position).getPod());
            holder.tvPOL.setText(mArrayListManagerModel.getCommInvDataArrayList().get(position).getPol());
            holder.tvShipmentDate.setText(GeneralUtils.getDate(mArrayListManagerModel.getCommInvDataArrayList().get(position).getShipmentDate()));
            holder.tvShipperName.setText(mArrayListManagerModel.getCommInvDataArrayList().get(position).getShipperName());
            holder.tvPONo.setText(mArrayListManagerModel.getCommInvDataArrayList().get(position).getPoNo());
            holder.consignee.setText(mArrayListManagerModel.getCommInvDataArrayList().get(position).getConsignee());
            holder.tvAta.setText(getConvertedDate(mArrayListManagerModel.getCommInvDataArrayList().get(position).getAta()).toString());
            holder.tvAtd.setText(getConvertedDate(mArrayListManagerModel.getCommInvDataArrayList().get(position).getAtd()).toString());
            holder.tvEta.setText(getConvertedDate(mArrayListManagerModel.getCommInvDataArrayList().get(position).getEta()).toString());
            holder.tvETD.setText(getConvertedDate(mArrayListManagerModel.getCommInvDataArrayList().get(position).getEtd()).toString());
            holder.tvDeliveryAgent.setText(mArrayListManagerModel.getCommInvDataArrayList().get(position).getDeliveryAgent());
            holder.tvCarrier.setText(mArrayListManagerModel.getCommInvDataArrayList().get(position).getCarrier());
            holder.tvTotalPcs.setText(mArrayListManagerModel.getCommInvDataArrayList().get(position).getTotalPcs());
            holder.tvPOStatus.setText(getPOStatus(position));
            holder.tvExceptionStatus.setText(getExceptionStatus(position));
            holder.commInvNo.setText(mArrayListManagerModel.getCommInvDataArrayList().get(position).getCommInvNo());

        } catch (NullPointerException e) {

        }


        holder.childLinearLayout.setVisibility(View.GONE);
        holder.expandArrow.setImageResource((R.drawable.ic_arrow_up));

        holder.topMainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.childLinearLayout.getVisibility() == View.VISIBLE) {
                    holder.childLinearLayout.setVisibility(View.GONE);
                } else {
                    holder.childLinearLayout.setVisibility(View.VISIBLE);
                    currentOpenPosition = position;
                    notifyDataSetChanged();
                    scrollCallBack.scrollToClickedPosition(position);
                }
            }
        });

        if (currentOpenPosition == position) {
            holder.childLinearLayout.setVisibility(View.VISIBLE);
            holder.expandArrow.setImageResource((R.drawable.ic_arrow_down));
        }
    }

    @Override
    public int getItemCount() {
        return mArrayListManagerModel.getInstance().getCommInvDataArrayList().size();
    }

    private String getPOStatus(int position) {
        String status = "";

        if (!mArrayListManagerModel.getCommInvDataArrayList().get(position).getBookingDate().equals("null")) {
            status = "ORDER BOOKED";
            if (!mArrayListManagerModel.getCommInvDataArrayList().get(position).getGoodReceivedDate().equals("null")) {
                status = "CARGO RECEIVED";
                if (!mArrayListManagerModel.getCommInvDataArrayList().get(position).getShipmentDate().equals("null")) {
                    status = "STUFFING DONE";
                    if (!mArrayListManagerModel.getCommInvDataArrayList().get(position).getEtd().equals("null")) {
                        Date toDate = new Date();
                        //System.out.println("DATE1:: " + );
                        if(convertDate(mArrayListManagerModel.getCommInvDataArrayList().get(position).getEtd()).compareTo(toDate)<0){
                            status = "CARGO DEPARTED";
                        }

                    }
                    if (!mArrayListManagerModel.getCommInvDataArrayList().get(position).getEta().equals("null")) {
                        Date toDate = new Date();
                        if(convertDate(mArrayListManagerModel.getCommInvDataArrayList().get(position).getEta()).compareTo(toDate)<0){
                            status = "CARGO ARRIVED";
                        }

                    }
                }
            }
        }
        return status;
    }

    private String getConvertedDate(String strDate) {

        if(strDate.equals("null")){
            return "";

        }else {
            String dateFormat = ""; //Fri Mar 20 20:44:49 CET 2015

            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            long mDateLongValue = Long.parseLong(strDate);
            Date mDate = new Date(mDateLongValue);
            dateFormat = formatter.format(mDate);

            return dateFormat;
        }
    }

    private Date convertDate(String strDate) {
        String dateFormat = strDate; //Fri Mar 20 20:44:49 CET 2015

        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        long mDateLongValue = Long.parseLong(strDate);
        Date mDate = new Date(mDateLongValue);
        dateFormat = formatter.format(mDate);

        return mDate;
    }

    private String getExceptionStatus(int position) {
        String status = "";

        if (!mArrayListManagerModel.getCommInvDataArrayList().get(position).getEta().equals("null")) {

            if (!mArrayListManagerModel.getCommInvDataArrayList().get(position).getAta().equals("null")) {
                if (mArrayListManagerModel.getCommInvDataArrayList().get(position).getAta().compareTo(mArrayListManagerModel.getCommInvDataArrayList().get(position).getEta()) > 0) {
                    status = "DELAYED";
                }
                if (mArrayListManagerModel.getCommInvDataArrayList().get(position).getAta().compareTo(mArrayListManagerModel.getCommInvDataArrayList().get(position).getEta()) < 0) {
                    status = "ADVANCE";
                }
                if (mArrayListManagerModel.getCommInvDataArrayList().get(position).getAta().compareTo(mArrayListManagerModel.getCommInvDataArrayList().get(position).getEta()) == 0) {
                    status = "ON-TIME";
                }
            } else {
                if (!mArrayListManagerModel.getCommInvDataArrayList().get(position).getFeta().equals("null")) {

                    if (mArrayListManagerModel.getCommInvDataArrayList().get(position).getFeta().compareTo(mArrayListManagerModel.getCommInvDataArrayList().get(position).getEta()) > 0) {
                        status = "DELAYED";
                    }
                    if (mArrayListManagerModel.getCommInvDataArrayList().get(position).getFeta().compareTo(mArrayListManagerModel.getCommInvDataArrayList().get(position).getEta()) < 0) {
                        status = "ADVANCE";
                    }
                    if (mArrayListManagerModel.getCommInvDataArrayList().get(position).getFeta().compareTo(mArrayListManagerModel.getCommInvDataArrayList().get(position).getEta()) == 0) {
                        status = "ON-TIME";
                    }
                } else {
                    status = "NOT AVAILABLE";
                }
            }

        } else {

            if (!mArrayListManagerModel.getCommInvDataArrayList().get(position).getEtd().equals("null")) {
                if (mArrayListManagerModel.getCommInvDataArrayList().get(position).getAtd() != null) {
                    if (mArrayListManagerModel.getCommInvDataArrayList().get(position).getAtd().compareTo(mArrayListManagerModel.getCommInvDataArrayList().get(position).getEtd()) > 0) {
                        status = "DELAYED";
                    }
                    if (mArrayListManagerModel.getCommInvDataArrayList().get(position).getAtd().compareTo(mArrayListManagerModel.getCommInvDataArrayList().get(position).getEtd()) < 0) {
                        status = "ADVANCE";
                    }
                    if (mArrayListManagerModel.getCommInvDataArrayList().get(position).getAtd().compareTo(mArrayListManagerModel.getCommInvDataArrayList().get(position).getEtd()) == 0) {
                        status = "ON-TIME";
                    }

                } else {

                    if (!mArrayListManagerModel.getCommInvDataArrayList().get(position).getFetd().equals("null")) {
                        if (mArrayListManagerModel.getCommInvDataArrayList().get(position).getFetd().compareTo(mArrayListManagerModel.getCommInvDataArrayList().get(position).getEtd()) > 0) {
                            status = "DELAYED";
                        }
                        if (mArrayListManagerModel.getCommInvDataArrayList().get(position).getFetd().compareTo(mArrayListManagerModel.getCommInvDataArrayList().get(position).getEtd()) < 0) {
                            status = "ADVANCE";
                        }
                        if (mArrayListManagerModel.getCommInvDataArrayList().get(position).getFetd().compareTo(mArrayListManagerModel.getCommInvDataArrayList().get(position).getEtd()) == 0) {
                            status = "ON-TIME";
                        }
                    } else {
                        status = "NOT AVAILABLE";
                    }
                }

            } else {
                status = "NOT AVAILABLE";
            }

        }


        return status;

    }

    public class ShipmentViewHolder extends RecyclerView.ViewHolder {

        public TextView tvPONo, tvShipperName, tvBlNo;
        public ImageView expandArrow;
        public TextView consignee, tvBookingDate, tvPOL, tvPOD, tvGrossWt, commInvNo,
                tvTotalPcs, tvGrDate, tvShipmentDate, tvPOStatus, tvExceptionStatus, tvEta, tvETD, tvAtd, tvAta,
                tvCarrier, tvDeliveryAgent;
        private LinearLayout childLinearLayout;
        private RelativeLayout topMainLayout;

        public ShipmentViewHolder(View itemView) {
            super(itemView);
            topMainLayout = (RelativeLayout) itemView.findViewById(R.id.topMainLayout);
            tvPONo = (TextView) itemView.findViewById(R.id.tvPONo);
            tvShipperName = (TextView) itemView.findViewById(R.id.tvShipperName);
            tvBlNo = (TextView) itemView.findViewById(R.id.tvBlNo);
            childLinearLayout = (LinearLayout) itemView.findViewById(R.id.childLinearLayout);
            expandArrow = (ImageView) itemView.findViewById(R.id.expandArrow);
            tvCarrier = (TextView) itemView.findViewById(R.id.carrierValue);
            tvBookingDate = (TextView) itemView.findViewById(R.id.tvBookingDate);
            tvPOL = (TextView) itemView.findViewById(R.id.tvPOLCode);
            tvPOD = (TextView) itemView.findViewById(R.id.tvPODCode);
            tvGrossWt = (TextView) itemView.findViewById(R.id.tvGrossWt);
            tvDeliveryAgent = (TextView) itemView.findViewById(R.id.deliveryAgentValue);
            tvTotalPcs = (TextView) itemView.findViewById(R.id.totalPcsValue);
            tvGrDate = (TextView) itemView.findViewById(R.id.tvGrDate);
            tvShipmentDate = (TextView) itemView.findViewById(R.id.tvShipmentDate);
            tvExceptionStatus = (TextView) itemView.findViewById(R.id.exceptionStatusValue);
            tvPOStatus = (TextView) itemView.findViewById(R.id.poStatusValue);
            tvEta = (TextView) itemView.findViewById(R.id.etaValue);
            tvETD = (TextView) itemView.findViewById(R.id.etdValue);
            tvAtd = (TextView) itemView.findViewById(R.id.atdValue);
            tvAta = (TextView) itemView.findViewById(R.id.ataValue);
            consignee = (TextView) itemView.findViewById(R.id.consigneeValue);
            commInvNo = (TextView) itemView.findViewById(R.id.commInvNoValue);
        }
    }
}
