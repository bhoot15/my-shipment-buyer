package com.mgh.shipment.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mgh.shipment.R;
import com.mgh.shipment.Utility.ScrollCallBack;
import com.mgh.shipment.beans.ArrayListManagerModel;

/**
 * Created by Akshay Thapliyal on 09-08-2016.
 */
public class ShipmentDetailAdapter extends RecyclerView.Adapter<ShipmentDetailAdapter.ShipmentViewHolder> {

    Context mContext;
    private int currentOpenPosition = -1;
    private ArrayListManagerModel mArrayListManagerModel;
    private  ScrollCallBack scrollCallBack;

    public ShipmentDetailAdapter(Context mContext, ArrayListManagerModel arrayListManagerModel) {
        this.mContext = mContext;
        this.mArrayListManagerModel = arrayListManagerModel;
        this.scrollCallBack =(ScrollCallBack)mContext;
    }

    @Override
    public ShipmentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.expandable_parent_list, null);
        return new ShipmentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ShipmentViewHolder holder, final int position) {

        holder.tvBlNo.setText(mArrayListManagerModel.getShipmentDetailsDataArrayList().get(position).getBlNo());
        holder.tvBLDate.setText(mArrayListManagerModel.getShipmentDetailsDataArrayList().get(position).getBldate());
        holder.tvBookingDate.setText(mArrayListManagerModel.getShipmentDetailsDataArrayList().get(position).getBookingDate());
        holder.tvChargeWt.setText(mArrayListManagerModel.getShipmentDetailsDataArrayList().get(position).getChargeWeight());
        holder.tvGrDate.setText(mArrayListManagerModel.getShipmentDetailsDataArrayList().get(position).getGrDate());
        holder.tvGrossWt.setText(mArrayListManagerModel.getShipmentDetailsDataArrayList().get(position).getGrossWeight());
        holder.tvPODCode.setText(mArrayListManagerModel.getShipmentDetailsDataArrayList().get(position).getPodCode());
        holder.tvPOLCode.setText(mArrayListManagerModel.getShipmentDetailsDataArrayList().get(position).getPolCode());
        holder.tvShipmentDate.setText(mArrayListManagerModel.getShipmentDetailsDataArrayList().get(position).getShipmentDate());
        holder.tvShipperName.setText(mArrayListManagerModel.getShipmentDetailsDataArrayList().get(position).getBuyerName());
        holder.tvShipperNo.setText(mArrayListManagerModel.getShipmentDetailsDataArrayList().get(position).getShipperNo());
        holder.tvTotalVol.setText(mArrayListManagerModel.getShipmentDetailsDataArrayList().get(position).getTotVolume());

        holder.childLinearLayout.setVisibility(View.GONE);
        holder.expandArrow.setImageResource((R.drawable.ic_arrow_up));

        holder.topMainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.childLinearLayout.getVisibility() == View.VISIBLE) {
                    holder.childLinearLayout.setVisibility(View.GONE);
                    holder.expandArrow.setImageResource((R.drawable.ic_arrow_up));
                } else {
                    holder.childLinearLayout.setVisibility(View.VISIBLE);
                    holder.expandArrow.setImageResource((R.drawable.ic_arrow_down));
                    currentOpenPosition = position;
                    notifyDataSetChanged();
                    scrollCallBack.scrollToClickedPosition(position);
                }
            }
        });
        if (currentOpenPosition == position) {
            holder.childLinearLayout.setVisibility(View.VISIBLE);
            holder.expandArrow.setImageResource((R.drawable.ic_arrow_down));
        }


    }

    @Override
    public int getItemCount() {
        return mArrayListManagerModel.getInstance().getShipmentDetailsDataArrayList().size();
    }

    public class ShipmentViewHolder extends RecyclerView.ViewHolder {

        public TextView tvShipperNo, tvShipperName, tvBlNo;
        public ImageView expandArrow;
        public TextView tvBLDate, tvBookingDate, tvPOLCode, tvPODCode, tvGrossWt, tvChargeWt,
                tvTotalVol, tvGrDate, tvShipmentDate;
        private LinearLayout childLinearLayout;
        private RelativeLayout topMainLayout;

        public ShipmentViewHolder(View itemView) {
            super(itemView);
            topMainLayout = (RelativeLayout) itemView.findViewById(R.id.topMainLayout);
            tvShipperNo = (TextView) itemView.findViewById(R.id.tvShipperNo);
            tvShipperName = (TextView) itemView.findViewById(R.id.tvShipperName);
            tvBlNo = (TextView) itemView.findViewById(R.id.tvBlNo);
            childLinearLayout = (LinearLayout) itemView.findViewById(R.id.childLinearLayout);
            expandArrow = (ImageView) itemView.findViewById(R.id.expandArrow);
            tvBLDate = (TextView) itemView.findViewById(R.id.tvBLDate);
            tvBookingDate = (TextView) itemView.findViewById(R.id.tvBookingDate);
            tvPOLCode = (TextView) itemView.findViewById(R.id.tvPOLCode);
            tvPODCode = (TextView) itemView.findViewById(R.id.tvPODCode);
            tvGrossWt = (TextView) itemView.findViewById(R.id.tvGrossWt);
            tvChargeWt = (TextView) itemView.findViewById(R.id.tvChargeWt);
            tvTotalVol = (TextView) itemView.findViewById(R.id.tvTotalVol);
            tvGrDate = (TextView) itemView.findViewById(R.id.tvGrDate);
            tvShipmentDate = (TextView) itemView.findViewById(R.id.tvShipmentDate);
        }
    }
}
