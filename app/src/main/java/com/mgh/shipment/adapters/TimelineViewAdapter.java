package com.mgh.shipment.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.mgh.shipment.R;
import com.mgh.shipment.beans.ArrayListManagerModel;
import com.vipul.hp_hp.timelineview.TimelineView;

/**
 * Created by Akshay Thapliyal on 26-05-2016.
 */
public class TimelineViewAdapter extends RecyclerView.Adapter<TimelineViewAdapter.TimeLineViewHolder> {
    private String data [] ={"Booking Confirmed   ","Goods Received   ","Stuffing Done   ", "Delivered   "};
    //,"Stage 5", "Stage 6", "Stage 7", "Stage 8", "Stage 9"
    private String checkBookingDate= "", checkGoodsReceived="", checkStuffingDone="", checkDelivered="";
    private Context mContext;
    private ArrayListManagerModel mArrayListManagerModel;
    public TimelineViewAdapter(Context context, ArrayListManagerModel arrayListManagerModel){
        this.mContext = context;
        this.mArrayListManagerModel = arrayListManagerModel;

    }
    @Override
    public TimelineViewAdapter.TimeLineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(parent.getContext(), R.layout.milestone_timeline_layout, null);
        return new TimeLineViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(TimeLineViewHolder holder, int position) {
        holder.tvName.setText(data[position]);
        holder.mTimelineView.setActivated(false);
        checkBookingDate = mArrayListManagerModel.getTrackingDataArrayList().get(0).getBookingDate();
        checkGoodsReceived = mArrayListManagerModel.getTrackingDataArrayList().get(0).getGoodsReceived();
        checkStuffingDone = mArrayListManagerModel.getTrackingDataArrayList().get(0).getShipmentDate();
        checkDelivered ="";


        if(checkBookingDate == null || checkBookingDate.isEmpty() && position==0){
            holder.tvName.setTextColor(mContext.getResources().getColor(R.color.color_dark_grey));
            holder.mTimelineView.setMarker(mContext.getResources().getDrawable(R.drawable.not_delivered_drawable));
            holder.dateText.setText(mArrayListManagerModel.getTrackingDataArrayList().get(0).getBookingDate());
        }else{
            holder.tvName.setTextColor(mContext.getResources().getColor(R.color.color_dark_blue));
            holder.mTimelineView.setMarker(mContext.getResources().getDrawable(R.drawable.delivered_drawable));
            holder.dateText.setText("NA");
        }
        if(checkGoodsReceived == null || checkGoodsReceived.isEmpty() && position==1){
            holder.mTimelineView.setMarker(mContext.getResources().getDrawable(R.drawable.not_delivered_drawable));
            holder.tvName.setTextColor(mContext.getResources().getColor(R.color.color_dark_grey));
            holder.dateText.setText(mArrayListManagerModel.getTrackingDataArrayList().get(0).getGoodsReceived());
        }else{
            holder.tvName.setTextColor(mContext.getResources().getColor(R.color.color_dark_blue));
            holder.mTimelineView.setMarker(mContext.getResources().getDrawable(R.drawable.delivered_drawable));
            holder.dateText.setText("NA");
        }
        if(checkStuffingDone == null || checkStuffingDone.isEmpty() && position==2){
            holder.mTimelineView.setMarker(mContext.getResources().getDrawable(R.drawable.not_delivered_drawable));
            holder.tvName.setTextColor(mContext.getResources().getColor(R.color.color_dark_grey));
            holder.dateText.setText(mArrayListManagerModel.getTrackingDataArrayList().get(0).getShipmentDate());
        }else{
            holder.tvName.setTextColor(mContext.getResources().getColor(R.color.color_dark_blue));
            holder.mTimelineView.setMarker(mContext.getResources().getDrawable(R.drawable.delivered_drawable));
            holder.dateText.setText("NA");
        }
        if(checkDelivered == null || checkDelivered.isEmpty()){
            if(position==3){
                holder.tvName.setTextColor(mContext.getResources().getColor(R.color.color_dark_grey));
                holder.mTimelineView.setMarker(mContext.getResources().getDrawable(R.drawable.not_delivered_drawable));
                holder.dateText.setText("NA");
            }
        }else{
            holder.tvName.setTextColor(mContext.getResources().getColor(R.color.color_dark_blue));
            holder.dateText.setText("NA");
        }
    }

    @Override
    public int getItemViewType(int position) {
        return TimelineView.getTimeLineViewType(position,getItemCount());
    }

    @Override
    public int getItemCount() {
        return data.length;
    }

    public class TimeLineViewHolder extends RecyclerView.ViewHolder {
        public TimelineView mTimelineView;
        TextView tvName,dateText;

        public TimeLineViewHolder(View itemView, int viewType) {
            super(itemView);
            mTimelineView = (TimelineView) itemView.findViewById(R.id.time_marker);
            tvName = (TextView)itemView.findViewById(R.id.tx_name);
            dateText = (TextView)itemView.findViewById(R.id.dateText);
            mTimelineView.initLine(viewType);
        }
    }
}
