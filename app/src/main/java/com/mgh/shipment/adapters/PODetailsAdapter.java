package com.mgh.shipment.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mgh.shipment.R;
import com.mgh.shipment.Utility.GeneralUtils;
import com.mgh.shipment.Utility.ScrollCallBack;
import com.mgh.shipment.beans.ArrayListManagerModel;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Akshay Thapliyal on 15-09-2016.
 */
public class PODetailsAdapter extends RecyclerView.Adapter<PODetailsAdapter.ShipmentViewHolder> {

    Context mContext;
    private int currentOpenPosition = -1;
    private ArrayListManagerModel mArrayListManagerModel;
    private ScrollCallBack scrollCallBack;

    public PODetailsAdapter(Context mContext, ArrayListManagerModel arrayListManagerModel) {
        this.mContext = mContext;
        this.mArrayListManagerModel = arrayListManagerModel;
        this.scrollCallBack =(ScrollCallBack)mContext;
    }

    @Override
    public ShipmentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.po_expandable_list, null);
        return new ShipmentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ShipmentViewHolder holder, final int position) {

        holder.tvBlNo.setText(mArrayListManagerModel.getPoDataArrayList().get(position).getBl_no());
        holder.tvBookingDate.setText(GeneralUtils.getDate(mArrayListManagerModel.getPoDataArrayList().get(position).getBooking_date()));
        holder.tvChargeWt.setText(mArrayListManagerModel.getPoDataArrayList().get(position).getCharge_wt());
        holder.tvGrDate.setText(GeneralUtils.getDate(mArrayListManagerModel.getPoDataArrayList().get(position).getGr_date()));
        holder.tvGrossWt.setText(mArrayListManagerModel.getPoDataArrayList().get(position).getGross_wt());
        holder.tvPODCode.setText(mArrayListManagerModel.getPoDataArrayList().get(position).getPod_code());
        holder.tvPOLCode.setText(mArrayListManagerModel.getPoDataArrayList().get(position).getPol_code());
        holder.tvShipmentDate.setText(GeneralUtils.getDate(mArrayListManagerModel.getPoDataArrayList().get(position).getShipment_date()));
        holder.tvShipperName.setText(mArrayListManagerModel.getPoDataArrayList().get(position).getBuyerName());
        holder.tvPONo.setText(mArrayListManagerModel.getPoDataArrayList().get(position).getPo_no());
        holder.tvTotalVol.setText(mArrayListManagerModel.getPoDataArrayList().get(position).getTot_volume());
        holder.docNo.setText(mArrayListManagerModel.getPoDataArrayList().get(position).getDocument_no());
        holder.containerNo.setText(mArrayListManagerModel.getPoDataArrayList().get(position).getContainer_no());
        holder.containerType.setText(mArrayListManagerModel.getPoDataArrayList().get(position).getContainer_type());
        holder.tvAta.setText(getConvertedDate(mArrayListManagerModel.getPoDataArrayList().get(position).getAta()).toString());
        holder.tvAtd.setText(getConvertedDate(mArrayListManagerModel.getPoDataArrayList().get(position).getAtd()).toString());
        holder.tvEta.setText(getConvertedDate(mArrayListManagerModel.getPoDataArrayList().get(position).getEta()).toString());
        holder.tvETD.setText(getConvertedDate(mArrayListManagerModel.getPoDataArrayList().get(position).getEtd()).toString());
        holder.tvPOStatus.setText(getPOStatus(position));
        holder.tvExceptionStatus.setText(getExceptionStatus(position));

        holder.childLinearLayout.setVisibility(View.GONE);
        holder.expandArrow.setImageResource((R.drawable.ic_arrow_up));

        holder.topMainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.childLinearLayout.getVisibility() == View.VISIBLE){
                    holder.childLinearLayout.setVisibility(View.GONE);
                }else{
                    holder.childLinearLayout.setVisibility(View.VISIBLE);
                    currentOpenPosition = position;
                    notifyDataSetChanged();
                    scrollCallBack.scrollToClickedPosition(position);
                }
            }
        });

        if (currentOpenPosition == position) {
            holder.childLinearLayout.setVisibility(View.VISIBLE);
            holder.expandArrow.setImageResource((R.drawable.ic_arrow_down));
        }
    }

    @Override
    public int getItemCount() {
        return mArrayListManagerModel.getInstance().getPoDataArrayList().size();
    }

    private String getPOStatus(int position) {
        String status = "";

        if (!mArrayListManagerModel.getPoDataArrayList().get(position).getBooking_date().equals("null")) {
            status = "ORDER BOOKED";
            if (!mArrayListManagerModel.getPoDataArrayList().get(position).getGr_date().equals("null")) {
                status = "CARGO RECEIVED";
                if (!mArrayListManagerModel.getPoDataArrayList().get(position).getShipment_date().equals("null")) {
                    status = "STUFFING DONE";
                    if (!mArrayListManagerModel.getPoDataArrayList().get(position).getEtd().equals("null")) {
                        Date toDate = new Date();
                        //System.out.println("DATE1:: " + );
                        if(convertDate(mArrayListManagerModel.getPoDataArrayList().get(position).getEtd()).compareTo(toDate)<0){
                            status = "CARGO DEPARTED";
                        }

                    }
                    if (!mArrayListManagerModel.getPoDataArrayList().get(position).getEta().equals("null")) {
                        Date toDate = new Date();
                        if(convertDate(mArrayListManagerModel.getPoDataArrayList().get(position).getEta()).compareTo(toDate)<0){
                            status = "CARGO ARRIVED";
                        }

                    }
                }
            }
        }
        return status;
    }

    private String getConvertedDate(String strDate) {

        if(strDate.equals("null")){
            return "";

        }else {
            String dateFormat = ""; //Fri Mar 20 20:44:49 CET 2015

            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            long mDateLongValue = Long.parseLong(strDate);
            Date mDate = new Date(mDateLongValue);
            dateFormat = formatter.format(mDate);

            return dateFormat;
        }
    }

    private Date convertDate(String strDate) {
        String dateFormat = strDate; //Fri Mar 20 20:44:49 CET 2015

        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        long mDateLongValue = Long.parseLong(strDate);
        Date mDate = new Date(mDateLongValue);
        dateFormat = formatter.format(mDate);

        return mDate;
    }

    private String getExceptionStatus(int position) {
        String status = "";

        if (!mArrayListManagerModel.getPoDataArrayList().get(position).getEta().equals("null")) {

            if (!mArrayListManagerModel.getPoDataArrayList().get(position).getAta().equals("null")) {
                if (mArrayListManagerModel.getPoDataArrayList().get(position).getAta().compareTo(mArrayListManagerModel.getPoDataArrayList().get(position).getEta()) > 0) {
                    status = "DELAYED";
                }
                if (mArrayListManagerModel.getPoDataArrayList().get(position).getAta().compareTo(mArrayListManagerModel.getPoDataArrayList().get(position).getEta()) < 0) {
                    status = "ADVANCE";
                }
                if (mArrayListManagerModel.getPoDataArrayList().get(position).getAta().compareTo(mArrayListManagerModel.getPoDataArrayList().get(position).getEta()) == 0) {
                    status = "ON-TIME";
                }
            } else {
                if (!mArrayListManagerModel.getPoDataArrayList().get(position).getFeta().equals("null")) {

                    if (mArrayListManagerModel.getPoDataArrayList().get(position).getFeta().compareTo(mArrayListManagerModel.getPoDataArrayList().get(position).getEta()) > 0) {
                        status = "DELAYED";
                    }
                    if (mArrayListManagerModel.getPoDataArrayList().get(position).getFeta().compareTo(mArrayListManagerModel.getPoDataArrayList().get(position).getEta()) < 0) {
                        status = "ADVANCE";
                    }
                    if (mArrayListManagerModel.getPoDataArrayList().get(position).getFeta().compareTo(mArrayListManagerModel.getPoDataArrayList().get(position).getEta()) == 0) {
                        status = "ON-TIME";
                    }
                } else {
                    status = "NOT AVAILABLE";
                }
            }

        } else {

            if (!mArrayListManagerModel.getPoDataArrayList().get(position).getEtd().equals("null")) {
                if (mArrayListManagerModel.getPoDataArrayList().get(position).getAtd() != null) {
                    if (mArrayListManagerModel.getPoDataArrayList().get(position).getAtd().compareTo(mArrayListManagerModel.getPoDataArrayList().get(position).getEtd()) > 0) {
                        status = "DELAYED";
                    }
                    if (mArrayListManagerModel.getPoDataArrayList().get(position).getAtd().compareTo(mArrayListManagerModel.getPoDataArrayList().get(position).getEtd()) < 0) {
                        status = "ADVANCE";
                    }
                    if (mArrayListManagerModel.getPoDataArrayList().get(position).getAtd().compareTo(mArrayListManagerModel.getPoDataArrayList().get(position).getEtd()) == 0) {
                        status = "ON-TIME";
                    }

                } else {

                    if (!mArrayListManagerModel.getPoDataArrayList().get(position).getFetd().equals("null")) {
                        if (mArrayListManagerModel.getPoDataArrayList().get(position).getFetd().compareTo(mArrayListManagerModel.getPoDataArrayList().get(position).getEtd()) > 0) {
                            status = "DELAYED";
                        }
                        if (mArrayListManagerModel.getPoDataArrayList().get(position).getFetd().compareTo(mArrayListManagerModel.getPoDataArrayList().get(position).getEtd()) < 0) {
                            status = "ADVANCE";
                        }
                        if (mArrayListManagerModel.getPoDataArrayList().get(position).getFetd().compareTo(mArrayListManagerModel.getPoDataArrayList().get(position).getEtd()) == 0) {
                            status = "ON-TIME";
                        }
                    } else {
                        status = "NOT AVAILABLE";
                    }
                }

            } else {
                status = "NOT AVAILABLE";
            }

        }


        return status;

    }


    public class ShipmentViewHolder extends RecyclerView.ViewHolder {

        public TextView tvPONo, tvShipperName, tvBlNo;
        public ImageView expandArrow;
        public TextView tvBLDate,docNo, containerNo,tvPOStatus, tvExceptionStatus, tvEta, tvETD, tvAtd, tvAta, containerType,tvBookingDate, tvPOLCode, tvPODCode, tvGrossWt, tvChargeWt,
                tvTotalVol, tvGrDate, tvShipmentDate;
        private LinearLayout childLinearLayout;
        private RelativeLayout topMainLayout;
        public ShipmentViewHolder(View itemView) {
            super(itemView);
            topMainLayout = (RelativeLayout)itemView.findViewById(R.id.topMainLayout);
            tvPONo = (TextView)itemView.findViewById(R.id.tvPONo);
            tvShipperName = (TextView)itemView.findViewById(R.id.tvShipperName);
            tvBlNo = (TextView)itemView.findViewById(R.id.tvBlNo);
            childLinearLayout = (LinearLayout)itemView.findViewById(R.id.childLinearLayout);
            expandArrow = (ImageView) itemView.findViewById(R.id.expandArrow);
            tvBLDate = (TextView)itemView.findViewById(R.id.tvBLDate);
            tvBookingDate = (TextView)itemView.findViewById(R.id.tvBookingDate);
            tvPOLCode = (TextView)itemView.findViewById(R.id.tvPOLCode);
            tvPODCode = (TextView)itemView.findViewById(R.id.tvPODCode);
            tvGrossWt = (TextView)itemView.findViewById(R.id.tvGrossWt);
            tvChargeWt = (TextView)itemView.findViewById(R.id.tvChargeWt);
            tvTotalVol = (TextView)itemView.findViewById(R.id.tvTotalVol);
            tvGrDate = (TextView)itemView.findViewById(R.id.tvGrDate);
            tvShipmentDate = (TextView)itemView.findViewById(R.id.tvShipmentDate);
            containerNo = (TextView)itemView.findViewById(R.id.containerNo);
            containerType = (TextView)itemView.findViewById(R.id.containerType);
            docNo = (TextView)itemView.findViewById(R.id.docNo);
            tvEta = (TextView) itemView.findViewById(R.id.etaValue);
            tvETD = (TextView) itemView.findViewById(R.id.etdValue);
            tvAtd = (TextView) itemView.findViewById(R.id.atdValue);
            tvAta = (TextView) itemView.findViewById(R.id.ataValue);
            tvExceptionStatus = (TextView) itemView.findViewById(R.id.exceptionStatusValue);
            tvPOStatus = (TextView) itemView.findViewById(R.id.poStatusValue);
        }
    }
}
