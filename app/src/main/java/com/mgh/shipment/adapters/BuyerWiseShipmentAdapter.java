package com.mgh.shipment.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mgh.shipment.R;
import com.mgh.shipment.Utility.AppConstants;
import com.mgh.shipment.beans.ArrayListManagerModel;

/**
 * Created by Amit Sharma on 10-08-2016.
 */
public class BuyerWiseShipmentAdapter extends RecyclerView.Adapter<BuyerWiseShipmentAdapter.BuyerWiseShipment> {

    Context mContext;
    private ArrayListManagerModel mArrayListManagerModel ;
    private String mType;

    public BuyerWiseShipmentAdapter(Context mContext,String type, ArrayListManagerModel arrayListManagerModel) {
        this.mContext = mContext;
        this.mArrayListManagerModel = arrayListManagerModel;
        this.mType = type;
    }

    @Override
    public BuyerWiseShipment onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.buyer_wise_row, null);
        return new BuyerWiseShipment(view);
    }

    @Override
    public void onBindViewHolder(BuyerWiseShipment holder, int position) {

        if(mType.equalsIgnoreCase(AppConstants.TOTAL_SHIPMENT)){
            holder.buyerName.setText(mArrayListManagerModel.getBuyerWiseDataArrayList().get(position).getBuyerName());
            holder.shipmentPercentage.setText(getRoundValue(mArrayListManagerModel.getBuyerWiseDataArrayList().get(position).getTotalShipmentPerct()));
            holder.totalShipment.setText(mArrayListManagerModel.getBuyerWiseDataArrayList().get(position).getTotalShipment());
        }else if(mType.equalsIgnoreCase(AppConstants.TOTAL_CBM)){
            holder.buyerName.setText(mArrayListManagerModel.getBuyerWiseCBMDataArrayList().get(position).getBuyerName());
            holder.shipmentPercentage.setText(getRoundValue(mArrayListManagerModel.getBuyerWiseCBMDataArrayList().get(position).getTotalCBMPerc()));
            holder.totalShipment.setText(mArrayListManagerModel.getBuyerWiseCBMDataArrayList().get(position).getTotalCBM());
        }else if(mType.equalsIgnoreCase(AppConstants.TOTAL_GWT)){
            holder.buyerName.setText(mArrayListManagerModel.getBuyerWiseGWTDataArrayList().get(position).getBuyerName());
            holder.shipmentPercentage.setText(getRoundValue(mArrayListManagerModel.getBuyerWiseGWTDataArrayList().get(position).getTotalGWTPerc()));
            holder.totalShipment.setText(mArrayListManagerModel.getBuyerWiseGWTDataArrayList().get(position).getTotalGWT());
        }
    }

    private String getRoundValue(String value){
        double doubleValue = Double.parseDouble(value);
            int scale = (int) Math.pow(10, 1);
            return String.valueOf((double) Math.round(doubleValue * scale) / scale);

    }


    @Override
    public int getItemCount() {
        int size = 0;
        if(mType.equalsIgnoreCase(AppConstants.TOTAL_SHIPMENT)){
          size = mArrayListManagerModel.getBuyerWiseDataArrayList().size();
        }else if(mType.equalsIgnoreCase(AppConstants.TOTAL_CBM)){
            size = mArrayListManagerModel.getBuyerWiseCBMDataArrayList().size();
        }else if(mType.equalsIgnoreCase(AppConstants.TOTAL_GWT)){
            size = mArrayListManagerModel.getBuyerWiseGWTDataArrayList().size();
        }
        return size;
    }

    public class BuyerWiseShipment extends RecyclerView.ViewHolder {
        private TextView buyerName,shipmentPercentage,totalShipment;
        public BuyerWiseShipment(View itemView) {
            super(itemView);

            buyerName = (TextView)itemView.findViewById(R.id.buyerName);
            shipmentPercentage = (TextView)itemView.findViewById(R.id.shipmentPercentage);
            totalShipment = (TextView)itemView.findViewById(R.id.totalShipment);
        }
    }
}
