package com.mgh.shipment.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mgh.shipment.R;
import com.mgh.shipment.beans.ArrayListManagerModel;

/**
 * Created by Akshay Thapliyal on 09-08-2016.
 */
public class TopShipmentAdapter extends RecyclerView.Adapter<TopShipmentAdapter.TopShipmentHolder> {

    public class TopShipmentHolder extends RecyclerView.ViewHolder {
        private TextView pod, totalGwt, totalShipments, totalCbm;
        public TopShipmentHolder(View itemView) {
            super(itemView);
            pod = (TextView)itemView.findViewById(R.id.podValueTV);
            totalGwt = (TextView)itemView.findViewById(R.id.gwtValueTV);
            totalShipments = (TextView)itemView.findViewById(R.id.shipmentValueTV);
            totalCbm = (TextView)itemView.findViewById(R.id.cbmValueTV);
        }
    }

    private Context context;
    private ArrayListManagerModel mArrayListManagerModel;

    public TopShipmentAdapter(Context context, ArrayListManagerModel arrayListManagerModel) {
        this.context = context;
        this.mArrayListManagerModel = arrayListManagerModel;
    }

    @Override
    public TopShipmentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.top_shipment_row, null);

        return new TopShipmentHolder(view);
    }

    @Override
    public void onBindViewHolder(TopShipmentHolder holder, int position) {
        holder.totalShipments.setText(mArrayListManagerModel.getTop5ShipmentDataArrayList().get(position).getTotalShipment());
        holder.totalCbm.setText(mArrayListManagerModel.getTop5ShipmentDataArrayList().get(position).getTotalCBM());
        holder.totalGwt.setText(mArrayListManagerModel.getTop5ShipmentDataArrayList().get(position).getTotalGWT());
        holder.pod.setText(mArrayListManagerModel.getTop5ShipmentDataArrayList().get(position).getPortOfDischarge());

    }

    @Override
    public int getItemCount() {
        return mArrayListManagerModel.getTop5ShipmentDataArrayList().size();
    }
}
