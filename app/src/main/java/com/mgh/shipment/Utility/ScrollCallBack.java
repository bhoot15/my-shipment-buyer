package com.mgh.shipment.Utility;

/**
 * Created by Akshay Thapliyal on 16-09-2016.
 */
public interface ScrollCallBack {

    public void scrollToClickedPosition(int position);
}
