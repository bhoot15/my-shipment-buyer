package com.mgh.shipment.Utility;

/**
 * Created by Akshay Thapliyal on 16-08-2016.
 */
public class AppConstants {

    public static final String TOTAL_SHIPMENT = "totalShipment";
    public static final String TOTAL_CBM = "totalCbm";
    public static final String TOTAL_GWT = "totalGwt";
}
