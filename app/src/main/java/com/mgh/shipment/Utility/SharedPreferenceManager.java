package com.mgh.shipment.Utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;

/**
 * Created by admin on 29-02-2016.
 */
public class SharedPreferenceManager {

    private static final String MY_PREFS_NAME ="info";

    public static void saveUserCode(String userCode, Context context){
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE).edit();
        editor.putString("userCode", userCode);
        editor.commit();
    }

    public static String getUserCode(Context context){
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
        return prefs.getString("userCode", "");
    }

    public static void setUserLoggedIn(boolean response, Context context){
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE).edit();
        editor.putBoolean("userlogin", response);
        editor.commit();
    }

    public static boolean isUserLoggedIn(Context context){
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
        return prefs.getBoolean("userlogin", false);
    }

    public static void setToDate(String response, Context context){
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE).edit();
        editor.putString("toDate", response);
        editor.commit();
    }

    public static String getToDate(Context context){
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
        return prefs.getString("toDate", "");
    }

    public static void setFromDate(String response, Context context){
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE).edit();
        editor.putString("fromDate", response);
        editor.commit();
    }

    public static String getFromDate(Context context){
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
        return prefs.getString("fromDate", "");
    }

    public static void saveSalesOrg(String response, Context context){
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE).edit();
        editor.putString("salesOrg", response);
        editor.commit();
    }

    public static String getSalesOrg(Context context){
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
        return prefs.getString("salesOrg", "");
    }

    public static void saveSalesOrgCode(String response, Context context){
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE).edit();
        editor.putString("salesOrgCode", response);
        editor.commit();
    }

    public static String getSalesOrgCode(Context context){
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
        return prefs.getString("salesOrgCode", "");
    }

    public static void saveDistChannel(String response, Context context){
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE).edit();
        editor.putString("distChannel", response);
        editor.commit();
    }

    public static String getDistChannel(Context context){
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
        return prefs.getString("distChannel","");
    }

    public static void saveDivision(String response, Context context){
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE).edit();
        editor.putString("division", response);
        editor.commit();
    }

    public static String getDivision(Context context){
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
        return prefs.getString("division","");
    }

    public static void clearCredentials(Context context){
        SharedPreferences preferences = (SharedPreferences)context.getSharedPreferences(MY_PREFS_NAME,0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
    }

}
