package com.mgh.shipment.Utility;

import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import com.mgh.shipment.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Amit Sharma on 11-08-2016.
 */
public class GeneralUtils

{
    static Context mContext;

    public GeneralUtils() {
        super();
    }

    public GeneralUtils(Context mContext) {
        this.mContext = mContext;
    }

    public void ActivateButton(Button view) {
        view.setBackgroundColor(mContext.getResources().getColor(R.color.color_dark_blue));
        view.setTextColor(Color.WHITE);
    }

    public void DeactivateButton(Button view) {
        view.setBackgroundColor(mContext.getResources().getColor(R.color.dull_whie));
        view.setTextColor(mContext.getResources().getColor(R.color.color_dark_blue));
    }

    public static boolean isNetworkAvailable(final Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public static void ScaleAnimate(View view, Context context) {
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.scale_up);
        view.startAnimation(animation);
    }

    public static String checkNullValue(String value){
        if(value.equals("null")){
            return "";
        }else{
            return value;
        }
    }

    public static String getDate(String dateString){
        System.out.println("DATE STRING::" + dateString);
        String newDateStr="";
        try{
            if(!(dateString.isEmpty()) && !(dateString.equalsIgnoreCase("NA")) && dateString!=null){
                SimpleDateFormat form = new SimpleDateFormat("yyyyMMdd");
                java.util.Date date = null;
                try
                {
                    date = form.parse(dateString);
                }
                catch (ParseException e)
                {

                    e.printStackTrace();
                }
                SimpleDateFormat postFormater = new SimpleDateFormat("MMM dd, yyyy");
                newDateStr = postFormater.format(date);
            }

        }catch (NullPointerException e){

        }


        return newDateStr;
    }
}
