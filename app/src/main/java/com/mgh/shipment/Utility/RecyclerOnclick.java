package com.mgh.shipment.Utility;

import android.view.View;

/**
 * Created by Amit Sharma on 09-08-2016.
 */
public interface RecyclerOnclick  {

    void onClick(View view, int position);

    void onLongClick(View view, int position);
}
