package com.mgh.shipment.Utility;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.TextView;

import com.mgh.shipment.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Akshay Thapliyal on 11-08-2016.
 */
public class CustomDateDialog extends Dialog implements View.OnClickListener {

    private Button cancelButton, applyButton, fromDateButton, toDatetButton;
    private CalendarView calendarView;
    Context context;
    private TextView selectedDate;
    private String fromDateStr, toDateStr;
    private String fromDate, toDate;
    private boolean isFromDate = false;
    DatePickerCallBack datePickerCallBack;

    public CustomDateDialog(Context context1, DatePickerCallBack datePickerCallBack) {
        super(context1);
        context = context1;
        this.datePickerCallBack = datePickerCallBack;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.date_picker_dialog);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = this.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        init();
        calenderInit();
        setDateOver();
    }

    public void init() {
        calendarView = (CalendarView) findViewById(R.id.calendarView);
        cancelButton = (Button) findViewById(R.id.cancel_btn);
        applyButton = (Button) findViewById(R.id.apply_btn);
        fromDateButton = (Button) findViewById(R.id.from_btn);
        toDatetButton = (Button) findViewById(R.id.to_btn);
        selectedDate = (TextView) findViewById(R.id.selected_date);
        fromDateButton.setOnClickListener(this);
        toDatetButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);
        applyButton.setOnClickListener(this);
        new GeneralUtils(context).ActivateButton(fromDateButton);
        new GeneralUtils(context).DeactivateButton(toDatetButton);
        isFromDate = true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.from_btn:
                new GeneralUtils(context).ActivateButton(fromDateButton);
                new GeneralUtils(context).DeactivateButton(toDatetButton);
                isFromDate = true;
                break;
            case R.id.to_btn:
                new GeneralUtils(context).ActivateButton(toDatetButton);
                new GeneralUtils(context).DeactivateButton(fromDateButton);
                isFromDate = false;
                break;
            case R.id.apply_btn:
                datePickerCallBack.SelectedDate(fromDate, toDate);
                dismiss();
                break;
            case R.id.cancel_btn:
                dismiss();
                break;
        }
    }

    private String getDate(String dateString) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println("converted date::" + convertedDate);
        return "43252";
    }

    public void calenderInit() {
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                try {
                    String month_ = "" + (month + 1);
                    String day = "" + dayOfMonth;
                    String dateStr = dayOfMonth + "-" + month_ + "-" + year;
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    Date date = simpleDateFormat.parse(dateStr);
                    String newDate = DateFormat.getDateInstance().format(date);
                    if (isFromDate) {
                        fromDateStr = newDate;
                        if (month < 10) {
                            month_ = "0" + month_;
                        }
                        if (dayOfMonth < 10) {
                            day = "0" + day;
                        }

                        fromDate = year + "" + month_ + "" + day;
                    } else {
                        toDateStr = newDate;

                        if (month < 10) {
                            month_ = "0" + month_;
                        }
                        if (dayOfMonth < 10) {
                            day = "0" + day;
                        }

                        toDate = year + "" + month_ + "" + day;
                    }
                    selectedDate.setText(fromDateStr + " To " + toDateStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void setDateOver() {
        fromDateStr = getThreeMonthBeforeDate();
        toDateStr = getCurrenDate();
        selectedDate.setText(fromDateStr + " To " + toDateStr);
    }

    public String getCurrenDate() {
        String currentDate = null;
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String Day = "";
        if (c.get(Calendar.DAY_OF_MONTH) < 10) {
            Day = "0" + c.get(Calendar.DAY_OF_MONTH);
        } else {
            Day = "0" + c.get(Calendar.DAY_OF_MONTH);
        }

        String Month = "";
        if ((c.get(Calendar.MONTH) + 1) < 10) {
            Month = "0" + (c.get(Calendar.MONTH) + 1);
        } else {
            Month = "" + (c.get(Calendar.MONTH) + 1);
        }
        String year = c.get(Calendar.YEAR) + "";
        fromDate = year + "" + Month + "" + Day;
        toDate = year + "" + Month + "" + Day;
        try {
            Date formattedDate = df.parse(Day + "-" + Month + "-" + year);
            currentDate = DateFormat.getDateInstance().format(formattedDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return currentDate;
    }

    public String getThreeMonthBeforeDate() {
        String currentDate = null;
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String Day = "";

        if (c.get(Calendar.DAY_OF_MONTH) < 1) {
            Day = "0" + c.get(Calendar.DAY_OF_MONTH);
        } else {
            Day = "" + c.get(Calendar.DAY_OF_MONTH);
        }

        String Month = "";
        if (( c.get(Calendar.MONTH) - 2 ) < 10) {
            Month = "0" + ( c.get(Calendar.MONTH) - 2 );
        } else {
            Month = "" + ( c.get(Calendar.MONTH) - 2 );
        }

        String year = c.get(Calendar.YEAR) + "";
        fromDate = year + "" + Month + "" + Day;
        toDate = year + "" + Month + "" + Day;
        try {
            Date formattedDate = df.parse(Day + "-" + Month + "-" + year);
            currentDate = DateFormat.getDateInstance().format(formattedDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return currentDate;
    }
}
