package com.mgh.shipment.Utility;

import javax.net.ssl.SSLPeerUnverifiedException;

/**
 * Created by Amit Sharma on 11-08-2016.
 */
public interface DatePickerCallBack {

    public void SelectedDate(String fromDate, String todate);
}
