package com.mgh.shipment.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mgh.shipment.R;
import com.mgh.shipment.activities.NewsAndEventsActivity;
import com.mgh.shipment.adapters.NewsEventsAdapter;

/**
 * Created by Akshay Thapliyal on 23-05-2016.
 */
public class NewsEventsFragment extends Fragment implements View.OnClickListener {

    private RecyclerView newsEventsRecyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private Toolbar toolbarId;
    private ImageView menuIcon;
    private TextView title;
    @Override
    public android.view.View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.news_events_fragment,container,false);

        init(view);
        title.setText(getActivity().getString(R.string.news_events_text));
        setListeners();
        setAdapter();
        return view;
    }

    private void init(View v){
        newsEventsRecyclerView = (RecyclerView)v.findViewById(R.id.newsEventsRecyclerView);
        toolbarId = (Toolbar)v.findViewById(R.id.toolbarId);
        menuIcon = (ImageView)toolbarId.findViewById(R.id.menuIcon);
        title = (TextView)toolbarId.findViewById(R.id.tvTitleToolbar);
    }

    private void setListeners(){

        menuIcon.setOnClickListener(this);
    }

    private void setAdapter(){

        newsEventsRecyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        newsEventsRecyclerView.setLayoutManager(layoutManager);
        newsEventsRecyclerView.setItemAnimator(new DefaultItemAnimator());

        NewsEventsAdapter newsEventsAdapter = new NewsEventsAdapter();
        newsEventsRecyclerView.setAdapter(newsEventsAdapter);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == menuIcon.getId()){
            ((NewsAndEventsActivity)getActivity()).clickEventSlide();
        }
    }
}
