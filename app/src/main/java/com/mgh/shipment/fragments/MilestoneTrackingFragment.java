package com.mgh.shipment.fragments;

import android.app.Fragment;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.mgh.shipment.R;
import com.mgh.shipment.activities.MilestoneTrackingActivity;
import com.mgh.shipment.adapters.ProductDetailsAdapter;
import com.mgh.shipment.adapters.ScheduleDetailAdapter;
import com.mgh.shipment.adapters.TimelineViewAdapter;
import com.mgh.shipment.beans.ArrayListManagerModel;

/**
 * Created by Akshay Thapliyal on 26-05-2016.
 */
public class MilestoneTrackingFragment extends Fragment implements View.OnClickListener {

    private RecyclerView timelineRecyclerView, productDetailsRecycler, scheduleRecycler;
    private RecyclerView.LayoutManager layoutManager;
    private ScrollView milestoneScrollView;
    private RelativeLayout bottomDetailsHeader, allDetailsLayout;
    private Toolbar toolbar;
    private ImageView menuIcon, packageArrow, scheduleArrow;
    private TextView tvTitleToolbar;
    private boolean isPackageOpen = true, isScheduleOpen = false;
    private RelativeLayout headerLayout, scheduleHeader;

    @Override
    public android.view.View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.milestone_tracking_fragment, container, false);

        init(view);
        setListeners();
        setAdapter();

        tvTitleToolbar.setText("Tracking Details");
        milestoneScrollView.fullScroll(ScrollView.FOCUS_UP);
        milestoneScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {

            @Override
            public void onScrollChanged() {

                Rect scrollBounds = new Rect();
                milestoneScrollView.getHitRect(scrollBounds);


            }
        });
        return view;
    }

    private void init(View v) {
        scheduleHeader = (RelativeLayout) v.findViewById(R.id.scheduleHeader);
        headerLayout = (RelativeLayout) v.findViewById(R.id.headerLayout);
        timelineRecyclerView = (RecyclerView) v.findViewById(R.id.timelineRecyclerView);
        productDetailsRecycler = (RecyclerView) v.findViewById(R.id.productDetailsRecycler);
        milestoneScrollView = (ScrollView) v.findViewById(R.id.milestoneScrollView);
        scheduleRecycler = (RecyclerView) v.findViewById(R.id.scheduleRecycler);
        //bottomDetailsHeader = (RelativeLayout)v.findViewById(R.id.bottomDetailsHeader);
        allDetailsLayout = (RelativeLayout) v.findViewById(R.id.allDetailsLayout);
        toolbar = (Toolbar) v.findViewById(R.id.toolbarId);
        menuIcon = (ImageView) toolbar.findViewById(R.id.menuIcon);
        tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvTitleToolbar);
        scheduleArrow = (ImageView) v.findViewById(R.id.scheduleArrow);
        packageArrow = (ImageView) v.findViewById(R.id.packageArrow);
    }

    private void setListeners() {
        menuIcon.setOnClickListener(this);
        headerLayout.setOnClickListener(this);
        scheduleHeader.setOnClickListener(this);
    }

    private void setAdapter() {

        timelineRecyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        timelineRecyclerView.setLayoutManager(layoutManager);
        timelineRecyclerView.setItemAnimator(new DefaultItemAnimator());
        TimelineViewAdapter timelineViewAdapter = new TimelineViewAdapter(getActivity(), ArrayListManagerModel.getInstance());
        timelineRecyclerView.setAdapter(timelineViewAdapter);

        //set Adapter for Product details
        productDetailsRecycler.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        productDetailsRecycler.setLayoutManager(layoutManager);
        productDetailsRecycler.setItemAnimator(new DefaultItemAnimator());
        ProductDetailsAdapter productDetailsAdapter = new ProductDetailsAdapter(getActivity(), ArrayListManagerModel.getInstance());
        productDetailsRecycler.setAdapter(productDetailsAdapter);

        //set Adapter for Product details
        scheduleRecycler.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        scheduleRecycler.setLayoutManager(layoutManager);
        scheduleRecycler.setItemAnimator(new DefaultItemAnimator());
        ScheduleDetailAdapter scheduleDetailAdapter = new ScheduleDetailAdapter(getActivity(), ArrayListManagerModel.getInstance());
        scheduleRecycler.setAdapter(scheduleDetailAdapter);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == menuIcon.getId()) {
            getActivity().finish();
        } else if (v.getId() == headerLayout.getId()) {
            if (isPackageOpen) {
                productDetailsRecycler.setVisibility(View.GONE);
                packageArrow.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down));
                isPackageOpen = false;
            } else {
                productDetailsRecycler.setVisibility(View.VISIBLE);
                packageArrow.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_up));
                isPackageOpen = true;

                if (isScheduleOpen) {
                    scheduleRecycler.setVisibility(View.GONE);
                    scheduleArrow.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down));
                    isScheduleOpen = false;
                }
            }

        } else if (v.getId() == scheduleHeader.getId()) {
            if (isScheduleOpen) {
                scheduleRecycler.setVisibility(View.GONE);
                scheduleArrow.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down));
                isScheduleOpen = false;
            } else {
                scheduleRecycler.setVisibility(View.VISIBLE);
                isScheduleOpen = true;
                scheduleArrow.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_up));
                if (isPackageOpen) {
                    productDetailsRecycler.setVisibility(View.GONE);
                    isPackageOpen = false;
                    packageArrow.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down));
                }
            }

        }
    }
}
