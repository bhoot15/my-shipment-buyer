package com.mgh.shipment.fragments;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.AxisValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.mgh.shipment.R;
import com.mgh.shipment.Utility.ApiUrls;
import com.mgh.shipment.Utility.AppConstants;
import com.mgh.shipment.Utility.AutoresizeTextView;
import com.mgh.shipment.Utility.CustomDateDialog;
import com.mgh.shipment.Utility.DatePickerCallBack;
import com.mgh.shipment.Utility.DayAxisValueFormatter;
import com.mgh.shipment.Utility.GeneralUtils;
import com.mgh.shipment.Utility.MyAxisValueFormatter;
import com.mgh.shipment.Utility.ProgressHUD;
import com.mgh.shipment.Utility.SharedPreferenceManager;
import com.mgh.shipment.Utility.XYMarkerView;
import com.mgh.shipment.activities.BuyerWiseShipmentActivity;
import com.mgh.shipment.activities.DashBoardActivity;
import com.mgh.shipment.activities.ShipmentDetailsActivity;
import com.mgh.shipment.activities.TopShipmentActivity;
import com.mgh.shipment.beans.ArrayListManagerModel;
import com.mgh.shipment.beans.BarChartData;
import com.mgh.shipment.beans.BuyerWiseCBMData;
import com.mgh.shipment.beans.BuyerWiseGWTData;
import com.mgh.shipment.beans.BuyerWiseShipmentData;
import com.mgh.shipment.beans.ChildDetailsData;
import com.mgh.shipment.beans.DashboardData;
import com.mgh.shipment.beans.ShipmentDetailsData;
import com.mgh.shipment.beans.Top5ShipmentData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Akshay Thapliyal on 26-07-2016.
 */
public class DashboardFragment extends Fragment implements View.OnClickListener {
    private PieChart pieChart;
    private BarChart barChart;
    private Typeface tf;
    private ImageView menuIcon;
    private Button topShipmentsBtn;
    private ProgressDialog dialog;
    private RelativeLayout totalGwtLayout, totalShipmentLayout, totalCbmLayout;
    private TextView tvTitleToolbar;
    private AutoresizeTextView topShipmentTextView, cbmTextView, gwtTextView;
    private Toolbar toolbar;
    private ImageView clockIcon;
    private DatePickerCallBack datePickerCallBack;
    private float mParties[] = {30, 20};
    private static final int[] JOYFUL_COLORS = {
            Color.rgb(217, 80, 138), Color.rgb(254, 149, 7)
    };
    private int[] MATERIAL_COLORS = {
            ColorTemplate.rgb("#2ecc71"), ColorTemplate.rgb("#f1c40f"), ColorTemplate.rgb("#e74c3c"), ColorTemplate.rgb("#3498db"),
            ColorTemplate.rgb("#51c51f"), ColorTemplate.rgb("#ae4c9c"), ColorTemplate.rgb("#ff9800"),
            ColorTemplate.rgb("#aacc00"), ColorTemplate.rgb("#00c400"), ColorTemplate.rgb("#ee4cec"), ColorTemplate.rgb("#5098db"),
            ColorTemplate.rgb("#30ccaa"), ColorTemplate.rgb("#90c4aa"), ColorTemplate.rgb("#cd4c30"), ColorTemplate.rgb("#549859")
    };


    @Nullable
    @Override
    public android.view.View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        init(view);
        setListeners();
        //setButtonAnimation();
        //call dashboard api the set pie chart and bar chart
        tvTitleToolbar.setText(getActivity().getResources().getString(R.string.dashboard_text));
        datePickerCallback();
        setDates();
        if (GeneralUtils.isNetworkAvailable(getActivity())) {
            callDashBoardApi();
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.internet_connection_msg), Toast.LENGTH_SHORT).show();
        }
        return view;
    }

    private void init(View view) {
        toolbar = (Toolbar) view.findViewById(R.id.toolbarId);
        menuIcon = (ImageView) view.findViewById(R.id.menuIcon);
        pieChart = (PieChart) view.findViewById(R.id.peiCart);
        barChart = (BarChart) view.findViewById(R.id.barChart);
        topShipmentsBtn = (Button) view.findViewById(R.id.topShipmentsBtn);
        topShipmentTextView = (AutoresizeTextView) view.findViewById(R.id.topShipmentTextView);
        cbmTextView = (AutoresizeTextView) view.findViewById(R.id.cbmTextView);
        gwtTextView = (AutoresizeTextView) view.findViewById(R.id.gwtTextView);
        clockIcon = (ImageView) view.findViewById(R.id.clock_icon);
        tvTitleToolbar = (TextView) view.findViewById(R.id.tvTitleToolbar);
        totalGwtLayout = (RelativeLayout) view.findViewById(R.id.totalGwtLayout);
        totalCbmLayout = (RelativeLayout) view.findViewById(R.id.totalCbmLayout);
        totalShipmentLayout = (RelativeLayout) view.findViewById(R.id.totalShipmentLayout);
    }

    private void setListeners() {
        menuIcon.setOnClickListener(this);
        topShipmentsBtn.setOnClickListener(this);
        clockIcon.setOnClickListener(this);
        totalGwtLayout.setOnClickListener(this);
        totalCbmLayout.setOnClickListener(this);
        totalShipmentLayout.setOnClickListener(this);
    }

    private void setDates() {
        String fromDate = "", toDate = "";
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String Day = "";

        if (c.get(Calendar.DAY_OF_MONTH) < 10) {
            Day = "0" + c.get(Calendar.DAY_OF_MONTH);
        } else {
            Day = "" + c.get(Calendar.DAY_OF_MONTH);
        }

        String Month = "";
        if ((c.get(Calendar.MONTH) - 2) < 10) {
            Month = "0" + (c.get(Calendar.MONTH) - 2);
        } else {
            Month = "" + (c.get(Calendar.MONTH) - 2);
        }

        String year = c.get(Calendar.YEAR) + "";
        fromDate = year + "" + Month + "" + Day;
        if ((c.get(Calendar.MONTH) + 1) < 10) {
            Month = "0" + (c.get(Calendar.MONTH) + 1);
        } else {
            Month = "" + (c.get(Calendar.MONTH) + 1);
        }
        toDate = year + "" + Month + "" + Day;

        SharedPreferenceManager.setFromDate(fromDate, getActivity());
        SharedPreferenceManager.setToDate(toDate, getActivity());
    }

    private void setButtonAnimation(){
    /*    GeneralUtils.ScaleAnimate(topShipmentsBtn);
        GeneralUtils.ScaleAnimate(totalCbmLayout);
        GeneralUtils.ScaleAnimate(totalGwtLayout);
        GeneralUtils.ScaleAnimate(totalShipmentLayout);*/
    }

    private void callDashBoardApi() {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("shipperNo", SharedPreferenceManager.getUserCode(getActivity()));
            jsonObject.put("division", SharedPreferenceManager.getDivision(getActivity()));
            jsonObject.put("distChannel", SharedPreferenceManager.getDistChannel(getActivity()));
            jsonObject.put("bookingDateFrom", SharedPreferenceManager.getFromDate(getActivity()));
            jsonObject.put("bookingDateTo", SharedPreferenceManager.getToDate(getActivity()));
            /*jsonObject.put("shipperNo", "1000000037");
            jsonObject.put("division", "SE");
            jsonObject.put("distChannel", "EX");
            jsonObject.put("bookingDateFrom", "20110210");
            jsonObject.put("bookingDateTo", "20160802");*/
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("JSON::" + jsonObject);

        dialog = new ProgressDialog(getActivity());
        dialog.setMessage(getResources().getString(R.string.please_wait_text));
        dialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                ApiUrls.DASHBOARD_URL, jsonObject, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

                try {
                    // Parsing json object response
                    String message = response.getString("message");
                    DashboardData dashboardData = new DashboardData();
                    ArrayListManagerModel arrayListManagerModel = ArrayListManagerModel.getInstance();
                    ArrayList<DashboardData> dashboardDataArrayList = new ArrayList<>();
                    if (message.equalsIgnoreCase("successful")) {
                        dashboardData.setTotalShipmentCount(response.getString("totalShipmentCount"));
                        dashboardData.setTotalCbmCount(response.getString("totalCBMCount"));
                        dashboardData.setTotalGwtCount(response.getString("totalGWTCount"));
                        dashboardData.setPendingPercentage(response.getString("blPendingPercentage"));
                        dashboardData.setReleasePercentage(response.getString("blReleasePercentage"));
                        dashboardDataArrayList.add(dashboardData);
                        arrayListManagerModel.setDashboardDataArrayList(dashboardDataArrayList);

                        //get bar graph data

                        JSONArray jsonArray = response.getJSONArray("lstSalesOrgWiseShipCount");
                        int length = jsonArray.length();
                        if (length > 0) {
                            ArrayList<BarChartData> barChartDataArrayList = new ArrayList<>();
                            for (int i = 0; i < length; i++) {
                                BarChartData barChartData = new BarChartData();
                                barChartData.setSalesOrgName(jsonArray.getJSONObject(i).getString("salesOrgName"));
                                barChartData.setShipmentCount(jsonArray.getJSONObject(i).getString("shipmentCount"));
                                barChartData.setSalesOrg(jsonArray.getJSONObject(i).getString("salesOrg"));
                                barChartDataArrayList.add(barChartData);
                            }

                            arrayListManagerModel.setBarChartDataArrayList(barChartDataArrayList);
                            setBarChart();
                        }
                        setGraphData();
                        setPieChart();
                    } else {
                        Toast.makeText(getActivity(),
                                getResources().getString(R.string.no_data),
                                Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(),
                            getResources().getString(R.string.error_fetching),
                            Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(),
                        getResources().getString(R.string.error_fetching), Toast.LENGTH_SHORT).show();
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

        // Adding request to request queue
        requestQueue.add(jsonObjReq);

    }

    private void setGraphData() {
        ArrayListManagerModel arrayListManagerModel = ArrayListManagerModel.getInstance();
        topShipmentTextView.setText("" + Math.round(Double.parseDouble(arrayListManagerModel.getDashboardDataArrayList().get(0).getTotalShipmentCount())));
        gwtTextView.setText("" + Math.round(Double.parseDouble(arrayListManagerModel.getDashboardDataArrayList().get(0).getTotalGwtCount())));
        cbmTextView.setText("" + Math.round(Double.parseDouble(arrayListManagerModel.getDashboardDataArrayList().get(0).getTotalCbmCount())));
    }

    private void setBarChart() {
        //barChart.setOnChartValueSelectedListener(this);

        barChart.setDrawBarShadow(false);
        barChart.setDrawValueAboveBar(true);

        barChart.setDescription("");

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        barChart.setMaxVisibleValueCount(60);

        // scaling can now only be done on x- and y-axis separately
        barChart.setPinchZoom(false);

        barChart.setDrawGridBackground(false);
        // mChart.setDrawYLabels(false);

        AxisValueFormatter xAxisFormatter = new DayAxisValueFormatter(barChart);

        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTypeface(tf);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(7);
        // xAxis.setValueFormatter(xAxisFormatter);

        AxisValueFormatter custom = new MyAxisValueFormatter();

        YAxis leftAxis = barChart.getAxisLeft();
        leftAxis.setTypeface(tf);
        leftAxis.setLabelCount(8, false);
        leftAxis.setValueFormatter(custom);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinValue(0f); // this replaces setStartAtZero(true)

        YAxis rightAxis = barChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setTypeface(tf);
        rightAxis.setLabelCount(8, false);
        rightAxis.setValueFormatter(custom);
        rightAxis.setSpaceTop(15f);
        rightAxis.setAxisMinValue(0f); // this replaces setStartAtZero(true)

        // barChart.setOnChartValueSelectedListener(this);

        ArrayListManagerModel arrayListManagerModel = ArrayListManagerModel.getInstance();
        int length = arrayListManagerModel.getBarChartDataArrayList().size();
        String legends[] = new String[length];
        int legendColors[] = new int[length];
        for (int i = 0; i < length; i++) {
            legends[i] = arrayListManagerModel.getBarChartDataArrayList().get(i).getSalesOrgName();
            legendColors[i] = MATERIAL_COLORS[i];
        }

        Legend l = barChart.getLegend();
        l.setPosition(Legend.LegendPosition.ABOVE_CHART_CENTER);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(9f);
        l.setTextSize(11f);
        l.setXEntrySpace(4f);
        l.setCustom(legendColors, legends);

        barChart.setMarkerView(new XYMarkerView(getActivity(), xAxisFormatter));

        setBarData(length, 100);
    }

    private void setBarData(int count, float range) {

        ArrayListManagerModel arrayListManagerModel = ArrayListManagerModel.getInstance();
        int noOfBars = arrayListManagerModel.getBarChartDataArrayList().size();

        float start = 0f;

        barChart.getXAxis().setAxisMinValue(start);
        barChart.getXAxis().setAxisMaxValue(start + count + 2);

        ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();

        for (int i = 0; i < noOfBars; i++) {
            yVals1.add(new BarEntry(i + 1f, Integer.parseInt(arrayListManagerModel.getBarChartDataArrayList().get(i).getShipmentCount())));
        }

     /* yVals1.add(new BarEntry(0 + 1f, 55));
        yVals1.add(new BarEntry(1 + 1f, 40));
        yVals1.add(new BarEntry(2 + 1f, 90));
        yVals1.add(new BarEntry(3 + 1f, 60));*/

        BarDataSet set1;

        if (barChart.getData() != null && barChart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) barChart.getData().getDataSetByIndex(0);
            set1.setValues(yVals1);
            barChart.getData().notifyDataChanged();
            barChart.notifyDataSetChanged();
        } else {
            set1 = new BarDataSet(yVals1, "");
            set1.setColors(ColorTemplate.MATERIAL_COLORS);

            ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
            data.setValueTypeface(tf);
            data.setBarWidth(0.9f);

            barChart.setData(data);

            // add a selection listener
            barChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
                @Override
                public void onValueSelected(Entry e, Highlight h) {

                    System.out.println("Bar:" + h.getX());
                    String salesOrg = ArrayListManagerModel.getInstance().getBarChartDataArrayList().get((int) (h.getX() - 1)).getSalesOrg();
                    callGraphDetailsApi(ApiUrls.BAR_CHART_DETAILS_URL, "bar", salesOrg);
                }

                @Override
                public void onNothingSelected() {

                }
            });

        }
    }


    private void setPieChart() {

        tf = Typeface.createFromAsset(getActivity().getAssets(),
                "quicksandbold.otf");

        pieChart.setUsePercentValues(true);
        pieChart.setDescription("");
        pieChart.setExtraOffsets(5, 10, 5, 5);

        pieChart.setDragDecelerationFrictionCoef(0.95f);

        pieChart.setCenterTextTypeface(tf);
        // pieChart.setCenterText(generateCenterSpannableText());

        pieChart.setDrawHoleEnabled(true);
        pieChart.setHoleColor(Color.WHITE);

        pieChart.setTransparentCircleColor(Color.WHITE);
        pieChart.setTransparentCircleAlpha(110);

        pieChart.setHoleRadius(0f);
        pieChart.setTransparentCircleRadius(0f);

        pieChart.setDrawCenterText(true);

        pieChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        pieChart.setRotationEnabled(true);
        pieChart.setHighlightPerTapEnabled(true);

        // mChart.setUnit(" â‚¬");
        // mChart.setDrawUnitsInChart(true);


        setData(2, 100);

        pieChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        // mChart.spin(2000, 0, 360);

        Legend l = pieChart.getLegend();
        l.setPosition(Legend.LegendPosition.ABOVE_CHART_RIGHT);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);
        l.setCustom(this.JOYFUL_COLORS, new String[]{"Pending", "Released"});

        // entry label styling
        pieChart.setEntryLabelColor(Color.BLACK);
        pieChart.setEntryLabelTypeface(tf);
        pieChart.setEntryLabelTextSize(12f);

    }

    private void setData(int count, float range) {

        float mult = range;
        ArrayListManagerModel arrayListManagerModel = ArrayListManagerModel.getInstance();
        float pendingPercentage = Float.parseFloat(arrayListManagerModel.getDashboardDataArrayList().get(0).getPendingPercentage());
        float releasedPercentage = Float.parseFloat(arrayListManagerModel.getDashboardDataArrayList().get(0).getReleasePercentage());

        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        entries.add(new PieEntry((float) ((pendingPercentage)), "Pending"));
        entries.add(new PieEntry((float) ((releasedPercentage)), "Released"));

        PieDataSet dataSet = new PieDataSet(entries, "BL Status");
        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);

        // add a lot of colors
        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        dataSet.setLabel("Pending \n Released");
        //dataSet.setSelectionShift(0f);
//        PieData dat = new PieData(list,dataSet);
        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);
        data.setValueTypeface(tf);
        pieChart.setData(data);
        pieChart.setHighlightPerTapEnabled(true);
        pieChart.setTouchEnabled(true);
        pieChart.setClickable(true);
        pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                if (GeneralUtils.isNetworkAvailable(getActivity())) {
                    System.out.println("Pie:" + h.getX());
                    String status = "";
                    if (((int) (h.getX())) == 0) {
                        status = "pending";
                    } else {
                        status = "release";
                    }
                    callGraphDetailsApi(ApiUrls.PIE_CHART_DETAILS_URL, "pie", status);
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.internet_connection_msg), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onNothingSelected() {

            }
        });
        // undo all highlights
        // pieChart.highlightValues(null);

        pieChart.invalidate();
    }


    private SpannableString generateCenterSpannableText() {

        SpannableString s = new SpannableString("myShipment\n BL Status");
        s.setSpan(new RelativeSizeSpan(1.7f), 0, 10, 0);
        s.setSpan(new StyleSpan(Typeface.NORMAL), 10, s.length() - 11, 0);
        s.setSpan(new ForegroundColorSpan(Color.GRAY), 10, s.length() - 11, 0);
        s.setSpan(new RelativeSizeSpan(.8f), 10, s.length() - 11, 0);
        s.setSpan(new StyleSpan(Typeface.ITALIC), s.length() - 10, s.length(), 0);
        s.setSpan(new ForegroundColorSpan(ColorTemplate.getHoloBlue()), s.length() - 10, s.length(), 0);
        return s;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == topShipmentsBtn.getId()) {
            GeneralUtils.ScaleAnimate(topShipmentsBtn, getActivity());
            checkForValidations();

        } else if (v.getId() == menuIcon.getId()) {
            ((DashBoardActivity) getActivity()).clickEventSlide();
        } else if (v.getId() == clockIcon.getId()) {
            new CustomDateDialog(getActivity(), datePickerCallBack).show();
        } else if (v.getId() == totalCbmLayout.getId()) {
            if (GeneralUtils.isNetworkAvailable(getActivity())) {
                GeneralUtils.ScaleAnimate(totalCbmLayout, getActivity());
                callBuyerWiseApi(ApiUrls.TOTAL_CBM_URL, AppConstants.TOTAL_CBM);
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.internet_connection_msg), Toast.LENGTH_SHORT).show();
            }

        } else if (v.getId() == totalGwtLayout.getId()) {
            if (GeneralUtils.isNetworkAvailable(getActivity())) {
                GeneralUtils.ScaleAnimate(totalGwtLayout, getActivity());
                callBuyerWiseApi(ApiUrls.TOTAL_GWT_URL, AppConstants.TOTAL_GWT);
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.internet_connection_msg), Toast.LENGTH_SHORT).show();
            }

        } else if (v.getId() == totalShipmentLayout.getId()) {
            if (GeneralUtils.isNetworkAvailable(getActivity())) {
                GeneralUtils.ScaleAnimate(totalShipmentLayout, getActivity());
                callBuyerWiseApi(ApiUrls.TOTAL_SHIPMENTS_URL, AppConstants.TOTAL_SHIPMENT);
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.internet_connection_msg), Toast.LENGTH_SHORT).show();
            }

        }
    }

    public void datePickerCallback() {
        datePickerCallBack = new DatePickerCallBack() {
            @Override
            public void SelectedDate(String fromDate, String todate) {
                SharedPreferenceManager.setToDate(todate, getActivity());
                SharedPreferenceManager.setFromDate(fromDate, getActivity());
                callDashBoardApi();
            }
        };
    }

    private void callBarGraphApi() {
        final JSONObject jsonObject = new JSONObject();
        JSONObject jsonObject1 = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            jsonObject.put("shipperNo", "1000001078");
            jsonObject.put("division", "SE");
            jsonObject.put("distChannel", "EX");
            jsonObject.put("bookingDateFrom", "20110210");
            jsonObject.put("bookingDateTo", "20160802");
            jsonArray.put(jsonObject);
            jsonObject1.put("salesOrg", "HN2");
            jsonObject1.put("dashboardShipperParams", jsonObject);
            System.out.println("json::" + jsonObject1.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        dialog = new ProgressDialog(getActivity());
        dialog.setMessage(getResources().getString(R.string.please_wait_text));
        dialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                ApiUrls.BAR_CHART_DETAILS_URL, jsonObject, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

                try {
                    // Parsing json object response
                    System.out.println("Response:" + response);
                    JSONArray jsonArray1 = response.getJSONArray("lstSalesOrgWiseShipmentJsons");
                    int length = jsonArray1.length();
                    if (length > 0) {

                    } else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(),
                            getResources().getString(R.string.error_fetching),
                            Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Toast.makeText(getActivity(),
                        getResources().getString(R.string.error_fetching), Toast.LENGTH_SHORT).show();
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

        // Adding request to request queue
        requestQueue.add(jsonObjReq);

    }

    private void checkForValidations() {
        callTopShipmentsApi();
    }

    private void callBuyerWiseApi(String url, final String type) {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("shipperNo", SharedPreferenceManager.getUserCode(getActivity()));
            jsonObject.put("division", SharedPreferenceManager.getDivision(getActivity()));
            jsonObject.put("distChannel", SharedPreferenceManager.getDistChannel(getActivity()));
            jsonObject.put("bookingDateFrom", SharedPreferenceManager.getFromDate(getActivity()));
            jsonObject.put("bookingDateTo", SharedPreferenceManager.getToDate(getActivity()));
         /*   jsonObject.put("shipperNo", "1000000037");
            jsonObject.put("division", "SE");
            jsonObject.put("distChannel", "EX");
            jsonObject.put("bookingDateFrom", "20110210");
            jsonObject.put("bookingDateTo", "20160802");*/
        } catch (JSONException e) {
            e.printStackTrace();
        }

        dialog = new ProgressDialog(getActivity());
        dialog.setMessage(getResources().getString(R.string.please_wait_text));
        dialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, jsonObject, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                System.out.println("TOP 3" + response);
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

                try {
                    // Parsing json object response
                    JSONArray jsonArray = null;
                    ArrayListManagerModel arrayListManagerModel = ArrayListManagerModel.getInstance();
                    int length = 0;
                    if (type.equalsIgnoreCase(AppConstants.TOTAL_SHIPMENT)) {
                        length = response.getJSONArray("lstBuyerWiseShipmentJson").length();
                        jsonArray = response.getJSONArray("lstBuyerWiseShipmentJson");

                        ArrayList<BuyerWiseShipmentData> buyerWiseDataArrayList = new ArrayList<>();
                        if (length > 0) {
                            for (int i = 0; i < length; i++) {
                                BuyerWiseShipmentData buyerWiseData = new BuyerWiseShipmentData();
                                buyerWiseData.setTotalShipment(jsonArray.getJSONObject(i).getString("totalShipment"));
                                buyerWiseData.setBuyerName(jsonArray.getJSONObject(i).getString("buyerName"));
                                buyerWiseData.setBuyerNo(jsonArray.getJSONObject(i).getString("buyerNo"));
                                buyerWiseData.setTotalShipmentPerct(jsonArray.getJSONObject(i).getString("totalShipmentPerc"));
                                buyerWiseDataArrayList.add(buyerWiseData);
                            }
                            arrayListManagerModel.setBuyerWiseDataArrayList(buyerWiseDataArrayList);
                            Intent intent = new Intent(getActivity(), BuyerWiseShipmentActivity.class);
                            intent.putExtra("type", AppConstants.TOTAL_SHIPMENT);
                            startActivity(intent);

                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                        }
                    } else if (type.equalsIgnoreCase(AppConstants.TOTAL_GWT)) {
                        length = response.getJSONArray("lstBuyerWiseGWTJson").length();
                        jsonArray = response.getJSONArray("lstBuyerWiseGWTJson");

                        ArrayList<BuyerWiseGWTData> buyerWiseDataArrayList = new ArrayList<>();
                        if (length > 0) {
                            for (int i = 0; i < length; i++) {
                                BuyerWiseGWTData buyerWiseData = new BuyerWiseGWTData();
                                buyerWiseData.setTotalGWT(jsonArray.getJSONObject(i).getString("totalGWT"));
                                buyerWiseData.setBuyerName(jsonArray.getJSONObject(i).getString("buyerName"));
                                buyerWiseData.setBuyerNo(jsonArray.getJSONObject(i).getString("buyerNo"));
                                buyerWiseData.setTotalGWTPerc(jsonArray.getJSONObject(i).getString("totalGWTPerc"));
                                buyerWiseDataArrayList.add(buyerWiseData);
                            }
                            arrayListManagerModel.setBuyerWiseGWTDataArrayList(buyerWiseDataArrayList);
                            Intent intent = new Intent(getActivity(), BuyerWiseShipmentActivity.class);
                            intent.putExtra("type", AppConstants.TOTAL_GWT);
                            startActivity(intent);

                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                        }

                    } else if (type.equalsIgnoreCase(AppConstants.TOTAL_CBM)) {
                        length = response.getJSONArray("lstBuyerWiseCBMJson").length();
                        jsonArray = response.getJSONArray("lstBuyerWiseCBMJson");

                        ArrayList<BuyerWiseCBMData> buyerWiseDataArrayList = new ArrayList<>();
                        if (length > 0) {
                            for (int i = 0; i < length; i++) {
                                BuyerWiseCBMData buyerWiseData = new BuyerWiseCBMData();
                                buyerWiseData.setTotalCBM(jsonArray.getJSONObject(i).getString("totalCBM"));
                                buyerWiseData.setBuyerName(jsonArray.getJSONObject(i).getString("buyerName"));
                                buyerWiseData.setBuyerNo(jsonArray.getJSONObject(i).getString("buyerNo"));
                                buyerWiseData.setTotalCBMPerc(jsonArray.getJSONObject(i).getString("totalCBMPerc"));
                                buyerWiseDataArrayList.add(buyerWiseData);
                            }
                            arrayListManagerModel.setBuyerWiseCBMDataArrayList(buyerWiseDataArrayList);
                            System.out.println("size:" + arrayListManagerModel.getBuyerWiseCBMDataArrayList().size());
                            Intent intent = new Intent(getActivity(), BuyerWiseShipmentActivity.class);
                            intent.putExtra("type", AppConstants.TOTAL_CBM);
                            startActivity(intent);

                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(),
                            getResources().getString(R.string.error_fetching),
                            Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Toast.makeText(getActivity(),
                        getResources().getString(R.string.error_fetching), Toast.LENGTH_SHORT).show();
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

        // Adding request to request queue
        requestQueue.add(jsonObjReq);
    }

    private void callTopShipmentsApi() {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("shipperNo", SharedPreferenceManager.getUserCode(getActivity()));
            jsonObject.put("division", SharedPreferenceManager.getDivision(getActivity()));
            jsonObject.put("distChannel", SharedPreferenceManager.getDistChannel(getActivity()));
            jsonObject.put("bookingDateFrom", SharedPreferenceManager.getFromDate(getActivity()));
            jsonObject.put("bookingDateTo", SharedPreferenceManager.getToDate(getActivity()));
           /* jsonObject.put("shipperNo", "1000000037");
            jsonObject.put("division", "SE");
            jsonObject.put("distChannel", "EX");
            jsonObject.put("bookingDateFrom", "20110210");
            jsonObject.put("bookingDateTo", "20160802");*/
            System.out.println("json2::" + jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        dialog = new ProgressDialog(getActivity());
        dialog.setMessage(getResources().getString(R.string.please_wait_text));
        dialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                ApiUrls.TOP_5_SHIPMENTS, jsonObject, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

                try {
                    // Parsing json object response
                    int length = response.getJSONArray("topFiveShipmentJsonData").length();
                    ArrayList<Top5ShipmentData> top5ShipmentDataArrayList = new ArrayList<>();
                    ArrayListManagerModel arrayListManagerModel = ArrayListManagerModel.getInstance();

                    if (length > 0) {
                        for (int i = 0; i < length; i++) {
                            Top5ShipmentData top5ShipmentData = new Top5ShipmentData();
                            top5ShipmentData.setPortOfDischarge(response.getJSONArray("topFiveShipmentJsonData").getJSONObject(i).getString("portOfDischarge"));
                            top5ShipmentData.setTotalShipment(response.getJSONArray("topFiveShipmentJsonData").getJSONObject(i).getString("totalShipment"));
                            top5ShipmentData.setTotalCBM(response.getJSONArray("topFiveShipmentJsonData").getJSONObject(i).getString("totalCBM"));
                            top5ShipmentData.setTotalGWT(response.getJSONArray("topFiveShipmentJsonData").getJSONObject(i).getString("totalGWT"));
                            top5ShipmentDataArrayList.add(top5ShipmentData);
                        }
                        arrayListManagerModel.setTop5ShipmentDataArrayList(top5ShipmentDataArrayList);
                        Intent intent = new Intent(getActivity(), TopShipmentActivity.class);
                        getActivity().startActivity(intent);
                    } else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(),
                            getResources().getString(R.string.error_fetching),
                            Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(),
                        getResources().getString(R.string.error_fetching), Toast.LENGTH_SHORT).show();
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

        // Adding request to request queue
        requestQueue.add(jsonObjReq);
    }

    private void callGraphDetailsApi(String url, String type, String data) {
        final JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject1 = new JSONObject();
        String params = "";
        try {
            jsonObject.put("shipperNo", SharedPreferenceManager.getUserCode(getActivity()));
            jsonObject.put("division", SharedPreferenceManager.getDivision(getActivity()));
            jsonObject.put("distChannel", SharedPreferenceManager.getDistChannel(getActivity()));
            jsonObject.put("bookingDateFrom", SharedPreferenceManager.getFromDate(getActivity()));
            jsonObject.put("bookingDateTo", SharedPreferenceManager.getToDate(getActivity()));
          /*  jsonObject.put("shipperNo", "1000000037");
            jsonObject.put("division", "SE");
            jsonObject.put("distChannel", "EX");
            jsonObject.put("bookingDateFrom", "20110210");
            jsonObject.put("bookingDateTo", "20160802");*/
            System.out.println("json3::" + jsonObject);
            jsonArray.put(jsonObject);
            if (type.equalsIgnoreCase("pie")) {
                jsonObject1.put("status", data);
            } else {
                if (type.equalsIgnoreCase("bar")) {
                    jsonObject1.put("salesOrg", data);
                }
            }
            jsonObject1.put("dashboardShipperParams", jsonObject);
            System.out.println("json::" + jsonObject1.toString());
            params = jsonObject1.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        dialog = new ProgressDialog(getActivity());
        dialog.setMessage(getResources().getString(R.string.please_wait_text));
        dialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest jsonObjReq = null;
        try {
            jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, new JSONObject(params), new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {

                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }

                    try {
                        // Parsing json object response

                        JSONArray jsonArray = response.getJSONArray("lstSOWiseShipDetailsBuyerJson");
                        int length = jsonArray.length();
                        if (length > 0) {
                            ArrayListManagerModel arrayListManagerModel = ArrayListManagerModel.getInstance();
                            ArrayList<ShipmentDetailsData> shipmentDetailsDataArrayList = new ArrayList<>();
                            ArrayList<ChildDetailsData> childDetailsDataArrayList = new ArrayList<>();
                            for (int i = 0; i < length; i++) {
                                ShipmentDetailsData shipmentDetailsData = new ShipmentDetailsData();
                                shipmentDetailsData.setBlNo(jsonArray.getJSONObject(i).getString("blNo"));
                                shipmentDetailsData.setBookingDate(jsonArray.getJSONObject(i).getString("bookingDate"));
                                shipmentDetailsData.setBldate(jsonArray.getJSONObject(i).getString("bldate"));
                                shipmentDetailsData.setChargeWeight(jsonArray.getJSONObject(i).getString("chargeWeight"));
                                shipmentDetailsData.setGrDate(jsonArray.getJSONObject(i).getString("grDate"));
                                shipmentDetailsData.setGrossWeight(jsonArray.getJSONObject(i).getString("grossWeight"));
                                shipmentDetailsData.setPodCode(jsonArray.getJSONObject(i).getString("podCode"));
                                shipmentDetailsData.setPolCode(jsonArray.getJSONObject(i).getString("polCode"));
                                shipmentDetailsData.setShipmentDate(jsonArray.getJSONObject(i).getString("shipmentDate"));
                                shipmentDetailsData.setBuyerName(jsonArray.getJSONObject(i).getString("buyerName"));
                                shipmentDetailsData.setShipperNo(jsonArray.getJSONObject(i).getString("buyerNo"));
                                shipmentDetailsData.setTotVolume(jsonArray.getJSONObject(i).getString("totVolume"));
                                shipmentDetailsDataArrayList.add(shipmentDetailsData);
                            }
                            arrayListManagerModel.setShipmentDetailsDataArrayList(shipmentDetailsDataArrayList);
                            System.out.println("size::" + arrayListManagerModel.getShipmentDetailsDataArrayList().size());
                            Intent intent = new Intent(getActivity(), ShipmentDetailsActivity.class);
                            startActivity(intent);

                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(),
                                getResources().getString(R.string.error_fetching),
                                Toast.LENGTH_LONG).show();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    Toast.makeText(getActivity(),
                            getResources().getString(R.string.error_fetching), Toast.LENGTH_SHORT).show();
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Adding request to request queue
        requestQueue.add(jsonObjReq);

    }
}
