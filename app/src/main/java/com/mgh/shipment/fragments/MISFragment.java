package com.mgh.shipment.fragments;

import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.mgh.shipment.R;
import com.mgh.shipment.Utility.ApiUrls;
import com.mgh.shipment.Utility.GeneralUtils;
import com.mgh.shipment.Utility.SharedPreferenceManager;
import com.mgh.shipment.activities.MISActivity;
import com.mgh.shipment.adapters.MISAdapter;
import com.mgh.shipment.beans.ArrayListManagerModel;
import com.mgh.shipment.beans.NShipmentData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Akshay Thapliyal on 24-05-2016.
 */
public class MISFragment extends Fragment implements View.OnClickListener {

    private RecyclerView misRecyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private TabLayout tabLayout;
    private Toolbar toolbarId;
    private TextView tvTitleToolbar;
    private ImageView menuIcon;
    private FloatingActionButton shipmentSearchBtn;
    private ProgressDialog dialog;
    private RelativeLayout shipmentNoLayout;
    private EditText noOfShipmentEditText;
    private Button okBtn;
    private MISAdapter trackingIdAdapter;

    @Nullable
    @Override
    public android.view.View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mis_fragment, container, false);

        init(view);
        setTabLayout();
        setViewsVisibility();
        tvTitleToolbar.setText(getActivity().getResources().getString(R.string.last_shipmentes_text));
        setListeners();
        return view;
    }

    private void init(View v) {
        misRecyclerView = (RecyclerView) v.findViewById(R.id.misRecyclerView);
        toolbarId = (Toolbar) v.findViewById(R.id.toolbarId);
        tabLayout = (TabLayout) v.findViewById(R.id.tabLayout);
        menuIcon = (ImageView) toolbarId.findViewById(R.id.menuIcon);
        tvTitleToolbar = (TextView) toolbarId.findViewById(R.id.tvTitleToolbar);
        shipmentSearchBtn = (FloatingActionButton) v.findViewById(R.id.shipmentSearchBtn);
        shipmentNoLayout = (RelativeLayout) v.findViewById(R.id.shipmentNoLayout);
        okBtn = (Button) v.findViewById(R.id.okBtn);
        noOfShipmentEditText = (EditText) v.findViewById(R.id.noOfShipmentEditText);
    }

    private void setViewsVisibility() {
        if (shipmentNoLayout.getVisibility() == View.GONE) {
            shipmentNoLayout.setVisibility(View.VISIBLE);
            misRecyclerView.setVisibility(View.GONE);
            shipmentSearchBtn.setVisibility(View.GONE);
        } else {
            shipmentNoLayout.setVisibility(View.GONE);
            misRecyclerView.setVisibility(View.VISIBLE);
            shipmentSearchBtn.setVisibility(View.VISIBLE);
        }
    }


    private void setTabLayout() {
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());

        View custom1 = LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        ((TextView) custom1.findViewById(R.id.tabTextView)).setText("Last Shipment");
        ((TextView) custom1.findViewById(R.id.tabTextView)).setTextSize(11);
        (custom1.findViewById(R.id.tabImageView)).setBackgroundResource(R.drawable.icon_user);
        TabLayout.Tab customTab = tabLayout.getTabAt(0);
        customTab.setCustomView(custom1);

        View custom2 = LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        ((TextView) custom2.findViewById(R.id.tabTextView)).setText("Goods In Transit");
        ((TextView) custom2.findViewById(R.id.tabTextView)).setTextSize(11);
        (custom2.findViewById(R.id.tabImageView)).setBackgroundResource(R.drawable.icon_user);
        TabLayout.Tab customTab2 = tabLayout.getTabAt(1);
        customTab2.setCustomView(custom2);

        View custom3 = LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        ((TextView) custom3.findViewById(R.id.tabTextView)).setText("Shipment History");
        ((TextView) custom3.findViewById(R.id.tabTextView)).setTextSize(11);
        (custom3.findViewById(R.id.tabImageView)).setBackgroundResource(R.drawable.icon_user);
        TabLayout.Tab customTab3 = tabLayout.getTabAt(2);
        customTab3.setCustomView(custom3);

    }

    private void setListeners() {
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                //Toast.makeText(getActivity(),"hi",Toast.LENGTH_SHORT).show();
                if (position == 0) {
                    tvTitleToolbar.setText(getActivity().getResources().getString(R.string.last_shipmentes_text));
                } else if (position == 1) {
                    tvTitleToolbar.setText(getActivity().getResources().getString(R.string.transit_text));
                } else {
                    tvTitleToolbar.setText(getActivity().getResources().getString(R.string.shipment_hitsory_text));
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        menuIcon.setOnClickListener(this);
        shipmentSearchBtn.setOnClickListener(this);
        okBtn.setOnClickListener(this);
    }

    private void setAdapter() {
        misRecyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getActivity());
        misRecyclerView.setLayoutManager(layoutManager);
        misRecyclerView.setItemAnimator(new DefaultItemAnimator());

        ArrayListManagerModel arrayListManagerModel = ArrayListManagerModel.getInstance();
        trackingIdAdapter = new MISAdapter(getActivity(), arrayListManagerModel);
        misRecyclerView.setAdapter(trackingIdAdapter);
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == menuIcon.getId()) {
            ((MISActivity) getActivity()).clickEventSlide();
        } else if (v.getId() == shipmentSearchBtn.getId()) {
            showSearchDialog();
        } else if (v.getId() == okBtn.getId()) {
            String noShipments = noOfShipmentEditText.getText().toString();
            if (!noShipments.isEmpty()) {
                if (noShipments.matches("[0-9]+")) {
                    checkValidations(noShipments, "");
                } else {
                    Toast.makeText(getActivity(), "Please enter valid number", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getActivity(), "Please enter no. of shipments", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void showSearchDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.last_shipments_dialog);
        // dialog.setTitle(getResources().getString(R.string.forgot_paswrd_text));
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        Button okBtn = (Button) dialog.findViewById(R.id.okBtn);
        final EditText noOfShipmentEditText = (EditText) dialog.findViewById(R.id.noOfShipmentEditText);


        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String noShipments = noOfShipmentEditText.getText().toString();
                System.out.println("no if shipments:" + noShipments);
                if (!noShipments.isEmpty()) {
                    if (noShipments.matches("[0-9]+")) {
                        checkValidations(noShipments, "dialog");
                        dialog.dismiss();
                    } else {
                        Toast.makeText(getActivity(), "Please enter valid number", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Please enter no. of shipments", Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialog.show();


    }

    private void checkValidations(String noOfShipments, String type) {
        if (GeneralUtils.isNetworkAvailable(getActivity())) {
            callTrackingApi(String.valueOf(noOfShipments), type);
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.internet_connection_msg), Toast.LENGTH_SHORT).show();
        }

    }

    private void updateRecyclerViewValues(){
        trackingIdAdapter.notifyDataSetChanged();
    }

    private void callTrackingApi(String noOfShipments, final String type) {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("kunnr", SharedPreferenceManager.getUserCode(getActivity()));
            jsonObject.put("number", noOfShipments);
            jsonObject.put("vkorg", SharedPreferenceManager.getSalesOrgCode(getActivity()));
            jsonObject.put("vtweg", SharedPreferenceManager.getDistChannel(getActivity()));
            jsonObject.put("spart", SharedPreferenceManager.getDivision(getActivity()));
            /*jsonObject.put("kunnr", "1010000012");
            jsonObject.put("number", "5");
            jsonObject.put("vkorg", "1100");
            jsonObject.put("vtweg", "EX");
            jsonObject.put("spart", "AR");*/
            System.out.println("tracking:" + jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        dialog = new ProgressDialog(getActivity());
        dialog.setMessage(getResources().getString(R.string.please_wait_text));
        dialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                ApiUrls.LAST_SHIPMENTS_URL, jsonObject, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

                try {
                    // Parsing json object response
                    JSONArray jsonArray = response.getJSONArray("itHeaderBeanLst");
                    int length = jsonArray.length();
                    ArrayList<NShipmentData> nShipmentDataArrayList = new ArrayList<>();
                    ArrayListManagerModel arrayListManagerModel = ArrayListManagerModel.getInstance();
                    if (length > 0) {
                        for (int i = 0; i < length; i++) {
                            NShipmentData nShipmentData = new NShipmentData();
                            nShipmentData.setPortOfLoading(jsonArray.getJSONObject(i).getString("zzportofloading"));
                            nShipmentData.setPortOfDestination(jsonArray.getJSONObject(i).getString("zzportofdest"));
                            nShipmentData.setHblNo(jsonArray.getJSONObject(i).getString("zzhblhawbno"));
                            nShipmentData.setContainerNo(jsonArray.getJSONObject(i).getString("zzhblhawbno"));
                            nShipmentDataArrayList.add(nShipmentData);
                        }
                        System.out.println("shipment");
                        arrayListManagerModel.setGetNShipmentDataArrayList(nShipmentDataArrayList);

                        if(type.equalsIgnoreCase("dialog")){
                            updateRecyclerViewValues();
                        }else {
                            setViewsVisibility();
                            setAdapter();
                        }

                    } else {
                        Toast.makeText(getActivity(),
                                getResources().getString(R.string.no_data),
                                Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(),
                            getResources().getString(R.string.error_fetching),
                            Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(),
                        getResources().getString(R.string.error_fetching), Toast.LENGTH_SHORT).show();
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

        // Adding request to request queue
        requestQueue.add(jsonObjReq);
    }
}