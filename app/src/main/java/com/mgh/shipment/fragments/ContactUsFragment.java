package com.mgh.shipment.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mgh.shipment.R;
import com.mgh.shipment.activities.ContactUsActivity;

/**
 * Created by Akshay Thapliyal on 24-05-2016.
 */
public class ContactUsFragment extends Fragment implements View.OnClickListener {

    private Toolbar toolbar;
    private ImageView menuIcon;
    private TextView tvTitleToolbar;

    @Nullable
    @Override
    public android.view.View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.contact_us_fragment,container,false);

        init(view);
        tvTitleToolbar.setText(getResources().getString(R.string.contact_us_text));
        setListeners();
        return view;
    }

    private void init(View v){
        toolbar = (Toolbar)v.findViewById(R.id.toolbarId);
        menuIcon = (ImageView)toolbar.findViewById(R.id.menuIcon);
        tvTitleToolbar = (TextView)toolbar.findViewById(R.id.tvTitleToolbar);
    }

    private void setListeners(){

        menuIcon.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if(v.getId() == menuIcon.getId()){
            checkForKeyboard();
            ((ContactUsActivity)getActivity()).clickEventSlide();
        }
    }
    private void checkForKeyboard(){
        InputMethodManager imm = (InputMethodManager) getActivity()
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        if (imm.isAcceptingText()) {
            imm.hideSoftInputFromWindow(
                    getActivity().getCurrentFocus().getWindowToken(), 0);
            // finish();
        } else {

        }
    }


}
