package com.mgh.shipment.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mgh.shipment.R;
import com.mgh.shipment.activities.FrequentTrackingActivity;
import com.mgh.shipment.adapters.TrackingIdAdapter;

import java.util.ArrayList;

/**
 * Created by Akshay Thapliyal on 23-05-2016.
 */
public class FrequentTrackingFragment extends Fragment implements View.OnClickListener {

    private RecyclerView freqRecyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private Toolbar toolbar;
    private ImageView menuIcon;
    private TextView tvTitleToolbar;
    private ArrayList<String> data  = new ArrayList<String>();
    private ArrayList<String> data2 = new ArrayList<String>();
    private ArrayList<String> pod = new ArrayList<String>();
    private ArrayList<String> pol = new ArrayList<String>();

    @Nullable
    @Override
    public android.view.View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frequent_tracking_fragment,container,false);

        init(view);
        tvTitleToolbar.setText(getActivity().getResources().getString(R.string.frequent_tracking_text));
        setListeners();
        addDataToList();
        setAdapter();
        return view;
    }

    private void init(View v){
        freqRecyclerView = (RecyclerView)v.findViewById(R.id.freqRecyclerView);
        toolbar = (Toolbar)v.findViewById(R.id.toolbarId);
        menuIcon = (ImageView)v.findViewById(R.id.menuIcon);
        tvTitleToolbar = (TextView)toolbar.findViewById(R.id.tvTitleToolbar);

    }

    private void addDataToList(){
        data.add("ABCDEF12345");
        data.add("EFGHI67890");
        data.add("IJKLM23452");

        data2.add("TRLU4284746");
        data2.add("MSKU6011672");
        data2.add("INXU6011677");

        pod.add("New Delhi, West Delhi");
        pod.add("New Delhi, North Delhi");
        pod.add("New Delhi, South Delhi");

        pol.add("Bengaluru, Koramangla");
        pol.add("Bengaluru, Indrapuram");
        pol.add("New Delhi, South Delhi");
    }

    private void setListeners(){
        menuIcon.setOnClickListener(this);
    }

    private void setAdapter(){
        freqRecyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getActivity());
        freqRecyclerView.setLayoutManager(layoutManager);
        freqRecyclerView.setItemAnimator(new DefaultItemAnimator());

        TrackingIdAdapter trackingIdAdapter = new TrackingIdAdapter(getActivity(),data,data2,pod,pol);
        freqRecyclerView.setAdapter(trackingIdAdapter);
    }

    @Override
    public void onClick(View v) {

        if(v.getId() == menuIcon.getId()){
            ((FrequentTrackingActivity)getActivity()).clickEventSlide();
        }
    }


}
