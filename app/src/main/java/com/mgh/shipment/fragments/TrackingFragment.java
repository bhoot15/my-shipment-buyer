package com.mgh.shipment.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.mgh.shipment.R;
import com.mgh.shipment.Utility.ApiUrls;
import com.mgh.shipment.Utility.GeneralUtils;
import com.mgh.shipment.Utility.SharedPreferenceManager;
import com.mgh.shipment.activities.CommInvTrackingActivity;
import com.mgh.shipment.activities.CompanySelectionActivity;
import com.mgh.shipment.activities.MilestoneTrackingActivity;
import com.mgh.shipment.activities.POActivity;
import com.mgh.shipment.activities.TrackingActivity;
import com.mgh.shipment.adapters.TrackingIdAdapter;
import com.mgh.shipment.beans.ArrayListManagerModel;
import com.mgh.shipment.beans.CommInvData;
import com.mgh.shipment.beans.POData;
import com.mgh.shipment.beans.PackagingData;
import com.mgh.shipment.beans.TrackingData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.EventListener;
import java.util.Locale;

/**
 * Created by Akshay Thapliyal on 16-05-2016.
 */
public class TrackingFragment extends Fragment implements View.OnClickListener {

    private RecyclerView trackingIdRecyclerView;
    private Button trackButton;
    private RecyclerView.LayoutManager layoutManager;
    private Toolbar toolbarId;
    private ImageView menuIcon;
    private TextView title;
    private ArrayList<String> data = new ArrayList<String>();
    private ArrayList<String> data2 = new ArrayList<String>();
    private ArrayList<String> pod = new ArrayList<String>();
    private ArrayList<String> pol = new ArrayList<String>();
    private MaterialSpinner referenceSpinner;
    private MaterialSpinner statusSpinner;
    private ProgressDialog dialog;
    private EditText valueEditText, toDateEditText, fromDateEditText;
    TrackingIdAdapter trackingIdAdapter;
    private LinearLayout toDateLayout, fromDateLayout, statusLayout;
    private DateFormat dateFormatter;
    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;
    private String status="";
    private String referenceNo = "HBL";

    @Nullable
    @Override
    public android.view.View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_tracking_dialog, container, false);

        init(view);
        setListeners();
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        title.setText(getActivity().getResources().getString(R.string.add_tracking_text));
        setDatePickerDialogs();
        setSpinner();
        return view;
    }

    private void init(View v) {
        trackButton = (Button) v.findViewById(R.id.trackButton);
        toolbarId = (Toolbar) v.findViewById(R.id.toolbarId);
        menuIcon = (ImageView) toolbarId.findViewById(R.id.menuIcon);
        title = (TextView) toolbarId.findViewById(R.id.tvTitleToolbar);
        referenceSpinner = (MaterialSpinner) v.findViewById(R.id.referenceSpinner);
        valueEditText = (EditText) v.findViewById(R.id.valueEditText);
        toDateLayout = (LinearLayout) v.findViewById(R.id.toDateLayout);
        fromDateLayout = (LinearLayout) v.findViewById(R.id.fromDateLayout);
        statusLayout = (LinearLayout) v.findViewById(R.id.statusLayout);
        statusSpinner = (MaterialSpinner) v.findViewById(R.id.statusSpinner);
        fromDateEditText = (EditText) v.findViewById(R.id.fromDateEditText);
        toDateEditText = (EditText) v.findViewById(R.id.toDateEditText);
    }

    private void setSpinner() {
        String str[] = {"HBL No.", "PO No.", "CommInvoice"};
        referenceSpinner.setItems(str);

        String str2[] = {"None","Order Booked","Cargo Received","Stuffing Done"};
        statusSpinner.setItems(str2);
    }

    private void setDatePickerDialogs() {
        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                fromDateEditText.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        toDatePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                toDateEditText.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    private void setListeners() {
        trackButton.setOnClickListener(this);
        menuIcon.setOnClickListener(this);
        fromDateEditText.setOnClickListener(this);
        toDateEditText.setOnClickListener(this);

        referenceSpinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                if (position == 1 || position ==2) {
                    toDateLayout.setVisibility(View.VISIBLE);
                    fromDateLayout.setVisibility(View.VISIBLE);
                    statusLayout.setVisibility(View.VISIBLE);
                } else {
                    toDateLayout.setVisibility(View.GONE);
                    fromDateLayout.setVisibility(View.GONE);
                    statusLayout.setVisibility(View.GONE);
                }

                if (position == 0) {
                    referenceNo = "HBL";
                } else if (position == 1) {
                    referenceNo = "PO";
                }else if(position ==2){
                    referenceNo = "COMM";
                }
            }
        });

        statusSpinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
              //  Toast.makeText(getActivity(), position + "", Toast.LENGTH_SHORT).show();
                if(position==0){
                    status="";
                }else if (position == 1) {
                    status = "Order Booked";
                } else if (position == 2) {
                    status = "Cargo Received";
                } else if (position == 3) {
                    status = "Stuffing Done";
                }
            }
        });
    }


    @Override
    public void onClick(View v) {

        if (v.getId() == trackButton.getId()) {
            //showAddTrackingDialog();
            GeneralUtils.ScaleAnimate(trackButton, getActivity());

            if(referenceNo.equalsIgnoreCase("HBL")){
                checkForValidations();
            }else if(referenceNo.equalsIgnoreCase("PO") || referenceNo.equalsIgnoreCase("COMM")){
                if (GeneralUtils.isNetworkAvailable(getActivity())) {
                    checkValidations(referenceNo);
                }else{
                    Toast.makeText(getActivity(), getResources().getString(R.string.internet_connection_msg), Toast.LENGTH_SHORT).show();
                }
            }


        } else if (v.getId() == menuIcon.getId()) {
            ((TrackingActivity) getActivity()).clickEventSlide();
        } else if (v.getId() == fromDateEditText.getId()) {
            //Toast.makeText(getActivity(),"hi",Toast.LENGTH_SHORT).show();
            fromDatePickerDialog.show();
        } else if (v.getId() == toDateEditText.getId()) {
            toDatePickerDialog.show();
        }
    }

    private void checkForValidations() {
        if (GeneralUtils.isNetworkAvailable(getActivity())) {
            String value = valueEditText.getText().toString();
            if (!value.isEmpty()) {
                callTrackingApi(value);
            } else {
                Toast.makeText(getActivity(), "Please enter value.", Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.internet_connection_msg), Toast.LENGTH_SHORT).show();
        }

    }

    private void checkValidations(String referenceNo){
        String fromDate = fromDateEditText.getText().toString();
        String toDate = toDateEditText.getText().toString();
        String value = valueEditText.getText().toString();

        //All empty check


        //Date checks
        if (!(value.isEmpty())) {
            if (referenceNo.equalsIgnoreCase("PO")){
                callPOApi(value, fromDate,toDate);
            }else if(referenceNo.equalsIgnoreCase("COMM")){
                callCommonInvoiceApi(value, fromDate,toDate);
            }
        }else if(!(fromDate.isEmpty()) && !(toDate.isEmpty())){
            if (referenceNo.equalsIgnoreCase("PO")){
                callPOApi(value, fromDate,toDate);
            }else if(referenceNo.equalsIgnoreCase("COMM")){
                callCommonInvoiceApi(value, fromDate,toDate);
            }

        }else if(!(fromDate.isEmpty()) && toDate.isEmpty()){
            Toast.makeText(getActivity(), "Please fill To Date!", Toast.LENGTH_LONG).show();
            return;
        }else if(fromDate.isEmpty() && !(toDate.isEmpty())){
            Toast.makeText(getActivity(), "Please fill From Date!", Toast.LENGTH_LONG).show();
            return;
        }else if(value.isEmpty() && !(status.equals(""))){//PO & status check
            Toast.makeText(getActivity(), "Please fill reference Value!!", Toast.LENGTH_LONG).show();
            return;
        }else if(status.equals("")){
            Toast.makeText(getActivity(), "Please fill some Value!!", Toast.LENGTH_LONG).show();
        }
        else {
            System.out.println("value" + value);
            if (referenceNo.equalsIgnoreCase("PO")){
                callPOApi(value, fromDate,toDate);
            }else if(referenceNo.equalsIgnoreCase("COMM")){
                callCommonInvoiceApi(value, fromDate,toDate);
            }

        }


    }

    private void callPOApi(String value, String fromDate, String toDate){
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("shipper_no",SharedPreferenceManager.getUserCode(getActivity()));
            jsonObject.put("poNo", value);
            jsonObject.put("fromDate",fromDate);
            jsonObject.put("toDate", toDate);
            jsonObject.put("statusType", status);
            System.out.println("PO::" + jsonObject);
            /*jsonObject.put("blNo", "BOM000483");
            jsonObject.put("salesOrg", "BOM2");
            jsonObject.put("distributionChannel", "EX");
            jsonObject.put("division", "SE");*/
        } catch (JSONException e) {
            e.printStackTrace();
        }

        dialog = new ProgressDialog(getActivity());
        dialog.setMessage(getResources().getString(R.string.please_wait_text));
        dialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                ApiUrls.PO_DETAIL_URL , jsonObject, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

                try {
                    // Parsing json object response
                    System.out.println("response: " + response);
                    JSONArray jsonArray = response.getJSONArray("poTrackingResultData");
                    int length = jsonArray.length();
                    ArrayListManagerModel arrayListManagerModel = ArrayListManagerModel.getInstance();
                    ArrayList<POData> poDataArrayList = new ArrayList<>();
                    if (length > 0) {
                        for (int i = 0; i < length; i++) {
                            POData poData = new POData();
                            poData.setPo_no(jsonArray.getJSONObject(i).getString("po_no"));
                            poData.setDocument_no(jsonArray.getJSONObject(i).getString("document_no"));
                            poData.setBl_no(jsonArray.getJSONObject(i).getString("bl_no"));
                            poData.setBooking_date(jsonArray.getJSONObject(i).getString("booking_date"));
                            poData.setBl_date(jsonArray.getJSONObject(i).getString("bl_date"));
                            poData.setShipper_name(jsonArray.getJSONObject(i).getString("shipper_name"));
                            poData.setBuyerName(jsonArray.getJSONObject(i).getString("buyer_name"));
                            poData.setPol_code(jsonArray.getJSONObject(i).getString("pol_code"));
                            poData.setPod_code(jsonArray.getJSONObject(i).getString("pod_code"));
                            poData.setGross_wt(jsonArray.getJSONObject(i).getString("gross_wt"));
                            poData.setCharge_wt(jsonArray.getJSONObject(i).getString("charge_wt"));
                            poData.setTot_volume(jsonArray.getJSONObject(i).getString("tot_volume"));
                            poData.setGr_date(jsonArray.getJSONObject(i).getString("gr_date"));
                            poData.setShipment_date(jsonArray.getJSONObject(i).getString("shipment_date"));
                            poData.setContainer_no(jsonArray.getJSONObject(i).getString("container_no"));
                            poData.setContainer_type(jsonArray.getJSONObject(i).getString("container_type"));
                            poData.setAta(jsonArray.getJSONObject(i).getString("ata"));
                            poData.setAtd(jsonArray.getJSONObject(i).getString("atd"));
                            poData.setEtd(jsonArray.getJSONObject(i).getString("etd"));
                            poData.setEta(jsonArray.getJSONObject(i).getString("eta"));
                            poData.setFeta(jsonArray.getJSONObject(i).getString("feta"));
                            poData.setFetd(jsonArray.getJSONObject(i).getString("fetd"));
                            poDataArrayList.add(poData);

                        }
                        arrayListManagerModel.setPoDataArrayList(poDataArrayList);
                        Intent intent = new Intent(getActivity(), POActivity.class);
                        startActivity(intent);

                    } else {
                        Toast.makeText(getActivity(),
                                getResources().getString(R.string.no_data),
                                Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(),
                            getResources().getString(R.string.error_fetching),
                            Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(),
                        getResources().getString(R.string.error_fetching), Toast.LENGTH_SHORT).show();
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

        // Adding request to request queue
        requestQueue.add(jsonObjReq);

    }

    private void callCommonInvoiceApi(String value, String fromDate, String toDate){
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("shipper_no",SharedPreferenceManager.getUserCode(getActivity()));
            jsonObject.put("commInvNo", value);
            jsonObject.put("fromDate",fromDate);
            jsonObject.put("toDate", toDate);
            jsonObject.put("statusType", status);
            /*jsonObject.put("shipper_no","1000000037");
            jsonObject.put("commInvNo", "TEST-01");
            jsonObject.put("fromDate","");
            jsonObject.put("toDate", "");
            jsonObject.put("statusType", "Order Booked");*/
            System.out.println("PO::" + jsonObject);
            /*jsonObject.put("blNo", "BOM000483");
            jsonObject.put("salesOrg", "BOM2");
            jsonObject.put("distributionChannel", "EX");
            jsonObject.put("division", "SE");*/
        } catch (JSONException e) {
            e.printStackTrace();
        }

        dialog = new ProgressDialog(getActivity());
        dialog.setMessage(getResources().getString(R.string.please_wait_text));
        dialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                ApiUrls.COMM_INV_NO_URL, jsonObject, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

                try {
                    // Parsing json object response
                    System.out.println("response: " + response);
                    JSONArray jsonArray = response.getJSONArray("poTrackingResultData");
                    int length = jsonArray.length();
                    ArrayListManagerModel arrayListManagerModel = ArrayListManagerModel.getInstance();
                    ArrayList<CommInvData> commInvDataArrayList = new ArrayList<>();
                    if (length > 0) {
                        for (int i = 0; i < length; i++) {
                            CommInvData commInvData = new CommInvData();
                            commInvData.setPoNo(jsonArray.getJSONObject(i).getString("po_no"));
                            commInvData.setDocumentNo(jsonArray.getJSONObject(i).getString("document_no"));
                            commInvData.setHblNo(jsonArray.getJSONObject(i).getString("bl_no"));
                            commInvData.setBookingDate(jsonArray.getJSONObject(i).getString("booking_date"));
                            commInvData.setShipperName(jsonArray.getJSONObject(i).getString("shipper_name"));
                            commInvData.setPol(jsonArray.getJSONObject(i).getString("pol_name"));
                            commInvData.setPod(jsonArray.getJSONObject(i).getString("pod_name"));
                            commInvData.setGrossWt(jsonArray.getJSONObject(i).getString("gross_wt"));
                            commInvData.setGoodReceivedDate(jsonArray.getJSONObject(i).getString("gr_date"));
                            commInvData.setShipmentDate(jsonArray.getJSONObject(i).getString("shipment_date"));
                            commInvData.setAta(jsonArray.getJSONObject(i).getString("ata"));
                            commInvData.setAtd(jsonArray.getJSONObject(i).getString("atd"));
                            commInvData.setEtd(jsonArray.getJSONObject(i).getString("etd"));
                            commInvData.setEta(jsonArray.getJSONObject(i).getString("eta"));
                            commInvData.setCommInvNo(jsonArray.getJSONObject(i).getString("comm_invoice_no"));
                            commInvData.setConsignee(jsonArray.getJSONObject(i).getString("buyer_name"));
                            commInvData.setCarrier(GeneralUtils.checkNullValue(jsonArray.getJSONObject(i).getString("carrier_name")));
                            commInvData.setDeliveryAgent(GeneralUtils.checkNullValue(jsonArray.getJSONObject(i).getString("agent_name")));
                            commInvData.setTotalPcs(jsonArray.getJSONObject(i).getString("tot_pcs"));
                            commInvData.setFeta(jsonArray.getJSONObject(i).getString("feta"));
                            commInvData.setFetd(jsonArray.getJSONObject(i).getString("fetd"));
                            commInvDataArrayList.add(commInvData);

                        }
                        arrayListManagerModel.setCommInvDataArrayList(commInvDataArrayList);
                        Intent intent = new Intent(getActivity(), CommInvTrackingActivity.class);
                        startActivity(intent);

                    } else {
                        Toast.makeText(getActivity(),
                                getResources().getString(R.string.no_data),
                                Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(),
                            getResources().getString(R.string.error_fetching),
                            Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(),
                        getResources().getString(R.string.error_fetching), Toast.LENGTH_SHORT).show();
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

        // Adding request to request queue
        requestQueue.add(jsonObjReq);

    }

    private void callTrackingApi(String value) {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("blNo", value);
            jsonObject.put("salesOrg", SharedPreferenceManager.getSalesOrgCode(getActivity()));
            jsonObject.put("distributionChannel", SharedPreferenceManager.getDistChannel(getActivity()));
            jsonObject.put("division", SharedPreferenceManager.getDivision(getActivity()));
            System.out.println("track::" + jsonObject);
            /*jsonObject.put("blNo", "BOM000483");
            jsonObject.put("salesOrg", "BOM2");
            jsonObject.put("distributionChannel", "EX");
            jsonObject.put("division", "SE");*/
        } catch (JSONException e) {
            e.printStackTrace();
        }

        dialog = new ProgressDialog(getActivity());
        dialog.setMessage(getResources().getString(R.string.please_wait_text));
        dialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                ApiUrls.TRACKING_URL, jsonObject, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

                try {
                    // Parsing json object response
                    System.out.println("response: " + response);
                    JSONArray jsonArray = response.getJSONArray("documentHeaderLst");
                    int length = jsonArray.length();
                    ArrayListManagerModel arrayListManagerModel = ArrayListManagerModel.getInstance();
                    ArrayList<TrackingData> trackingDataArrayList = new ArrayList<>();
                    if (length > 0) {
                        for (int i = 0; i < length; i++) {
                            TrackingData trackingData = new TrackingData();
                            trackingData.setHblNo(jsonArray.getJSONObject(i).getString("bl_no"));
                            trackingData.setContainerNo(jsonArray.getJSONObject(i).getString("bl_no"));
                            trackingData.setShipmentDate(jsonArray.getJSONObject(i).getString("shipment_date"));
                            trackingData.setDeliveryDate(jsonArray.getJSONObject(i).getString("shipment_date"));
                            trackingData.setPod(jsonArray.getJSONObject(i).getString("pod_name"));
                            trackingData.setPol(jsonArray.getJSONObject(i).getString("pol_name"));
                            trackingData.setBuyerName(jsonArray.getJSONObject(i).getString("buyer_name"));
                            trackingData.setBookingDate(jsonArray.getJSONObject(i).getString("booking_date"));
                            trackingData.setGoodsReceived(jsonArray.getJSONObject(i).getString("gr_date"));
                            trackingData.setCarrierName(jsonArray.getJSONObject(i).getString("carrier_name"));
                            trackingData.setCarrierNo(jsonArray.getJSONObject(i).getString("carrier_no"));
                            trackingData.setChargeWt(jsonArray.getJSONObject(i).getString("charge_wt"));
                            trackingData.setGrossWt(jsonArray.getJSONObject(i).getString("gross_wt"));
                            trackingData.setTotalVol(jsonArray.getJSONObject(i).getString("tot_volume"));
                            trackingData.setDocumentNo(jsonArray.getJSONObject(i).getString("document_no"));
                            trackingData.setShipperName(jsonArray.getJSONObject(i).getString("shipper_name"));
                            trackingDataArrayList.add(trackingData);

                            ArrayList<PackagingData> packagingDataArrayList = new ArrayList<>();

                            if((jsonArray.getJSONObject(i).isNull("itScheduleDetailBean"))){

                                PackagingData packagingData = new PackagingData();
                                packagingData.setPod("NA");
                                packagingData.setPol("NA");
                                packagingData.setEta("NA");
                                packagingData.setEtd("NA");
                                packagingData.setTransShipmentNo("NA");
                                packagingDataArrayList.add(packagingData);

                                arrayListManagerModel.setPackagingDataArrayList(packagingDataArrayList);
                            }else{
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("itScheduleDetailBean");

                                PackagingData packagingData = new PackagingData();
                                packagingData.setPod(jsonObject1.getString("pod"));
                                packagingData.setPol(jsonObject1.getString("pol"));
                                packagingData.setEta(jsonObject1.getString("pod_eta"));
                                packagingData.setEtd(jsonObject1.getString("pol_etd"));
                                packagingData.setTransShipmentNo(jsonObject1.getString("transhipment_port"));
                                packagingDataArrayList.add(packagingData);

                                arrayListManagerModel.setPackagingDataArrayList(packagingDataArrayList);
                            }

                        }

                        arrayListManagerModel.setTrackingDataArrayList(trackingDataArrayList);
                        if (trackingDataArrayList.size() > 0) {
                            Intent intent = new Intent(getActivity(), MilestoneTrackingActivity.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(getActivity(), "No data available", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(),
                                getResources().getString(R.string.error_fetching),
                                Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(),
                            getResources().getString(R.string.error_fetching),
                            Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(),
                        getResources().getString(R.string.error_fetching), Toast.LENGTH_SHORT).show();
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

        // Adding request to request queue
        requestQueue.add(jsonObjReq);

    }

    private void showAddTrackingDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.add_tracking_dialog);
        Spinner referenceSpinner = (Spinner) dialog.findViewById(R.id.referenceSpinner);
        Button trackButton = (Button) dialog.findViewById(R.id.trackButton);
        final EditText valueEditText = (EditText) dialog.findViewById(R.id.valueEditText);

        String str[] = {"Container No.", "", "Booking No."};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, str);
        referenceSpinner.setAdapter(adapter);
        referenceSpinner.setSelection(0);

        dialog.show();

        trackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (valueEditText.getText().toString() != "") {
                    data2.add(valueEditText.getText().toString());
                    data.add("ABCDEF12345");
                    pod.add("New Delhi, West Delhi");
                    pol.add("New Delhi, South Delhi");

                    trackingIdAdapter.notifyDataSetChanged();
                    dialog.dismiss();
                }
            }
        });
    }
}
