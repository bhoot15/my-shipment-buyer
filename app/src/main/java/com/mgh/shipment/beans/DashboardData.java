package com.mgh.shipment.beans;

/**
 * Created by Akshay Thapliyal on 10-08-2016.
 */
public class DashboardData {

    String pendingPercentage;

    public String getReleasePercentage() {
        return releasePercentage;
    }

    public void setReleasePercentage(String releasePercentage) {
        this.releasePercentage = releasePercentage;
    }

    public String getPendingPercentage() {
        return pendingPercentage;
    }

    public void setPendingPercentage(String pendingPercentage) {
        this.pendingPercentage = pendingPercentage;
    }

    String releasePercentage;
    public String getTotalShipmentCount() {
        return totalShipmentCount;
    }

    public void setTotalShipmentCount(String totalShipmentCount) {
        this.totalShipmentCount = totalShipmentCount;
    }

    public String getTotalGwtCount() {
        return totalGwtCount;
    }

    public void setTotalGwtCount(String totalGwtCount) {
        this.totalGwtCount = totalGwtCount;
    }

    public String getTotalCbmCount() {
        return totalCbmCount;
    }

    public void setTotalCbmCount(String totalCbmCount) {
        this.totalCbmCount = totalCbmCount;
    }

    private String totalShipmentCount, totalCbmCount, totalGwtCount;

}
