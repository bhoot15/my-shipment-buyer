package com.mgh.shipment.beans;

/**
 * Created by Akshay Thapliyal on 09-09-2016.
 */
public class PackagingData {
    private String pol;
    private String pod;
    private String eta;
    private String etd;

    public String getPol() {
        return pol;
    }

    public void setPol(String pol) {
        this.pol = pol;
    }

    public String getPod() {
        return pod;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }

    public String getEta() {
        return eta;
    }

    public void setEta(String eta) {
        this.eta = eta;
    }

    public String getEtd() {
        return etd;
    }

    public void setEtd(String etd) {
        this.etd = etd;
    }

    public String getTransShipmentNo() {
        return transShipmentNo;
    }

    public void setTransShipmentNo(String transShipmentNo) {
        this.transShipmentNo = transShipmentNo;
    }

    private String transShipmentNo;
}
