package com.mgh.shipment.beans;

import java.util.ArrayList;

/**
 * Created by Akshay Thapliyal on 29-06-2016.
 */
public class FilteredSalesOrgList {
    public String getSalesOrgName() {
        return salesOrgName;
    }

    public void setSalesOrgName(String salesOrgName) {
        this.salesOrgName = salesOrgName;
    }

    public String getSalesOrg() {
        return salesOrg;
    }

    public void setSalesOrg(String salesOrg) {
        this.salesOrg = salesOrg;
    }

    public ArrayList<String> getDistChannel() {
        return distChannel;
    }

    public void setDistChannel(ArrayList<String> distChannel) {
        this.distChannel = distChannel;
    }

    public ArrayList<String> getDivision() {
        return division;
    }

    public void setDivision(ArrayList<String> division) {
        this.division = division;
    }

    String salesOrgName, salesOrg;
    ArrayList<String> distChannel;
    ArrayList<String> division;
}
