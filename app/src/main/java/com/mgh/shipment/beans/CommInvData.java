package com.mgh.shipment.beans;

/**
 * Created by Akshay Thapliyal on 03-01-2017.
 */
public class CommInvData {

    String feta;

    public String getFetd() {
        return fetd;
    }

    public void setFetd(String fetd) {
        this.fetd = fetd;
    }

    public String getFeta() {
        return feta;
    }

    public void setFeta(String feta) {
        this.feta = feta;
    }

    String fetd;
    public String getDocumentNo() {
        return documentNo;
    }

    public void setDocumentNo(String documentNo) {
        this.documentNo = documentNo;
    }

    String documentNo;
    public String getHblNo() {
        return hblNo;
    }

    public void setHblNo(String hblNo) {
        this.hblNo = hblNo;
    }

    public String getShipperName() {
        return shipperName;
    }

    public void setShipperName(String shipperName) {
        this.shipperName = shipperName;
    }

    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    public String getDeliveryAgent() {
        return deliveryAgent;
    }

    public void setDeliveryAgent(String deliveryAgent) {
        this.deliveryAgent = deliveryAgent;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public String getCommInvNo() {
        return commInvNo;
    }

    public void setCommInvNo(String commInvNo) {
        this.commInvNo = commInvNo;
    }

    public String getPol() {
        return pol;
    }

    public void setPol(String pol) {
        this.pol = pol;
    }

    public String getPod() {
        return pod;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }

    public String getTotalPcs() {
        return totalPcs;
    }

    public void setTotalPcs(String totalPcs) {
        this.totalPcs = totalPcs;
    }

    public String getGrossWt() {
        return grossWt;
    }

    public void setGrossWt(String grossWt) {
        this.grossWt = grossWt;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getEtd() {
        return etd;
    }

    public void setEtd(String etd) {
        this.etd = etd;
    }

    public String getAtd() {
        return atd;
    }

    public void setAtd(String atd) {
        this.atd = atd;
    }

    public String getEta() {
        return eta;
    }

    public void setEta(String eta) {
        this.eta = eta;
    }

    public String getAta() {
        return ata;
    }

    public void setAta(String ata) {
        this.ata = ata;
    }

    public String getPoStatus() {
        return poStatus;
    }

    public void setPoStatus(String poStatus) {
        this.poStatus = poStatus;
    }

    public String getExceptionStatus() {
        return exceptionStatus;
    }

    public void setExceptionStatus(String exceptionStatus) {
        this.exceptionStatus = exceptionStatus;
    }

    public String getShipmentDate() {
        return shipmentDate;
    }

    public void setShipmentDate(String shipmentDate) {
        this.shipmentDate = shipmentDate;
    }

    public String getGoodReceivedDate() {
        return goodReceivedDate;
    }

    public void setGoodReceivedDate(String goodReceivedDate) {
        this.goodReceivedDate = goodReceivedDate;
    }

    public String getPoNo() {
        return poNo;
    }

    public void setPoNo(String poNo) {
        this.poNo = poNo;
    }

    String hblNo, shipperName,consignee, deliveryAgent, carrier, commInvNo, pol, pod, totalPcs, grossWt
            , bookingDate, etd, atd, eta, ata, poStatus, exceptionStatus,shipmentDate, goodReceivedDate, poNo;
}
