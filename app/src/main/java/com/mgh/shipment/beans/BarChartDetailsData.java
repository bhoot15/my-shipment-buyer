package com.mgh.shipment.beans;

/**
 * Created by Akshay Thapliyal on 11-08-2016.
 */
public class BarChartDetailsData {
    public String getPortOfDischarge() {
        return portOfDischarge;
    }

    public void setPortOfDischarge(String portOfDischarge) {
        this.portOfDischarge = portOfDischarge;
    }

    public String getTotalGrossWt() {
        return totalGrossWt;
    }

    public void setTotalGrossWt(String totalGrossWt) {
        this.totalGrossWt = totalGrossWt;
    }

    public String getTotalShipment() {
        return totalShipment;
    }

    public void setTotalShipment(String totalShipment) {
        this.totalShipment = totalShipment;
    }

    public String getTotalCbm() {
        return totalCbm;
    }

    public void setTotalCbm(String totalCbm) {
        this.totalCbm = totalCbm;
    }

    private String portOfDischarge, totalGrossWt, totalShipment, totalCbm;

}
