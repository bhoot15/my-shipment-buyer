package com.mgh.shipment.beans;

/**
 * Created by Akshay Thapliyal on 12-08-2016.
 */
public class Top5ShipmentData {
    private String portOfDischarge, totalShipment, totalCBM, totalGWT;

    public String getPortOfDischarge() {
        return portOfDischarge;
    }

    public void setPortOfDischarge(String portOfDischarge) {
        this.portOfDischarge = portOfDischarge;
    }

    public String getTotalCBM() {
        return totalCBM;
    }

    public void setTotalCBM(String totalCBM) {
        this.totalCBM = totalCBM;
    }

    public String getTotalShipment() {
        return totalShipment;
    }

    public void setTotalShipment(String totalShipment) {
        this.totalShipment = totalShipment;
    }

    public String getTotalGWT() {
        return totalGWT;
    }

    public void setTotalGWT(String totalGWT) {
        this.totalGWT = totalGWT;
    }
}
