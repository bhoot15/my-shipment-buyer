package com.mgh.shipment.beans;

/**
 * Created by Akshay Thapliyal on 09-08-2016.
 */
public class NShipmentData {

    String portOfLoading;

    public String getHblNo() {
        return hblNo;
    }

    public void setHblNo(String hblNo) {
        this.hblNo = hblNo;
    }

    public String getPortOfLoading() {
        return portOfLoading;
    }

    public void setPortOfLoading(String portOfLoading) {
        this.portOfLoading = portOfLoading;
    }

    public String getContainerNo() {
        return containerNo;
    }

    public void setContainerNo(String containerNo) {
        this.containerNo = containerNo;
    }

    String hblNo;
    String containerNo;

    public String getPortOfDestination() {
        return portOfDestination;
    }

    public void setPortOfDestination(String portOfDestination) {
        this.portOfDestination = portOfDestination;
    }

    String portOfDestination;
}
