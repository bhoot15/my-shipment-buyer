package com.mgh.shipment.beans;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mgh.shipment.R;

/**
 * Created by Akshay Thapliyal on 02-09-2016.
 */
public class ChildDetailsExpandableAdapter extends RecyclerView.Adapter<ChildDetailsExpandableAdapter.MyViewHolder> {

    Context mContext;
    private ArrayListManagerModel mArrayListManagerModel ;

    public ChildDetailsExpandableAdapter(Context mContext, ArrayListManagerModel arrayListManagerModel) {
        this.mContext = mContext;
        this.mArrayListManagerModel = arrayListManagerModel;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.expandable_child_list, null);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.tvBLDate.setText(mArrayListManagerModel.getShipmentDetailsDataArrayList().get(position).getBldate());

    }

    @Override
    public int getItemCount() {
        return mArrayListManagerModel.getShipmentDetailsDataArrayList().size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvBLDate, tvBookingDate, tvPOLCode, tvPODCode, tvGrossWt, tvChargeWt,
                tvTotalVol, tvGrDate, tvShipmentDate;
        public MyViewHolder(View itemView) {
            super(itemView);

            tvBLDate = (TextView)itemView.findViewById(R.id.tvBLDate);
            tvBookingDate = (TextView)itemView.findViewById(R.id.tvBookingDate);
            tvPOLCode = (TextView)itemView.findViewById(R.id.tvPOLCode);
            tvPODCode = (TextView)itemView.findViewById(R.id.tvPODCode);
            tvGrossWt = (TextView)itemView.findViewById(R.id.tvGrossWt);
            tvChargeWt = (TextView)itemView.findViewById(R.id.tvChargeWt);
            tvTotalVol = (TextView)itemView.findViewById(R.id.tvTotalVol);
            tvGrDate = (TextView)itemView.findViewById(R.id.tvGrDate);
            tvShipmentDate = (TextView)itemView.findViewById(R.id.tvShipmentDate);
        }
    }
}
