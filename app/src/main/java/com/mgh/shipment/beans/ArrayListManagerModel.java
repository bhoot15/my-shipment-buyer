package com.mgh.shipment.beans;

import java.util.ArrayList;

/**
 * Created by akshay on 27/5/15.
 */
public class ArrayListManagerModel {

    public ArrayList<CommInvData> getCommInvDataArrayList() {
        return commInvDataArrayList;
    }

    public void setCommInvDataArrayList(ArrayList<CommInvData> commInvDataArrayList) {
        this.commInvDataArrayList = commInvDataArrayList;
    }

    private ArrayList<CommInvData> commInvDataArrayList = new ArrayList<>();
    public ArrayList<POData> getPoDataArrayList() {
        return poDataArrayList;
    }

    public void setPoDataArrayList(ArrayList<POData> poDataArrayList) {
        this.poDataArrayList = poDataArrayList;
    }

    private ArrayList<POData> poDataArrayList = new ArrayList<>();

    public ArrayList<PackagingData> getPackagingDataArrayList() {
        return packagingDataArrayList;
    }

    public void setPackagingDataArrayList(ArrayList<PackagingData> packagingDataArrayList) {
        this.packagingDataArrayList = packagingDataArrayList;
    }

    private ArrayList<PackagingData> packagingDataArrayList = new ArrayList<>();
    public ArrayList<LoginData> getLoginDataArrayList() {
        return loginDataArrayList;
    }

    public void setLoginDataArrayList(ArrayList<LoginData> loginDataArrayList) {
        this.loginDataArrayList = loginDataArrayList;
    }

    private ArrayList<LoginData> loginDataArrayList = new ArrayList<>();

    public ArrayList<ChildDetailsData> getChildDetailsDataArrayList() {
        return childDetailsDataArrayList;
    }

    public void setChildDetailsDataArrayList(ArrayList<ChildDetailsData> childDetailsDataArrayList) {
        this.childDetailsDataArrayList = childDetailsDataArrayList;
    }

    private ArrayList<ChildDetailsData> childDetailsDataArrayList = new ArrayList<>();


    public ArrayList<ShipmentDetailsData> getShipmentDetailsDataArrayList() {
        return shipmentDetailsDataArrayList;
    }

    public void setShipmentDetailsDataArrayList(ArrayList<ShipmentDetailsData> shipmentDetailsDataArrayList) {
        this.shipmentDetailsDataArrayList = shipmentDetailsDataArrayList;
    }

    private ArrayList<ShipmentDetailsData> shipmentDetailsDataArrayList = new ArrayList<>();
    public ArrayList<BuyerWiseCBMData> getBuyerWiseCBMDataArrayList() {
        return buyerWiseCBMDataArrayList;
    }

    public void setBuyerWiseCBMDataArrayList(ArrayList<BuyerWiseCBMData> buyerWiseCBMDataArrayList) {
        this.buyerWiseCBMDataArrayList = buyerWiseCBMDataArrayList;
    }

    public ArrayList<BuyerWiseGWTData> getBuyerWiseGWTDataArrayList() {
        return buyerWiseGWTDataArrayList;
    }

    public void setBuyerWiseGWTDataArrayList(ArrayList<BuyerWiseGWTData> buyerWiseGWTDataArrayList) {
        this.buyerWiseGWTDataArrayList = buyerWiseGWTDataArrayList;
    }

    private ArrayList<BuyerWiseCBMData> buyerWiseCBMDataArrayList = new ArrayList<>();
    private ArrayList<BuyerWiseGWTData> buyerWiseGWTDataArrayList = new ArrayList<>();
    public ArrayList<BuyerWiseShipmentData> getBuyerWiseDataArrayList() {
        return buyerWiseDataArrayList;
    }

    public void setBuyerWiseDataArrayList(ArrayList<BuyerWiseShipmentData> buyerWiseDataArrayList) {
        this.buyerWiseDataArrayList = buyerWiseDataArrayList;
    }

    private ArrayList<BuyerWiseShipmentData> buyerWiseDataArrayList = new ArrayList<>();
    public ArrayList<Top5ShipmentData> getTop5ShipmentDataArrayList() {
        return top5ShipmentDataArrayList;
    }

    public void setTop5ShipmentDataArrayList(ArrayList<Top5ShipmentData> top5ShipmentDataArrayList) {
        this.top5ShipmentDataArrayList = top5ShipmentDataArrayList;
    }

    private ArrayList<Top5ShipmentData> top5ShipmentDataArrayList = new ArrayList<>();
    public ArrayList<BarChartData> getBarChartDataArrayList() {
        return barChartDataArrayList;
    }

    public void setBarChartDataArrayList(ArrayList<BarChartData> barChartDataArrayList) {
        this.barChartDataArrayList = barChartDataArrayList;
    }

    private ArrayList<BarChartData> barChartDataArrayList = new ArrayList<>();
    public ArrayList<DashboardData> getDashboardDataArrayList() {
        return dashboardDataArrayList;
    }

    public void setDashboardDataArrayList(ArrayList<DashboardData> dashboardDataArrayList) {
        this.dashboardDataArrayList = dashboardDataArrayList;
    }

    private ArrayList<DashboardData> dashboardDataArrayList = new ArrayList<>();
    public ArrayList<TrackingData> getTrackingDataArrayList() {
        return trackingDataArrayList;
    }

    public void setTrackingDataArrayList(ArrayList<TrackingData> trackingDataArrayList) {
        this.trackingDataArrayList = trackingDataArrayList;
    }

    private  ArrayList<TrackingData> trackingDataArrayList = new ArrayList<TrackingData>();

    public ArrayList<NShipmentData> getGetNShipmentDataArrayList() {
        return getNShipmentDataArrayList;
    }

    public void setGetNShipmentDataArrayList(ArrayList<NShipmentData> getNShipmentDataArrayList) {
        this.getNShipmentDataArrayList = getNShipmentDataArrayList;
    }

    private  ArrayList<NShipmentData> getNShipmentDataArrayList = new ArrayList<NShipmentData>();


    private  static ArrayListManagerModel getReportData;

    public ArrayListManagerModel(){

    }
    public  static synchronized ArrayListManagerModel getInstance() {
        if (getReportData == null) {
            getReportData = new ArrayListManagerModel();
        }
        return getReportData;
    }
}