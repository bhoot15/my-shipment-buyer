package com.mgh.shipment.beans;

/**
 * Created by Akshay Thapliyal on 16-08-2016.
 */
public class BuyerWiseShipmentData {

    public String getBuyerNo() {
        return buyerNo;
    }

    public void setBuyerNo(String buyerNo) {
        this.buyerNo = buyerNo;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getTotalShipment() {
        return totalShipment;
    }

    public void setTotalShipment(String totalShipment) {
        this.totalShipment = totalShipment;
    }

    public String getTotalShipmentPerct() {
        return totalShipmentPerct;
    }

    public void setTotalShipmentPerct(String totalShipmentPerct) {
        this.totalShipmentPerct = totalShipmentPerct;
    }

    private String buyerNo, buyerName, totalShipment, totalShipmentPerct;
}
