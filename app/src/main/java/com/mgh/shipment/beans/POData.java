package com.mgh.shipment.beans;

/**
 * Created by Akshay Thapliyal on 15-09-2016.
 */
public class POData {

    public String getPo_no() {
        return po_no;
    }

    public void setPo_no(String po_no) {
        this.po_no = po_no;
    }

    private String po_no;

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    private String buyerName;

    public String getDocument_no() {
        return document_no;
    }

    public void setDocument_no(String document_no) {
        this.document_no = document_no;
    }

    public String getBl_no() {
        return bl_no;
    }

    public void setBl_no(String bl_no) {
        this.bl_no = bl_no;
    }

    public String getBooking_date() {
        return booking_date;
    }

    public void setBooking_date(String booking_date) {
        this.booking_date = booking_date;
    }

    public String getBl_date() {
        return bl_date;
    }

    public void setBl_date(String bl_date) {
        this.bl_date = bl_date;
    }

    public String getShipper_name() {
        return shipper_name;
    }

    public void setShipper_name(String shipper_name) {
        this.shipper_name = shipper_name;
    }

    public String getPol_code() {
        return pol_code;
    }

    public void setPol_code(String pol_code) {
        this.pol_code = pol_code;
    }

    public String getPod_code() {
        return pod_code;
    }

    public void setPod_code(String pod_code) {
        this.pod_code = pod_code;
    }

    public String getGross_wt() {
        return gross_wt;
    }

    public void setGross_wt(String gross_wt) {
        this.gross_wt = gross_wt;
    }

    public String getCharge_wt() {
        return charge_wt;
    }

    public void setCharge_wt(String charge_wt) {
        this.charge_wt = charge_wt;
    }

    public String getTot_volume() {
        return tot_volume;
    }

    public void setTot_volume(String tot_volume) {
        this.tot_volume = tot_volume;
    }

    public String getGr_date() {
        return gr_date;
    }

    public void setGr_date(String gr_date) {
        this.gr_date = gr_date;
    }

    public String getShipment_date() {
        return shipment_date;
    }

    public void setShipment_date(String shipment_date) {
        this.shipment_date = shipment_date;
    }

    public String getContainer_no() {
        return container_no;
    }

    public void setContainer_no(String container_no) {
        this.container_no = container_no;
    }

    public String getContainer_type() {
        return container_type;
    }

    public void setContainer_type(String container_type) {
        this.container_type = container_type;
    }

    private String ata;
    private String eta;
    private String atd;
    private String etd;

    public String getEta() {
        return eta;
    }

    public void setEta(String eta) {
        this.eta = eta;
    }

    public String getAtd() {
        return atd;
    }

    public void setAtd(String atd) {
        this.atd = atd;
    }

    public String getEtd() {
        return etd;
    }

    public void setEtd(String etd) {
        this.etd = etd;
    }

    public String getFeta() {
        return feta;
    }

    public void setFeta(String feta) {
        this.feta = feta;
    }

    public String getFetd() {
        return fetd;
    }

    public void setFetd(String fetd) {
        this.fetd = fetd;
    }

    public String getAta() {
        return ata;
    }

    public void setAta(String ata) {
        this.ata = ata;
    }

    private String feta;
    private String fetd;
    private String document_no;
    private String bl_no;
    private String booking_date;
    private String bl_date;
    private String shipper_name;
    private String pol_code;
    private String pod_code;
    private String gross_wt;
    private String charge_wt;
    private String tot_volume;
    private String gr_date;
    private String shipment_date;
    private String container_no;
    private String container_type;
}
