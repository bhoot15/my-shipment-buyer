package com.mgh.shipment.beans;

/**
 * Created by Akshay Thapliyal on 16-08-2016.
 */
public class BuyerWiseCBMData {

    public String getBuyerNo() {
        return buyerNo;
    }

    public void setBuyerNo(String buyerNo) {
        this.buyerNo = buyerNo;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getTotalCBM() {
        return totalCBM;
    }

    public void setTotalCBM(String totalCBM) {
        this.totalCBM = totalCBM;
    }

    public String getTotalCBMPerc() {
        return totalCBMPerc;
    }

    public void setTotalCBMPerc(String totalCBMPerc) {
        this.totalCBMPerc = totalCBMPerc;
    }

    private String buyerNo, buyerName, totalCBM, totalCBMPerc;
}
