package com.mgh.shipment.beans;

/**
 * Created by Akshay Thapliyal on 11-08-2016.
 */
public class BarChartData {
    private String salesOrgName;

    public String getSalesOrg() {
        return salesOrg;
    }

    public void setSalesOrg(String salesOrg) {
        this.salesOrg = salesOrg;
    }

    private String salesOrg;

    public String getShipmentCount() {
        return shipmentCount;
    }

    public void setShipmentCount(String shipmentCount) {
        this.shipmentCount = shipmentCount;
    }

    public String getSalesOrgName() {
        return salesOrgName;
    }

    public void setSalesOrgName(String salesOrgName) {
        this.salesOrgName = salesOrgName;
    }

    private String shipmentCount;
}
