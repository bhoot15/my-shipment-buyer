package com.mgh.shipment.beans;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;
import com.mgh.shipment.R;

/**
 * Created by Akshay Thapliyal on 01-09-2016.
 */
public class DetailsParentViewHolder extends ParentViewHolder{
    public TextView tvShipperNo, tvShipperName, tvBlNo;
    public ImageView expandArrow;

    public DetailsParentViewHolder(View itemView){
        super(itemView);

        tvShipperNo = (TextView)itemView.findViewById(R.id.tvShipperNo);
        tvShipperName = (TextView)itemView.findViewById(R.id.tvShipperName);
        tvBlNo = (TextView)itemView.findViewById(R.id.tvBlNo);
        expandArrow = (ImageView) itemView.findViewById(R.id.expandArrow);

    }
}
