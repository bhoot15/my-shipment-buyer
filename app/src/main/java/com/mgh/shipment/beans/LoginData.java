package com.mgh.shipment.beans;

import java.util.ArrayList;

/**
 * Created by Akshay Thapliyal on 05-09-2016.
 */
public class LoginData {

    public ArrayList<String> getDistChannel() {
        return distChannel;
    }

    public void setDistChannel(ArrayList<String> distChannel) {
        this.distChannel = distChannel;
    }

    public String getSalesOrg() {
        return salesOrg;
    }

    public void setSalesOrg(String salesOrg) {
        this.salesOrg = salesOrg;
    }

    public ArrayList<String> getDivision() {
        return division;
    }

    public void setDivision(ArrayList<String> division) {
        this.division = division;
    }

    private String salesOrg;

    public String getSalesOrgCode() {
        return salesOrgCode;
    }

    public void setSalesOrgCode(String salesOrgCode) {
        this.salesOrgCode = salesOrgCode;
    }

    private String salesOrgCode;
    private ArrayList<String> distChannel = new ArrayList<>();
    private ArrayList<String> division = new ArrayList<>();
}
