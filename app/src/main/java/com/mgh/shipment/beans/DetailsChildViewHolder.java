package com.mgh.shipment.beans;

import android.view.View;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;
import com.mgh.shipment.R;

/**
 * Created by Akshay Thapliyal on 01-09-2016.
 */
public class DetailsChildViewHolder extends ChildViewHolder{

    public TextView tvBLDate, tvBookingDate, tvPOLCode, tvPODCode, tvGrossWt, tvChargeWt,
            tvTotalVol, tvGrDate, tvShipmentDate;

    public DetailsChildViewHolder(View itemView){
        super(itemView);

        tvBLDate = (TextView)itemView.findViewById(R.id.tvBLDate);
        tvBookingDate = (TextView)itemView.findViewById(R.id.tvBookingDate);
        tvPOLCode = (TextView)itemView.findViewById(R.id.tvPOLCode);
        tvPODCode = (TextView)itemView.findViewById(R.id.tvPODCode);
        tvGrossWt = (TextView)itemView.findViewById(R.id.tvGrossWt);
        tvChargeWt = (TextView)itemView.findViewById(R.id.tvChargeWt);
        tvTotalVol = (TextView)itemView.findViewById(R.id.tvTotalVol);
        tvGrDate = (TextView)itemView.findViewById(R.id.tvGrDate);
        tvShipmentDate = (TextView)itemView.findViewById(R.id.tvShipmentDate);
    }
}
