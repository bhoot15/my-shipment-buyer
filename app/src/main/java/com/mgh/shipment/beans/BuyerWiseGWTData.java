package com.mgh.shipment.beans;

/**
 * Created by Akshay Thapliyal on 16-08-2016.
 */
public class BuyerWiseGWTData {
    public String getBuyerNo() {
        return buyerNo;
    }

    public void setBuyerNo(String buyerNo) {
        this.buyerNo = buyerNo;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getTotalGWT() {
        return totalGWT;
    }

    public void setTotalGWT(String totalGWT) {
        this.totalGWT = totalGWT;
    }

    public String getTotalGWTPerc() {
        return totalGWTPerc;
    }

    public void setTotalGWTPerc(String totalGWTPerc) {
        this.totalGWTPerc = totalGWTPerc;
    }

    private String buyerNo, buyerName, totalGWT, totalGWTPerc;
}
