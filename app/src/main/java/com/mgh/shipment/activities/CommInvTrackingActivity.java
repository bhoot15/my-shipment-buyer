package com.mgh.shipment.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.Model.ParentObject;
import com.mgh.shipment.R;
import com.mgh.shipment.Utility.RecyclerOnclick;
import com.mgh.shipment.Utility.RecyclerTouchListener;
import com.mgh.shipment.Utility.ScrollCallBack;
import com.mgh.shipment.adapters.CommInvAdapter;
import com.mgh.shipment.adapters.PODetailsAdapter;
import com.mgh.shipment.beans.ArrayListManagerModel;
import com.mgh.shipment.beans.ChildDetailsData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Akshay Thapliyal on 29-12-2016.
 */
public class CommInvTrackingActivity extends Activity implements View.OnClickListener, ScrollCallBack {
    private Toolbar toolbar;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private ImageView menuIcon;
    private TextView tvTitleToolbar;
    private LinearLayoutManager linearLayoutManager;

    @Override
    public void onCreate(Bundle savedInsatnceState){
        super.onCreate(savedInsatnceState);
        setContentView(R.layout.activity_po_details);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        // setSupportActionBar(toolbar);
        init();
        setListener();

    }

    public void init() {
        menuIcon = (ImageView)findViewById(R.id.menuIcon);
        tvTitleToolbar = (TextView) findViewById(R.id.tvTitleToolbar);
        //menuIcon.setImageResource(R.drawable.left_32);
        tvTitleToolbar.setText("Comm. Invoice Detail");
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        InitRecycler();
       /* swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
                //InitRecycler();
            }
        });*/
    }

    public void setListener() {

        menuIcon.setOnClickListener(this);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView, new RecyclerOnclick() {
            @Override
            public void onClick(View view, int position) {
                //InitRecycler();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.menuIcon:
                finish();
                break;
        }
    }



    public void InitRecycler() {
        CommInvAdapter shipmentDetailAdapter = new CommInvAdapter(this, ArrayListManagerModel.getInstance());
        recyclerView.setAdapter(shipmentDetailAdapter);

    }

    private ArrayList<ParentObject> generateParentObject() {
        // CrimeLab crimeLab = CrimeLab.get(getActivity());
        copyData();
        List<ChildDetailsData> crimes = ArrayListManagerModel.getInstance().getChildDetailsDataArrayList();//crimeLab.getCrimes();
        ArrayList<ChildDetailsData> childDetailsDatas;
        ArrayList<ParentObject> parentObjects = new ArrayList<>();
        for (ChildDetailsData crime : crimes) {
            ArrayList<Object> childList = new ArrayList<>();
            childList.add(new ChildDetailsData("232"));
            crime.setChildObjectList(childList);
            parentObjects.add(crime);
        }
        return parentObjects;
    }

    private void copyData(){
        int length = ArrayListManagerModel.getInstance().getShipmentDetailsDataArrayList().size();
        ArrayList<ChildDetailsData> childDetailsDataArrayList = new ArrayList<>();
        for(int i =0;i<length; i++){
            ChildDetailsData childDetailsData = new ChildDetailsData("9892");
            childDetailsData.setBldate(ArrayListManagerModel.getInstance().getShipmentDetailsDataArrayList().get(i).getBldate());
            childDetailsDataArrayList.add(childDetailsData);
        }
        ArrayListManagerModel.getInstance().setChildDetailsDataArrayList(childDetailsDataArrayList);
    }

    @Override
    public void scrollToClickedPosition(int position) {
        linearLayoutManager.scrollToPositionWithOffset(position, 10);
    }
}