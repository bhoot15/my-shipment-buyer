package com.mgh.shipment.activities;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.mgh.shipment.R;
import com.mgh.shipment.Utility.AppConstants;
import com.mgh.shipment.adapters.BuyerWiseShipmentAdapter;
import com.mgh.shipment.beans.ArrayListManagerModel;

import java.util.ArrayList;

/**
 * Created by Akshay Thapliyal on 10-08-2016.
 */
public class BuyerWiseShipmentActivity extends Activity implements View.OnClickListener {

    private RecyclerView recyclerView;
    private ScrollView nestedScroll;
    private Toolbar toolbar;
    private ImageView menuIcon;
    private Typeface tf;
    private PieChart pieChart;
    private String type;
    private TextView tvTitleToolbar;
    private int[] MATERIAL_COLORS = {
            ColorTemplate.rgb("#2ecc71"), ColorTemplate.rgb("#f1c40f"), ColorTemplate.rgb("#e74c3c"), ColorTemplate.rgb("#3498db"),
            ColorTemplate.rgb("#51c51f"), ColorTemplate.rgb("#ae4c9c"), ColorTemplate.rgb("#ff9800"),
            ColorTemplate.rgb("#aacc00"), ColorTemplate.rgb("#00c400"), ColorTemplate.rgb("#ee4cec"), ColorTemplate.rgb("#5098db"),
            ColorTemplate.rgb("#30ccaa"), ColorTemplate.rgb("#90c4aa"), ColorTemplate.rgb("#cd4c30"), ColorTemplate.rgb("#549859")
            ,
            ColorTemplate.rgb("#2ecc71"), ColorTemplate.rgb("#f1c40f"), ColorTemplate.rgb("#e74c3c"), ColorTemplate.rgb("#3498db"),
            ColorTemplate.rgb("#51c51f"), ColorTemplate.rgb("#ae4c9c"), ColorTemplate.rgb("#ff9800"),
            ColorTemplate.rgb("#aacc00"), ColorTemplate.rgb("#00c400"), ColorTemplate.rgb("#ee4cec"), ColorTemplate.rgb("#5098db"),
            ColorTemplate.rgb("#30ccaa"), ColorTemplate.rgb("#90c4aa"), ColorTemplate.rgb("#cd4c30"), ColorTemplate.rgb("#549859")
            ,
            ColorTemplate.rgb("#2ecc71"), ColorTemplate.rgb("#f1c40f"), ColorTemplate.rgb("#e74c3c"), ColorTemplate.rgb("#3498db"),
            ColorTemplate.rgb("#51c51f"), ColorTemplate.rgb("#ae4c9c"), ColorTemplate.rgb("#ff9800"),
            ColorTemplate.rgb("#aacc00"), ColorTemplate.rgb("#00c400"), ColorTemplate.rgb("#ee4cec"), ColorTemplate.rgb("#5098db"),
            ColorTemplate.rgb("#30ccaa"), ColorTemplate.rgb("#90c4aa"), ColorTemplate.rgb("#cd4c30"), ColorTemplate.rgb("#549859")
    };
//#2ecc71
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.buyer_wise_shipment);

      /*  setSupportActionBar(toolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);*/
        init();
        setRecyclerViewScrolling();

        type = getIntent().getStringExtra("type");
        setToolbarTitle(type);

        setAdapter();
        setListner();
    }

    public void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvTitleToolbar = (TextView)findViewById(R.id.tvTitleToolbar);
        nestedScroll = (ScrollView) findViewById(R.id.nestedScroll);
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        menuIcon = (ImageView) findViewById(R.id.menuIcon);
        pieChart = (PieChart) findViewById(R.id.pieChart);
    }

    private void setRecyclerViewScrolling(){
        recyclerView.setNestedScrollingEnabled(false);
        nestedScroll.fullScroll(View.FOCUS_UP);
        recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                int action = e.getAction();
                switch (action) {
                    case MotionEvent.ACTION_MOVE:
                        rv.getParent().requestDisallowInterceptTouchEvent(true);
                        break;
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
    }

    private void setToolbarTitle(String type){
        if(type.equalsIgnoreCase(AppConstants.TOTAL_SHIPMENT)){
            tvTitleToolbar.setText("Top Shipment");
        }else if(type.equalsIgnoreCase(AppConstants.TOTAL_CBM)){
            tvTitleToolbar.setText("Top CBM");
        }else if(type.equalsIgnoreCase(AppConstants.TOTAL_GWT)){
            tvTitleToolbar.setText("Top GWT");
        }
    }
    private void setAdapter(){
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        BuyerWiseShipmentAdapter buyerWiseShipmentAdapter = new BuyerWiseShipmentAdapter(this, type, ArrayListManagerModel.getInstance());
        recyclerView.setAdapter(buyerWiseShipmentAdapter);
        setPieChart();
    }

    public void setListner() {
        menuIcon.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == menuIcon.getId()) {
            finish();
        }
    }

    private void setPieChart() {

        tf = Typeface.createFromAsset(this.getAssets(),
                "quicksandbold.otf");

        pieChart.setUsePercentValues(true);
        pieChart.setDescription("");
        pieChart.setExtraOffsets(5, 10, 5, 5);

        pieChart.setDragDecelerationFrictionCoef(0.95f);

        pieChart.setCenterTextTypeface(tf);
//        pieChart.setCenterText(generateCenterSpannableText());

        pieChart.setDrawHoleEnabled(true);
        pieChart.setHoleColor(Color.TRANSPARENT);

        pieChart.setTransparentCircleColor(Color.TRANSPARENT);
        pieChart.setTransparentCircleAlpha(110);

        pieChart.setHoleRadius(0f);
        pieChart.setTransparentCircleRadius(0f);

        pieChart.setDrawCenterText(true);

        pieChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        pieChart.setRotationEnabled(true);
        pieChart.setHighlightPerTapEnabled(true);

        // mChart.setUnit(" â‚¬");
        // mChart.setDrawUnitsInChart(true);

        // add a selection listener
//        pieChart.setOnChartValueSelectedListener(this);

        setData(2, 100);

        pieChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        // mChart.spin(2000, 0, 360);
        Legend l = pieChart.getLegend();
        l.setPosition(Legend.LegendPosition.RIGHT_OF_CHART);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);

        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());


        if(type.equalsIgnoreCase(AppConstants.TOTAL_SHIPMENT)){
            int length = ArrayListManagerModel.getInstance().getBuyerWiseDataArrayList().size();
            int pieLength = 10;
            if(length<pieLength){
                pieLength = length;
            }
            String legend [] = new String[pieLength];
            int legendColors[] = new int[pieLength];
            for(int i =0; i<pieLength; i++){
                legend[i] = ArrayListManagerModel.getInstance().getBuyerWiseDataArrayList().get(i).getBuyerName();
                legendColors[i] = MATERIAL_COLORS[i];
            }
            l.setCustom(legendColors, legend);

        }else if(type.equalsIgnoreCase(AppConstants.TOTAL_CBM)){
            int length = ArrayListManagerModel.getInstance().getBuyerWiseCBMDataArrayList().size();
            int pieLength = 10;
            if(length<pieLength){
                pieLength = length;
            }
            int legendColors[] = new int[pieLength];
            String legend [] = new String[pieLength];
            for(int i =0; i<pieLength; i++){
                legend[i] = ArrayListManagerModel.getInstance().getBuyerWiseCBMDataArrayList().get(i).getBuyerName();
                legendColors[i] = MATERIAL_COLORS[i];
            }
            l.setCustom(legendColors, legend);

        }else if(type.equalsIgnoreCase(AppConstants.TOTAL_GWT)){
            int length = ArrayListManagerModel.getInstance().getBuyerWiseGWTDataArrayList().size();
            int pieLength = 10;
            if(length<pieLength){
                pieLength = length;
            }
            int legendColors[] = new int[pieLength];
            String legend [] = new String[pieLength];
            for(int i =0; i<pieLength; i++){
                legend[i] = ArrayListManagerModel.getInstance().getBuyerWiseGWTDataArrayList().get(i).getBuyerName();
                legendColors[i] = MATERIAL_COLORS[i];
            }
            l.setCustom(legendColors, legend);

        }

        l.setComputedColors(colors);

        // entry label styling
        pieChart.setEntryLabelColor(Color.WHITE);
        pieChart.setEntryLabelTypeface(tf);
        pieChart.setEntryLabelTextSize(12f);

//        pieChart.setOnChartValueSelectedListener(this);

    }

    private void setData(int count, float range) {

        float mult = range;
        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.

       /* entries.add(new PieEntry((float) ((Math.random() * mult) + mult / 5), "Released"));
        entries.add(new PieEntry((float) ((Math.random() * mult) + mult / 5), "Pending"));*/

        PieDataSet dataSet = new PieDataSet(entries, " --BL Status");
        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);

        if(type.equalsIgnoreCase(AppConstants.TOTAL_SHIPMENT)){
            int length = ArrayListManagerModel.getInstance().getBuyerWiseDataArrayList().size();
            int pieLength = 10;
            if(length<pieLength){
                pieLength = length;
            }
            String legend [] = new String[pieLength];
            int legendColors[] = new int[pieLength];
            float data;
            for(int i =0; i<pieLength; i++){
                legend[i] = ArrayListManagerModel.getInstance().getBuyerWiseDataArrayList().get(i).getBuyerName();
                data = Float.parseFloat(ArrayListManagerModel.getInstance().getBuyerWiseDataArrayList().get(i).getTotalShipmentPerct());
                entries.add(new PieEntry((float) ((data)), ""));
                legendColors[i] = MATERIAL_COLORS[i];
                dataSet.setColors(legendColors);
            }

        }else if(type.equalsIgnoreCase(AppConstants.TOTAL_CBM)){
            int length = ArrayListManagerModel.getInstance().getBuyerWiseCBMDataArrayList().size();
            int pieLength = 10;
            if(length<pieLength){
                pieLength = length;
            }
            float data;
            int legendColors[] = new int[pieLength];
            String legend [] = new String[pieLength];
            for(int i =0; i<pieLength; i++){
                legend[i] = ArrayListManagerModel.getInstance().getBuyerWiseCBMDataArrayList().get(i).getBuyerName();
                data = Float.parseFloat(ArrayListManagerModel.getInstance().getBuyerWiseCBMDataArrayList().get(i).getTotalCBMPerc());
                entries.add(new PieEntry((float) ((data)), ""));
                legendColors[i] = MATERIAL_COLORS[i];
                dataSet.setColors(legendColors);
            }

        }else if(type.equalsIgnoreCase(AppConstants.TOTAL_GWT)){
            int length = ArrayListManagerModel.getInstance().getBuyerWiseGWTDataArrayList().size();
            int pieLength = 10;
            if(length<pieLength){
                pieLength = length;
            }
            String legend [] = new String[pieLength];
            int legendColors[] = new int[pieLength];
            float data;
            for(int i =0; i<pieLength; i++){
                legend[i] = ArrayListManagerModel.getInstance().getBuyerWiseGWTDataArrayList().get(i).getBuyerName();
                data = Float.parseFloat(ArrayListManagerModel.getInstance().getBuyerWiseGWTDataArrayList().get(i).getTotalGWTPerc());
                entries.add(new PieEntry((float) ((data)), ""));
                legendColors[i] = MATERIAL_COLORS[i];
                dataSet.setColors(legendColors);
            }
        }

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        dataSet.setLabel("Pending \n Released");
        //dataSet.setSelectionShift(0f);
//        PieData dat = new PieData(list,dataSet);
        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);
        data.setValueTypeface(tf);
        pieChart.setData(data);

        // undo all highlights
        pieChart.highlightValues(null);

        pieChart.invalidate();
    }

    public static final int[] JOYFUL_COLORS = {
            Color.rgb(217, 80, 138), Color.rgb(254, 149, 7)
    };
}
