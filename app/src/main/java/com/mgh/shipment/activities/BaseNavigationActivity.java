package com.mgh.shipment.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.mgh.shipment.R;
import com.mgh.shipment.Utility.SharedPreferenceManager;

/**
 * Created by Akshay Thapliyal on 12-05-2016.
 */
public class BaseNavigationActivity extends AppCompatActivity implements View.OnClickListener{

    protected DrawerLayout mDrawerLayout;
    //protected FrameLayout frame;
    RelativeLayout menuLayout;
    private LinearLayout trackingLayout, freqTrackingLayout, misLayout, newEventsLayout, contactUsLayout, logoutLayout,dashboardLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_activity);
        init();
        setListeners();
    }

    private void init(){
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        //frame = (FrameLayout) findViewById(R.id.content_frame);
        menuLayout = (RelativeLayout)findViewById(R.id.menuLayout);
        trackingLayout = (LinearLayout)findViewById(R.id.trackingLayout);
        freqTrackingLayout = (LinearLayout)findViewById(R.id.freqTrackingLayout);
        misLayout = (LinearLayout)findViewById(R.id.misLayout);
        newEventsLayout = (LinearLayout)findViewById(R.id.newEventsLayout);
        contactUsLayout = (LinearLayout)findViewById(R.id.contactUsLayout);
        logoutLayout = (LinearLayout)findViewById(R.id.logoutLayout);
        dashboardLayout = (LinearLayout)findViewById(R.id.dashboardLayout);
    }

    private void setListeners(){
        trackingLayout.setOnClickListener(this);
        freqTrackingLayout.setOnClickListener(this);
        misLayout.setOnClickListener(this);
        newEventsLayout.setOnClickListener(this);
        contactUsLayout.setOnClickListener(this);
        logoutLayout.setOnClickListener(this);
        dashboardLayout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == trackingLayout.getId()){
            clickEventSlide();
            Intent intent = new Intent(this, TrackingActivity.class);
            startActivity(intent);
        }else if(v.getId() == dashboardLayout.getId()){
            clickEventSlide();
            Intent intent = new Intent(this, DashBoardActivity.class);
            startActivity(intent);
        }
        else if(v.getId() == freqTrackingLayout.getId()){
            clickEventSlide();
            Intent intent = new Intent(this, FrequentTrackingActivity.class);
            startActivity(intent);
        }
        else if(v.getId() == misLayout.getId()){
            clickEventSlide();
            Intent intent = new Intent(this, MISActivity.class);
            startActivity(intent);
        }
        else if(v.getId() == newEventsLayout.getId()){
            clickEventSlide();
            Intent intent = new Intent(this, NewsAndEventsActivity.class);
            startActivity(intent);
        }
        else if(v.getId() == contactUsLayout.getId()){
            clickEventSlide();
            Intent intent = new Intent(this, ContactUsActivity.class);
            startActivity(intent);
        }
        else if(v.getId() == logoutLayout.getId()){
            clickEventSlide();
            logout();
        }
    }

    public void clickEventSlide() {
        if (mDrawerLayout.isDrawerOpen(menuLayout)) {
            mDrawerLayout.closeDrawer(menuLayout);
        } else {
            mDrawerLayout.openDrawer(menuLayout);
            menuLayout.invalidate();
        }
    }

    private void logout() {
        SharedPreferenceManager.clearCredentials(this);
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
