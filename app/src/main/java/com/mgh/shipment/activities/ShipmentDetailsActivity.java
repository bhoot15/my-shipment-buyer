package com.mgh.shipment.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.Model.ParentObject;
import com.mgh.shipment.R;
import com.mgh.shipment.Utility.RecyclerOnclick;
import com.mgh.shipment.Utility.RecyclerTouchListener;
import com.mgh.shipment.Utility.ScrollCallBack;
import com.mgh.shipment.adapters.DetailsExpandableAdapter;
import com.mgh.shipment.adapters.ShipmentDetailAdapter;
import com.mgh.shipment.beans.ArrayListManagerModel;
import com.mgh.shipment.beans.ChildDetailsData;
import com.mgh.shipment.beans.ShipmentDetailsData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Amit Sharma on 09-08-2016.
 */
public class ShipmentDetailsActivity extends Activity implements View.OnClickListener, ScrollCallBack {
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private ImageView menuIcon;
    private TextView tvTitleToolbar;
    private LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shipmentdetails_activity);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
       // setSupportActionBar(toolbar);
        init();
        setListener();

    }

    public void init() {
        menuIcon = (ImageView)findViewById(R.id.menuIcon);
        tvTitleToolbar = (TextView) findViewById(R.id.tvTitleToolbar);
        //menuIcon.setImageResource(R.drawable.left_32);
        tvTitleToolbar.setText("Shipment Detail");
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        InitRecycler();
    }

    public void setListener() {
        menuIcon.setOnClickListener(this);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView, new RecyclerOnclick() {
            @Override
            public void onClick(View view, int position) {
                //InitRecycler();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.menuIcon:
                finish();
                break;
        }
    }


    public void InitRecycler() {
        ShipmentDetailAdapter shipmentDetailAdapter = new ShipmentDetailAdapter(this, ArrayListManagerModel.getInstance());
        recyclerView.setAdapter(shipmentDetailAdapter);
       /*DetailsExpandableAdapter detailsExpandableAdapter = new DetailsExpandableAdapter(ShipmentDetailsActivity.this,generateParentObject(),ShipmentDetailsActivity.this.getLayoutInflater(), ArrayListManagerModel.getInstance());
        detailsExpandableAdapter.setCustomParentAnimationViewId(R.id.expandArrow);
        detailsExpandableAdapter.setParentClickableViewAnimationDefaultDuration();
        detailsExpandableAdapter.setParentAndIconExpandOnClick(true);
        recyclerView.setAdapter(detailsExpandableAdapter);
        swipeRefreshLayout.setRefreshing(false);*/
    }

    private ArrayList<ParentObject> generateParentObject() {
       // CrimeLab crimeLab = CrimeLab.get(getActivity());
        copyData();
        List<ChildDetailsData> crimes = ArrayListManagerModel.getInstance().getChildDetailsDataArrayList();//crimeLab.getCrimes();
        ArrayList<ChildDetailsData> childDetailsDatas;
        ArrayList<ParentObject> parentObjects = new ArrayList<>();
        for (ChildDetailsData crime : crimes) {
            ArrayList<Object> childList = new ArrayList<>();
            childList.add(new ChildDetailsData("232"));
            crime.setChildObjectList(childList);
            parentObjects.add(crime);
        }
        return parentObjects;
    }

    private void copyData(){
        int length = ArrayListManagerModel.getInstance().getShipmentDetailsDataArrayList().size();
        ArrayList<ChildDetailsData> childDetailsDataArrayList = new ArrayList<>();
        for(int i =0;i<length; i++){
            ChildDetailsData childDetailsData = new ChildDetailsData("9892");
            childDetailsData.setBldate(ArrayListManagerModel.getInstance().getShipmentDetailsDataArrayList().get(i).getBldate());
            childDetailsDataArrayList.add(childDetailsData);
        }
        ArrayListManagerModel.getInstance().setChildDetailsDataArrayList(childDetailsDataArrayList);
    }

    @Override
    public void scrollToClickedPosition(int position) {
        System.out.println("POS::" + position);
        linearLayoutManager.scrollToPositionWithOffset(position, 10);
    }
}
