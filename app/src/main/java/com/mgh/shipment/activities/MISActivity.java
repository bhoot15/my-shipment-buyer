package com.mgh.shipment.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;

import com.mgh.shipment.R;

/**
 * Created by Akshay Thapliyal on 24-05-2016.
 */
public class MISActivity  extends BaseNavigationActivity {

    private FrameLayout misFrameLayout;
    private float lastTranslate = 0.0f;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.mis_activity, null, false);
        mDrawerLayout.addView(contentView, 0);

        init(contentView);
        movingLayout();
    }

    private void init(View view){
        misFrameLayout =(FrameLayout)view.findViewById(R.id.misFrameLayout);
    }

    private void movingLayout(){
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.acc_drawer_open, R.string.acc_drawer_close)
        {
            @SuppressLint("NewApi")
            public void onDrawerSlide(View drawerView, float slideOffset)
            {
                float moveFactor = (menuLayout.getWidth() * slideOffset);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                {
                    misFrameLayout.setTranslationX(moveFactor);
                }
                else
                {
                    TranslateAnimation anim = new TranslateAnimation(lastTranslate, moveFactor, 0.0f, 0.0f);
                    anim.setDuration(0);
                    anim.setFillAfter(true);
                    misFrameLayout.startAnimation(anim);
                    lastTranslate = moveFactor;
                }
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }
}
