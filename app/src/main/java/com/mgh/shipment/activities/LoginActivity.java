package com.mgh.shipment.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.mgh.shipment.R;
import com.mgh.shipment.Utility.ApiUrls;
import com.mgh.shipment.Utility.GeneralUtils;
import com.mgh.shipment.Utility.SharedPreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Akshay Thapliyal on 10-05-2016.
 */
public class LoginActivity extends Activity implements View.OnClickListener {

    private Button signInBtn;
    private TextView tvForgotPassword;
    private EditText userCodeEditText, passwordEditText;
    private ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.login_activity);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        init();
        setListeners();
        checkIfAlreadyLoggedIn();
    }
    private void init(){
        userCodeEditText = (EditText)findViewById(R.id.userCodeEditText);
        passwordEditText = (EditText)findViewById(R.id.passwordEditText);
        signInBtn = (Button)findViewById(R.id.signInBtn);
        tvForgotPassword = (TextView)findViewById(R.id.tvForgotPassword);
    }

    private  void setListeners(){
        signInBtn.setOnClickListener(this);
        tvForgotPassword.setOnClickListener(this);
    }

    private void checkIfAlreadyLoggedIn(){
        if(!(SharedPreferenceManager.getUserCode(this).equals(""))){
            Intent intent = new Intent(this, DashBoardActivity.class);
            startActivity(intent);
            finish();
        }
    }
    @Override
    public void onClick(View v) {

        if(v.getId() == signInBtn.getId()){
            if(GeneralUtils.isNetworkAvailable(this)){
                GeneralUtils.ScaleAnimate(signInBtn, LoginActivity.this);
                checkValidations();
            }else{
                Toast.makeText(this, getResources().getString(R.string.internet_connection_msg), Toast.LENGTH_SHORT).show();
            }
        }
        else if(v.getId() == tvForgotPassword.getId()){
            //showForgotPasswordDialog();
        }
    }

    private void showForgotPasswordDialog(){
        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.forgot_password_dialog);
        dialog.setTitle(getResources().getString(R.string.forgot_paswrd_text));
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        Button resetBtn = (Button)dialog.findViewById(R.id.resetBtn);
        EditText userEditText = (EditText)dialog.findViewById(R.id.userEditText);
        dialog.show();

        resetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callForgotPasswordApi();
            }
        });
    }

    /**
     * Check for user credentials & hit api for the same
     */
    private void checkValidations(){
        String userCode = userCodeEditText.getText().toString();
        String password = passwordEditText.getText().toString();
        //Check for empty spaces and give error message
        if(userCode!= null && !userCode.isEmpty()){
            if(password!=null && !password.isEmpty()){
                callLoginApi(userCode,password);
            }else{
                new AlertDialog.Builder(this).setMessage(getResources().getString(R.string.password_error))
                        .setTitle(getResources().getString(R.string.error_text))
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                
                            }
                        }).show();
            }
        }else{
            new AlertDialog.Builder(this).setMessage(getResources().getString(R.string.userCode_error))
                    .setTitle(getResources().getString(R.string.error_text))
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                        }
                    }).show();
        }
    }

    private void callLoginApi(final String userCode, String password){

        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userCode", userCode);
            jsonObject.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        dialog = new ProgressDialog(LoginActivity.this);
        dialog.setMessage(getResources().getString(R.string.please_wait_text));
        dialog.show();

        System.out.println("Login json:" + jsonObject.toString());
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                ApiUrls.LOGIN_URL, jsonObject, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                if(dialog.isShowing()){
                    dialog.dismiss();
                }

                try {
                    // Parsing json object response
                    JSONObject jsonObject = response.getJSONObject("bapiReturn2");
                    String message = jsonObject.getString("message");
                    if(message.equalsIgnoreCase("Login successfull!!")){

                        Intent intent = new Intent(LoginActivity.this, CompanySelectionActivity.class);
                        intent.putExtra("loginResponse",response.toString());
                        intent.putExtra("usercode", userCode);
                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_left,R.anim.slide_right);
                        finish();
                    }else{
                        Toast.makeText(getApplicationContext(),
                                getResources().getString(R.string.valid_usercode_pass_text),
                                Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.error_authenticating_text),
                            Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        getResources().getString(R.string.error_authenticating_text), Toast.LENGTH_SHORT).show();
                if(dialog.isShowing()){
                    dialog.dismiss();
                }
            }
        });

        // Adding request to request queue
        requestQueue.add(jsonObjReq);

    }


    private void callForgotPasswordApi(){

    }
}
