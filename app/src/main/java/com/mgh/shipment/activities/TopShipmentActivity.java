package com.mgh.shipment.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.mgh.shipment.R;
import com.mgh.shipment.Utility.ApiUrls;
import com.mgh.shipment.Utility.RecyclerOnclick;
import com.mgh.shipment.Utility.RecyclerTouchListener;
import com.mgh.shipment.Utility.SharedPreferenceManager;
import com.mgh.shipment.adapters.TopShipmentAdapter;
import com.mgh.shipment.beans.ArrayListManagerModel;
import com.mgh.shipment.beans.ChildDetailsData;
import com.mgh.shipment.beans.ShipmentDetailsData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Amit Sharma on 09-08-2016.
 */
public class TopShipmentActivity extends Activity implements View.OnClickListener {

    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private ImageView MenuIcon;
    private TextView tvTitleToolbar;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shipmentdetails_activity);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        init();
        setListener();
        initRecycler();
    }

    public void init() {
        MenuIcon = (ImageView) findViewById(R.id.menuIcon);
        tvTitleToolbar = (TextView) findViewById(R.id.tvTitleToolbar);
        tvTitleToolbar.setText("Top Shipments");
       // MenuIcon.setImageResource(R.drawable.left_32);
        recyclerView = (RecyclerView) findViewById(R.id.recycler);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    public void setListener() {

        MenuIcon.setOnClickListener(this);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(TopShipmentActivity.this, recyclerView, new RecyclerOnclick() {
            @Override
            public void onClick(View view, int position) {
                String pod = ArrayListManagerModel.getInstance().getTop5ShipmentDataArrayList().get(position).getPortOfDischarge();
                callTopShipmentDetailsApi(pod);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void callTopShipmentDetailsApi(String pod){
        final JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject1 = new JSONObject();
        String params="";
        try {
            jsonObject.put("shipperNo", SharedPreferenceManager.getUserCode(this));
            jsonObject.put("division", SharedPreferenceManager.getDivision(this));
            jsonObject.put("distChannel", SharedPreferenceManager.getDistChannel(this));
            jsonObject.put("bookingDateFrom", SharedPreferenceManager.getFromDate(this));
            jsonObject.put("bookingDateTo", SharedPreferenceManager.getToDate(this));
            jsonArray.put(jsonObject);
            jsonObject1.put("pod", pod);
            jsonObject1.put("dashboardShipperParams",jsonObject);
            System.out.println("pod::" + jsonObject1.toString());
            params = jsonObject1.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        dialog = new ProgressDialog(this);
        dialog.setMessage(getResources().getString(R.string.please_wait_text));
        dialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjReq = null;
        try {
            jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    ApiUrls.SHIPMENT_DETAILS, new JSONObject(params), new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {

                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }

                    try {
                        // Parsing json object response

                        JSONArray jsonArray = response.getJSONArray("lstSOWiseShipDetailsBuyerJson");
                        int length = jsonArray.length();
                        if (length > 0) {
                            ArrayListManagerModel arrayListManagerModel = ArrayListManagerModel.getInstance();
                            ArrayList<ShipmentDetailsData> shipmentDetailsDataArrayList = new ArrayList<>();
                            ArrayList<ChildDetailsData> childDetailsDataArrayList = new ArrayList<>();
                            for(int i =0; i<length ; i++){
                                ShipmentDetailsData shipmentDetailsData = new ShipmentDetailsData();
                                shipmentDetailsData.setBlNo(jsonArray.getJSONObject(i).getString("blNo"));
                                shipmentDetailsData.setBookingDate(jsonArray.getJSONObject(i).getString("bookingDate"));
                                shipmentDetailsData.setBldate(jsonArray.getJSONObject(i).getString("bldate"));
                                shipmentDetailsData.setChargeWeight(jsonArray.getJSONObject(i).getString("chargeWeight"));
                                shipmentDetailsData.setGrDate(jsonArray.getJSONObject(i).getString("grDate"));
                                shipmentDetailsData.setGrossWeight(jsonArray.getJSONObject(i).getString("grossWeight"));
                                shipmentDetailsData.setPodCode(jsonArray.getJSONObject(i).getString("podCode"));
                                shipmentDetailsData.setPolCode(jsonArray.getJSONObject(i).getString("polCode"));
                                shipmentDetailsData.setShipmentDate(jsonArray.getJSONObject(i).getString("shipmentDate"));
                                shipmentDetailsData.setBuyerName(jsonArray.getJSONObject(i).getString("buyerName"));
                                shipmentDetailsData.setShipperNo(jsonArray.getJSONObject(i).getString("buyerNo"));
                                shipmentDetailsData.setTotVolume(jsonArray.getJSONObject(i).getString("totVolume"));
                                shipmentDetailsDataArrayList.add(shipmentDetailsData);
                            }
                            arrayListManagerModel.setShipmentDetailsDataArrayList(shipmentDetailsDataArrayList);
                            System.out.println("size::" + arrayListManagerModel.getShipmentDetailsDataArrayList().size());
                            Intent intent = new Intent(TopShipmentActivity.this, ShipmentDetailsActivity.class);
                            startActivity(intent);

                        } else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(),
                                getResources().getString(R.string.error_fetching),
                                Toast.LENGTH_LONG).show();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.error_fetching), Toast.LENGTH_SHORT).show();
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Adding request to request queue
        requestQueue.add(jsonObjReq);

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.menuIcon:
                finish();
                break;
        }
    }


    public void initRecycler() {
        TopShipmentAdapter topShipmentAdapter = new TopShipmentAdapter(this, ArrayListManagerModel.getInstance());
        recyclerView.setAdapter(topShipmentAdapter);

    }
}

